#ifndef __ultragame__Renderer__
#define __ultragame__Renderer__

#include <vector>
#include <glm/glm.hpp>
#include <unordered_map>
#include <map>
#include <functional>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengles2.h>

#include "IFileSaver.h"
#include "GeometrySet.h"


/**
 * The attribute contains all the information we need 
 * to set the pointer and enable it.
 */
struct Attribute {
	std::string name;
	GLuint index;
	GLint size;
	GLenum type;
	GLboolean normalized;
	GLsizei stride;
	GLvoid* pointer;
};

/**
 * Contains information about a shader program, it's attributes and uniforms.
 */
class ShaderProgram {
public:
	GLuint program;
	std::unordered_map<std::string, Attribute> attributes;
	std::unordered_map<std::string, GLint> uniforms;
	
	/**
	 * Makes the shader program ready
	 * By setting the pointers to the attributes
	 * and setting it as the used program.
	 */
	void readyShaderProgram() const;
	
	
	/**
	 * Readies a single attirbute.
	 * Sets and enables the pointer.
	 * @param attribute The attribute we want to make ready.
	 */
	void readyAttribute(const Attribute& attribute) const;
	
};


/**
 * Handles the rendering of the scene.
 */
class Renderer {
public:
	
	
	Renderer();
	virtual ~Renderer();
	
	/**
	 * Initiates the renderer.
	 * Setting the width, height, textureName and textureNumber.
	 */
	void init(int width, int height,
		  GLuint textureName,
		  GLuint textureNumber);
	
	/**
	 * Renders the scene.
	 */
	virtual void render() = 0;
	
	
	/**
	 * Handles the creation of shaders.
	 * Creates a shaderprogram from the passed in shaderpath,
	 * and ads it to the renderer's shaderprograms, shaderPrograms.
	 * Uses the passed along vector of attributes to bind theese.
	 * Uses the passed along function ptr to get the uniformLocations.
	 *
	 * @param name The name we want for the shader program.
	 * @param shaderPath The path to the shaders.
	 * @param attributes A vector containing the attributes.
	 * @param getUniformLocations The function called to get the uniformLocations.
	 */
	void createShaderProgramAndBindUniforms(std::string name,
						std::string shaderPath,
						std::vector<Attribute> attributes,
						std::function<std::unordered_map<std::string, GLint> (GLuint shaderProgram)> getUniformLocations);
	
	/**
	 * Adds the geometry set to the renderers sets to be rendered.
	 * @param set The set to be added.
	 */
	void addGeometrySet(const std::shared_ptr<GeometrySet>& set);
	
	/**
	 * Removes the set from the renderers rendered set.
	 * @param set The set to be removed.
	 */
	void removeGeometrySet(const std::shared_ptr<GeometrySet>& set);
	
	void renderGeometrySets(const ShaderProgram& shaderProgram);

	
protected:
	std::unique_ptr<IFileSaver> fileHandler;
	std::unordered_map<std::string, ShaderProgram> shaderPrograms;
	
	std::vector<std::shared_ptr<GeometrySet>> geometrySets;
	std::map<GeometryType, size_t> geometryOrder;
	
	
	float width;
	float height;
	
	GLuint VBO;
	GLuint textureName;
	GLuint activeTextureNumber;
};

#endif /* defined(__ultragame__Renderer__) */
