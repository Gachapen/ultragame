#include "MusicManager.h"

#include <cassert>
#include <algorithm>
#include <SDL2/SDL.h>

#include "log.h"
#include "Assets.h"
#include "GeneralFileSaver.h"

MusicManager* MusicManager::instance = nullptr;

MusicManager::MusicManager():
       lastTrackPlayed(nullptr)
{
}

bool MusicManager::loadFile(const std::string& filePath)
{
	GeneralFileSaver fileLoader;
	std::vector<unsigned char> fileData = fileLoader.readFile(filePath);

	if (fileData.size() == 0) {
		LOGE("MusicManager: Error: Got 0 bytes from music file %s\n", filePath.c_str());
		return false;
	}

	pugi::xml_document document;
	document.load_buffer(fileData.data(), fileData.size());

	pugi::xml_node musicNode = document.child("music");
	if (!musicNode) {
		LOGE("Could not find XML music node.\n");
		return false;
	}

	this->parseXml(musicNode);

	return true;
}

bool MusicManager::loadSet(const std::string& setName)
{
	LOGD("MusicManager: Loading set %s\n", setName.c_str());
	assert(std::find(this->setNames.begin(), this->setNames.end(), setName) != this->setNames.end());

	for (auto trackIt : this->tracks) {
		Track& track = trackIt.second;

		if (track.setName == setName && track.music == nullptr) {
			Mix_Music* music = Mix_LoadMUS(Assets::getInstance()->getPrependAssetsRootPath(track.filePath).c_str());
			if (music == nullptr) {
				LOGE("Could not load music file: %s\n", Mix_GetError());
			} else {
				this->tracks[trackIt.first].music = music;
				LOGD("MusicManager: Loaded track %s\n", track.filePath.c_str());
			}
		}

	}

	return true;
}

void MusicManager::unload()
{
    //TODO
    assert(!"TODO");
}

void MusicManager::pauseMusic()
{
    if (Mix_PlayingMusic() == 1)
    {
        Mix_PauseMusic();
    }
}

void MusicManager::resumeMusic()
{
    if (Mix_PausedMusic() == 1)
    {
        Mix_ResumeMusic();
    }
}

void MusicManager::restorePreviouslyStopped()
{
    if (this->lastTrackPlayed != nullptr)
    {
        Mix_PlayMusic(this->lastTrackPlayed, -1);
        Mix_SetMusicPosition(this->lastTrackDurationPlayed.count());
        this->startTime = std::chrono::high_resolution_clock::now();
    }
}

void MusicManager::stopMusic()
{
    Mix_HaltMusic();
    auto stopTime = std::chrono::high_resolution_clock::now();
    this->lastTrackDurationPlayed = stopTime - startTime + this->lastTrackDurationPlayed;
}

bool MusicManager::playTrack(const std::string& trackName)
{
    auto trackIt = this->tracks.find(trackName);
    if (trackIt == this->tracks.end())
    {
        LOGE("Could find track.\n");
        return false;
    }

    if (trackIt->second.music == nullptr)
    {
        LOGE("Track \"%s\" not loaded.\n", trackName.c_str());
        return false;
    }

    Mix_PlayMusic(trackIt->second.music, -1);
    this->startTime = std::chrono::high_resolution_clock::now();
    this->lastTrackDurationPlayed = std::chrono::duration<double>(0.0);
    this->lastTrackPlayed = trackIt->second.music;
    return true;
}

bool MusicManager::playRandomTrack(const std::string& set)
{
    std::vector<std::string> trackNames = this->getTrackNames(set);
    if (trackNames.empty() == true)
    {
        return false;
    }

    std::string track = trackNames[rand() % trackNames.size()];
    return this->playTrack(track);
}

const std::vector<std::string>& MusicManager::getSetNames() const
{
    return this->setNames;
}

std::vector<std::string> MusicManager::getTrackNames(const std::string& set) const
{
    std::vector<std::string> trackNames;

    for (auto trackIt : this->tracks)
    {
        Track& track = trackIt.second;
        if (track.setName == set)
        {
            trackNames.push_back(trackIt.first);
        }
    }

    return trackNames;
}

void MusicManager::parseXml(const pugi::xml_node& musicNode)
{
    for (pugi::xml_node setNode : musicNode.children()) {
        if (setNode.type() == pugi::xml_node_type::node_element)
        {
            std::string setName = setNode.name();
            this->setNames.push_back(setName);
            LOGD("MusicManager: Read set %s\n", setName.c_str());

            for (pugi::xml_node trackNode : setNode.children("track"))
            {
                pugi::xml_attribute fileAtribute = trackNode.attribute("file");
                if (!fileAtribute)
                {
                    LOGE("Track with no file attribute.\n");
                }

                std::string trackName = trackNode.child_value();

                Track track;
                track.filePath = fileAtribute.as_string();
                track.music = nullptr;
                track.setName = setName;

                this->tracks[trackName] = track;
                LOGD("MusicManager: Read track %s\n", trackName.c_str());
            }
        }
    }
}

MusicManager* MusicManager::getInstance()
{
    return instance;
}

void MusicManager::create()
{
	if (instance == nullptr) {
		instance = new MusicManager();
	} else {
		LOGE("Error creating MusicManager: Already instantiated.\n");
	}
}

void MusicManager::destroy()
{
	if (instance != nullptr) {
		delete instance;
		instance = nullptr;
	} else {
		LOGE("Error destroying MusicManager: Not instantiated.\n");
	}
}
