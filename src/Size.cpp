//
//  Size.cpp
//  ultragame
//
//  Created by Asbjoern Sporaland on 9/29/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#include "Size.h"

Size::Size() {
    this->width = 0.0f;
    this->height = 0.0f;
}

Size::Size(float width, float height) {
    this->width = width;
    this->height = height;
}

Size::~Size() {
    
}