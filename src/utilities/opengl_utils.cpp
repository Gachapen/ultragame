#include "opengl_utils.h"

#include <vector>
#include <cassert>
#include <cstdlib>
//#include "../system_logger.h"
//#include "../Logger.h"
#include "../log.h"

bool linkShaderProgram(GLuint shaderProgram, GLuint vertexShader, GLuint fragmentShader)
{
    if (shaderProgram == 0) {
        LOGE("Creating shader program failed: Could not create gl shader program.\n");
        return false;
    }

    if (vertexShader == 0 || fragmentShader == 0)
    {
        return false;
    }

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);

    if (checkLinkStatus(shaderProgram) == false)
    {
        return false;
    }

    if (vertexShader)
    {
        glDetachShader(shaderProgram, vertexShader);
        glDeleteShader(vertexShader);
    }
    if (fragmentShader)
    {
        glDetachShader(shaderProgram, fragmentShader);
        glDeleteShader(fragmentShader);
    }

    return true;
}

GLuint createShader(const std::string& shaderContent, GLenum shaderType)
{
    GLuint shaderId = glCreateShader(shaderType);
    if (shaderId == 0)
    {
        LOGE("glCreateShader() failed.");
        return 0;
    }

    const GLchar* shaderCString = shaderContent.c_str();
    glShaderSource(shaderId, 1, &shaderCString, nullptr);

    glCompileShader(shaderId);

    if (checkCompileStatus(shaderId) == false)
    {
        return 0;
    }

    return shaderId;
}

GLuint createShader(std::fstream& shaderFile, GLenum shaderType)
{
    std::string shaderString;
    shaderString.assign(std::istreambuf_iterator<char>(shaderFile), std::istreambuf_iterator<char>());

    return createShader(shaderString, shaderType);
}

std::string generateGlslVersionString(const std::string& requiredVersionString, GLuint currentGlMajor, GLuint currentGlMinor)
{
	int requiredGlslVersion = std::atoi(requiredVersionString.c_str());
	assert(requiredGlslVersion > 110 && requiredGlslVersion < 500);

	std::string currentGlslVersionString("000\n");

	if (currentGlMajor >= 3 && currentGlMinor >= 3)
	{
		currentGlslVersionString[0] = (char)(((int)'0') + currentGlMajor);
		currentGlslVersionString[1] = (char)(((int)'0') + currentGlMajor);
	}
	else {
		currentGlslVersionString[0] = '1';
		if (currentGlMajor == 3)
		{
			currentGlslVersionString[1] = (char)(((int)'0') + currentGlMajor + 3);
		}
		else
		{
			currentGlslVersionString[1] = (char)(((int)'0') + currentGlMinor + 1);
		}
	}

	int currentGlslVersion = std::atoi(currentGlslVersionString.c_str());

	if (currentGlslVersion < requiredGlslVersion)
	{
		LOGE("generateGlslVersionString: Current version (%i) lower than required version (%i).", currentGlslVersion, requiredGlslVersion);
		return "";
	}

	return "#version " + currentGlslVersionString;
}

GLuint createShaderAutoVersion(std::fstream& shaderFile, GLenum shaderType, GLuint currentGlMajor, GLuint currentGlMinor)
{
	std::string shaderString;
	shaderString.assign(std::istreambuf_iterator<char>(shaderFile), std::istreambuf_iterator<char>());

	// TODO: This parsing could probably be made better.
	std::string::size_type specCommentPos = shaderString.find("//?");
	if (specCommentPos == 0) // Currently comment needs to be the very first thing.
	{
		std::string::size_type lineEndPos = shaderString.find_first_of('\n');
		assert(lineEndPos != std::string::npos);

		std::string firstLine = shaderString.substr(specCommentPos, lineEndPos - specCommentPos);
		std::string required = "required";
		std::string::size_type requiredVersionPos = firstLine.find(required);
		assert(requiredVersionPos != std::string::npos);

		std::string::size_type versionPos = requiredVersionPos + required.size() + 1;
		std::string commentVersionString = firstLine.substr(versionPos, firstLine.size() - versionPos);

		std::string glslVersionLine = generateGlslVersionString(commentVersionString, currentGlMajor, currentGlMinor);
		if (glslVersionLine == "")
		{
			return 0;
		}

		shaderString.insert(0, glslVersionLine);
	}

	return createShader(shaderString, shaderType);
}

bool checkLinkStatus(GLuint program)
{
    GLint status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);

    if (status == GL_FALSE) {

        GLint length;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);

        std::vector<char> log(length);
        glGetProgramInfoLog(program, length, &length, &log[0]);

        std::string errorMessage = "Shader program linking error: " + std::string(&log.front());

        LOGE("%s %s",errorMessage.c_str(), &log[0]);
        return false;
    }

    return true;
}

bool checkCompileStatus(GLuint shaderId)
{
    GLint status;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        GLint length;
        glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &length);

        std::vector<char> log(length);
        glGetShaderInfoLog(shaderId, length, &length, &log[0]);

        LOGE("Shader compilation error: %s", std::string(&log.front()).c_str());
        return false;
    }

    return true;
}
