/**
 * @file opengl_utils.h
 * @brief OpenGL utility functions.
 */

#ifndef OPENGL_UTILS_H_
#define OPENGL_UTILS_H_

#include <string>
#include <fstream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengles2.h>

/**
 * Create and compile shader from the content in the provided string.
 * @param shaderContent The shader code to compile.
 * @param shaderType The shader type to compile.
 * @return The ID of the created shader. 0 if failed.
 */
GLuint createShader(const std::string& shaderContent, GLenum shaderType);

/**
 * Create and compile shader from the content in the provided file stream.
 * @param shaderFile File stream with code to compile.
 * @param shaderType The shader type to compile.
 * @return The ID of the created shader. 0 if failed.
 */
GLuint createShader(std::fstream& shaderFile, GLenum shaderType);

/**
 * Generate best possible GLSL version string from the comment and current GL version.
 * The string will be of the format "#version xxx\n".
 * @param requiredVersionString The string for the version required ("xxx")
 * @param currentGlMajor The current GL major version.
 * @param currentGlMinor The current GL minor version.
 * @return The GLSL version string.
 */
std::string generateGlslVersionString(const std::string& requiredVersionString, GLuint currentGlMajor, GLuint currentGlMinor);

/**
 * Create and compile shader from the content in the provided file stream and automatically find version.
 * Does the same as createShader(std::fstream& shaderFile, GLenum shaderType)
 * but will automatically find the best GLSL version from the comment "//? required xxx"
 * @param shaderFile File stream with code to compile.
 * @param shaderType The shader type to compile.
 * @param currentGlMajor The current GL major version.
 * @param currentGlMinor The current GL minor version.
 * @return The ID of the created shader. 0 if failed.
 */
GLuint createShaderAutoVersion(std::fstream& shaderFile, GLenum shaderType, GLuint currentGlMajor, GLuint currentGlMinor);

/**
 * Link shader program from vertex and fragment shaders.
 * @param shaderProgram Program to link to. Must be created beforehand and not be 0.
 * @param vertexShader Vertex shader to link.
 * @param fragmentShader Fragment shader to link.
 * @return true if linking succeeded, otherwise false.
 */
bool linkShaderProgram(GLuint shaderProgram, GLuint vertexShader, GLuint fragmentShader);

/**
 * Check the linking status of program and log errors.
 * @param program Program to check.
 * @return true if OK, false if not.
 */
bool checkLinkStatus(GLuint program);

/**
 * Check compile status of shader and log errors.
 * @param shaderId Shader to check.
 * @return true if OK, false if not.
 */
bool checkCompileStatus(GLuint shaderId);

#endif /* OPENGL_UTILS_H_ */
