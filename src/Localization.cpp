#include "Localization.h"

#include "log.h"
#include "GeneralFileSaver.h"

Localization* Localization::instance = nullptr;

Localization::Localization() :
		strings(this),
		currentLocale("en"),
		defaultLocale("en")
{
}

Localization::~Localization()
{
}

void Localization::setLocale(const std::string& locale)
{
	this->currentLocale = locale;
}

void Localization::setDefaultLocale(const std::string& locale)
{
	this->defaultLocale = locale;
}

bool Localization::loadLocalization(const std::string& localeFilePath)
{
	GeneralFileSaver fileLoader;
	std::vector<unsigned char> fileData = fileLoader.readFile(localeFilePath);

	if (fileData.size() == 0) {
		LOGE("Localization: Error: Got 0 bytes from locale file %s\n", localeFilePath.c_str());
		return false;
	}

	pugi::xml_document document;
	document.load_buffer(fileData.data(), fileData.size());

	pugi::xml_node localesNode = document.child("locales");
	if (!localesNode) {
		LOGE("Could not find XML locales node.\n");
	}

	for (pugi::xml_node localeNode : localesNode.children("locale")) {
		this->parseLocale(localeNode);
	}

	return true;
}

bool Localization::parseLocale(const pugi::xml_node& localeNode)
{
	pugi::xml_attribute localeNameAttribute = localeNode.attribute("name");
	if (!localeNameAttribute) {
		return false;
	}

	std::string localeName = localeNameAttribute.as_string();

	for (pugi::xml_node stringNode : localeNode.children("string")) {
		pugi::xml_attribute stringNameAttribute = stringNode.attribute("name");

		if (stringNameAttribute) {
			std::string stringName = stringNameAttribute.as_string();
			std::string stringValue = stringNode.child_value();
			this->strings.add(localeName, stringName, stringValue);
			LOG("Added string: %s %s %s\n", localeName.c_str(), stringName.c_str(), stringValue.c_str());
		}
	}

	return true;
}

Localization::Strings::Strings(Localization* localization) :
		localization(localization)
{
}

std::string Localization::Strings::get(const std::string& stringName)
{
	const StringMap* preferredStringMap = this->getPreferredStringMap();
	if (preferredStringMap == nullptr) {
		LOGE("Did not find strings for current locale (%s) or default locale (%s)",
			this->localization->currentLocale.c_str(),
			this->localization->defaultLocale.c_str());
		return "";
	}

	auto stringIt = preferredStringMap->find(stringName);
	if (stringIt == preferredStringMap->end())
	{
		preferredStringMap = this->getDefaultLocaleStringMap();

		stringIt = preferredStringMap->find(stringName);
		if (stringIt == preferredStringMap->end())
		{
			LOGE("Did not find the string \"%s\" in the current (%s) or default (%s) locale",
				stringName.c_str(),
				this->localization->currentLocale.c_str(),
				this->localization->defaultLocale.c_str());
			return "";
		}
	}

	return stringIt->second;
}

void Localization::Strings::add(const std::string& locale, const std::string& stringName, const std::string& stringValue)
{
	this->localeMap[locale][stringName] = stringValue;
}

const Localization::Strings::StringMap* Localization::Strings::getPreferredStringMap() const
{
	auto localeMapIt = this->localeMap.find(this->localization->currentLocale);
	if (localeMapIt == this->localeMap.end()) {
		return this->getDefaultLocaleStringMap();
	} else {
		return &(localeMapIt->second);
	}
}

const Localization::Strings::StringMap* Localization::Strings::getDefaultLocaleStringMap() const
{
	auto localeMapIt = this->localeMap.find(this->localization->defaultLocale);
	if (localeMapIt == this->localeMap.end()){
		return nullptr;
	} else {
		return &(localeMapIt->second);
	}
}

Localization* Localization::getInstance()
{
	return instance;
}

void Localization::create()
{
	if (instance == nullptr) {
		instance = new Localization();
	} else {
		LOGE("Error creating Localization: Already instantiated.\n");
	}
}

void Localization::destroy()
{
	if (instance != nullptr) {
		delete instance;
		instance = nullptr;
	} else {
		LOGE("Error destroying Localization: Not instantiated.\n");
	}
}
