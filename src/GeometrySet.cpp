#include "GeometrySet.h"

#include <algorithm>
#include "log.h"
#include "Assets.h"

GeometrySet::GeometrySet(GeometryType type):
	active(true),
	geometryType(type)
{
}

Triangle* GeometrySet::createTriangle()
{
	assert(this->geometryType == GeometryType::TRIANGLE);

	std::unique_ptr<Triangle> uniqueGeometry(new Triangle(this));
	Triangle* geometryPointer = uniqueGeometry.get();

	this->addGeometry(std::move(uniqueGeometry));

	return geometryPointer;
}

Quad* GeometrySet::createQuad()
{
	assert(this->geometryType == GeometryType::QUAD);

    
    Sprite sprite = Assets::getInstance()->spriteManager->getSprite("white.png");
    
	std::unique_ptr<Quad> uniqueGeometry(new Quad(this, sprite));
	Quad* geometryPointer = uniqueGeometry.get();

	this->addGeometry(std::move(uniqueGeometry));

	return geometryPointer;
}

Line* GeometrySet::createLine()
{
	assert(this->geometryType == GeometryType::LINE);

	std::unique_ptr<Line> uniqueGeometry(new Line(this));
	Line* geometryPointer = uniqueGeometry.get();

	this->addGeometry(std::move(uniqueGeometry));

	return geometryPointer;
}

Polygon* GeometrySet::createPolygon(unsigned int numVertices)
{
	assert(this->geometryType == GeometryType::POLYGON);

    std::unique_ptr<Polygon> uniqueGeometry(new Polygon(this, numVertices));
    Polygon* geometryPointer = uniqueGeometry.get();

    this->addGeometry(std::move(uniqueGeometry));

    return geometryPointer;
}

void GeometrySet::removeGeometry(Geometry *geometryToBeRemoved)
{    
    for (auto it = this->geometry.begin(); it != this->geometry.end(); it++)
    {
        if (it->get() == geometryToBeRemoved)
        {
            this->onGeometryRemove(geometryToBeRemoved);
            auto newEndIt = std::remove(this->geometry.begin(), this->geometry.end(), *it);
            it = this->geometry.erase(newEndIt);
            this->updateStartPositionMap();

            return;
        }
    }
    
    
//	this->geometry.erase(newEndIt);
}

void GeometrySet::onGeometryChanged(Geometry* geometry)
{
	std::vector<ObjectCoordinate> updatedCoordinates = geometry->generateRenderableVertices();
	unsigned int startPos = this->geometryRenderDataStart[geometry];

	for (size_t i = 0; i < updatedCoordinates.size(); i++)
	{
		this->renderVertices[startPos + i] = updatedCoordinates[i];
	}
}

void GeometrySet::onGeometryRemove(Geometry* geometry)
{
    unsigned numberOfVerticesInGeometry = geometry->getNumVertices();
    unsigned startPosition = this->geometryRenderDataStart[geometry];
    
    auto startRemove = this->renderVertices.begin() + startPosition;
    auto endRemove = startRemove + numberOfVerticesInGeometry;
    this->renderVertices.erase(startRemove, endRemove);
}

void GeometrySet::updateStartPositionMap()
{
    this->geometryRenderDataStart.clear();
    
    unsigned startPosition = 0;
    
    for (auto it = this->geometry.begin(); it != this->geometry.end(); it++)
    {
        this->geometryRenderDataStart[it->get()] = startPosition;
        startPosition += (*it)->getNumVertices();
    }
    
}

unsigned int GeometrySet::getNumRenderObjects() const
{
    unsigned int totalNumber = 0;
    for (const auto& geometry : this->geometry)
    {
        totalNumber += geometry->getNumRenderObjects();
    }

    return totalNumber;
}

void GeometrySet::addGeometry(std::unique_ptr<Geometry> geometry)
{
	unsigned int renderDataSize = this->renderVertices.size() + geometry->getNumVertices();
	this->geometryRenderDataStart[geometry.get()] = this->renderVertices.size();
	this->renderVertices.resize(renderDataSize);

	this->geometry.push_back(std::move(geometry));
}

GeometryType GeometrySet::getGeometryType() const
{
	return this->geometryType;
}

const std::vector<ObjectCoordinate>& GeometrySet::getVertexData() const
{
	return this->renderVertices;
}

unsigned int GeometrySet::getNumObjects() const
{
	return this->geometry.size();
}

void GeometrySet::setActive(bool state)
{
	this->active = state;
}

bool GeometrySet::isActive() const
{
	return this->active;
}
