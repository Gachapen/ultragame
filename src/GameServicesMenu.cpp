#include "GameServicesMenu.h"

GameServicesMenu::GameServicesMenu():
	quadHandler(nullptr),
	shown(false),
	mainMenuDim(0.8)
{
}

GameServicesMenu::~GameServicesMenu()
{
	this->removeOnClickListener(this);
}

void GameServicesMenu::init(GeometrySet* quadHandler)
{
	this->gameServiceMenu = std::make_shared<View>();
	this->gameServiceMenu->setRenderQuad(quadHandler->createQuad());
	this->gameServiceMenu->setClickable(true);
	this->gameServiceMenu->setDefaultColor(SDL_Color { 255, 255, 255, 50 });
	this->gameServiceMenu->setPressedColor(SDL_Color{255, 255, 255, 50});

	this->setDefaultColor(SDL_Color{0, 0, 0, 0});
	this->setPressedColor(SDL_Color{0, 0, 0, 0});
	this->addChildView(this->gameServiceMenu);

	this->popupDuration = std::chrono::duration<float>(0.15f);

	this->addOnClickListener(this);

	this->setupUi(this->gameServiceMenu, quadHandler);
}

bool GameServicesMenu::isShown() const
{
	return this->shown;
}

void GameServicesMenu::show()
{
	if (this->isShown() == false) {
		int hiddenPos = this->getRect().y + this->getRect().h;
		int shownPos = hiddenPos - this->gameServiceMenu->getRect().h;

		std::unique_ptr<Transition> opacityTransition(new Transition());
		opacityTransition->setValues(0.0f, this->mainMenuDim);

		std::unique_ptr<Transition> slideTransition(new Transition());
		slideTransition->setValues((float)hiddenPos, (float)shownPos);
		slideTransition->setSuccessCallback([this]{ this->shown = true; });

		slideTransition->setDuration(this->popupDuration);
		slideTransition->setUpdateCallback(
			[this](float position) {
				this->gameServiceMenu->setVerticalPosition((int)position);
			}
		);

		opacityTransition->setDuration(this->popupDuration);
		opacityTransition->setUpdateCallback(
			[this](float opacity) {
				SDL_Color color = this->getColor();
				color.a = (Uint8)(opacity * 255);
				this->setDefaultColor(color);
				this->setPressedColor(color);
				this->setCurrentColor(color);
			}
		);

		this->processManager.startProcess(std::move(slideTransition));
		this->processManager.startProcess(std::move(opacityTransition));

		this->setClickable(true);
	}
}

void GameServicesMenu::hide()
{
	if (this->isShown() == true) {
		int hiddenPos = this->getRect().y + this->getRect().h;
		int shownPos = hiddenPos - this->gameServiceMenu->getRect().h;

		std::unique_ptr<Transition> opacityTransition(new Transition());
		opacityTransition->setValues(this->mainMenuDim, 0.0f);

		std::unique_ptr<Transition> slideTransition(new Transition());
		slideTransition->setValues((float)shownPos, (float)hiddenPos);
		slideTransition->setSuccessCallback([this]{ this->shown = false; });

		slideTransition->setDuration(this->popupDuration);
		slideTransition->setUpdateCallback(
			[this](float position) {
				this->gameServiceMenu->setVerticalPosition((int)position);
			}
		);

		opacityTransition->setDuration(this->popupDuration);
		opacityTransition->setUpdateCallback(
			[this](float opacity) {
				SDL_Color color = this->getColor();
				color.a = (Uint8)(opacity * 255);
				this->setDefaultColor(color);
				this->setPressedColor(color);
				this->setCurrentColor(color);
			}
		);

		this->processManager.startProcess(std::move(slideTransition));
		this->processManager.startProcess(std::move(opacityTransition));

		this->setClickable(false);
	}
}

void GameServicesMenu::onAttachToParent(View* parent)
{
	SDL_Rect parentRect = parent->getRect();

	this->setWidthPixels(parentRect.w);
	this->setHeightPixels(parentRect.h);

	this->gameServiceMenu->setWidthPixels(parentRect.w);
	this->gameServiceMenu->setHeightPercentage(30.0f);
	this->gameServiceMenu->setPositioningTypeVertical(View::Position::PIXELS);
	this->gameServiceMenu->setVerticalPosition(parentRect.y + parentRect.h);
}

void GameServicesMenu::onClick(const View& clickedElement)
{
	if (clickedElement == *this) {
		this->hide();
	}
}

void GameServicesMenu::onUpdate(float deltaTime)
{
	this->processManager.updateProcesses(deltaTime);
}

void GameServicesMenu::updatePositioning()
{
	this->gameServiceMenu->setVerticalPosition(this->getParent()->getRect().y + this->getParent()->getRect().h);
	View::updatePositioning();
}
