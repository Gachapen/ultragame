#ifndef GEOMETRYSET_H_
#define GEOMETRYSET_H_

#include <memory>
#include <unordered_map>
#include "Geometry.h"

/**
 * A set of geometries used for rendering.
 */
class GeometrySet
{
public:
	/**
	 * Construct a GeometrySet containing geometry of the specified type.
	 * No other geometry types can be added to this set.
	 * @param type Geometry type to keep in this set.
	 */
	GeometrySet(GeometryType type);
	
	/**
	 * Create a triangle managed by this set.
	 * @return Pointer to the geometry created.
	 */
	Triangle* createTriangle();
	
	/**
	 * Create a quad managed by this set.
	 * @return Pointer to the geometry created.
	 */
	Quad* createQuad();
	
	/**
	 * Create a line managed by this set.
	 * @return Pointer to the geometry created.
	 */
	Line* createLine();
	
	/**
	 * Create a polygon managed by this set.
	 * @param numVertices Number of vertices in the polygon.
	 * @return Pointer to the geometry created.
	 */
	Polygon* createPolygon(unsigned int numVertices);
	
	/**
	 *  Removes the Geometry from the set.
	 *  If the passed pointer is nullptr, nothing is done.
	 */
	void removeGeometry(Geometry* geometryToBeRemoved);
	
	GeometryType getGeometryType() const;
	
	/**
	 * Get the vertex data in this set used for rendering.
	 * @return Vertex data.
	 */
	const std::vector<ObjectCoordinate>& getVertexData() const;
	
	/**
	 * Get the umber of geometry objects in this set.
	 */
	unsigned int getNumObjects() const;
	
	/**
	 * Get the number of render objects in this set.
	 * See Geometry::getNumRenderObjects().
	 */
	unsigned int getNumRenderObjects() const;
	
	/**
	 * Called when one of the geometries in this set changes its vertices.
	 * Update this set's representation of the vertices.
	 * @param geometry Geometry changed.
	 */
	void onGeometryChanged(Geometry* geometry);
	
	/**
	 * Called when one of the geometries is removed from the set.
	 * Updates the set's representation of the vertices.
	 * @param geometry The Geometry to be removed.
	 */
	void onGeometryRemove(Geometry* geometry);
        
	/**
	 * Set if the set is active or not.
	 * @param state Active or not.
	 */
	void setActive(bool state);
	
	/**
	 * Check if the set is active.
	 * The active state is an indication for the user of this instance
	 * if it should use it or not at the moment.
	 * E.g. the renderer renders it or don't.
	 */
	bool isActive() const;
	
private:
	void addGeometry(std::unique_ptr<Geometry> geometry);
	void updateStartPositionMap();
	
	bool active;
	
	GeometryType geometryType;
	std::vector<std::unique_ptr<Geometry>> geometry;
	std::vector<ObjectCoordinate> renderVertices;
	std::unordered_map<Geometry*, unsigned int> geometryRenderDataStart;
};

#endif /* GEOMETRYSET_H_ */
