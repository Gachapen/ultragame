#ifndef BLACKHOLE_H_
#define BLACKHOLE_H_

#include <glm/glm.hpp>

class SimplePhysics;
class PhysicsBody;

class BlackHole
{
public:
	BlackHole(SimplePhysics* physics);

	enum class GravitationCalculation
	{
		NEWTON,
		SIMPLE
	};

	PhysicsBody* getBody() const;
	glm::vec2 calculateGravitationalEffect(const PhysicsBody* otherBody, GravitationCalculation = GravitationCalculation::NEWTON) const;

private:
	PhysicsBody* body;
};

#endif /* BLACKHOLE_H_ */
