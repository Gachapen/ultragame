#ifndef TRANSITION_H_
#define TRANSITION_H_

#include <chrono>
#include <functional>
#include "process/ProcessImplementation.h"

/**
 * Transition from one value to another value over time.
 */
class Transition: public ProcessImplementation
{
public:
	Transition();
	virtual ~Transition();

	void onInit() override;
	void onUpdate(float deltaTime) override;
	void onSuccess() override;
	void onFail() override;
	void onAbort() override;

	/**
	 * Set the values to transition from and to.
	 * @param start Value to transition from.
	 * @param end Value to transition to.
	 */
	void setValues(float start, float end);

	/**
	 * Set the duration of the transition.
	 * @param duration The duration.
	 */
	void setDuration(std::chrono::duration<float> duration);

	/**
	 * Set a function to be called when the value changes.
	 * The callback function has a float as a parameter which represents
	 * the current value in the transition.
	 * @param callback Function to be called.
	 */
	void setUpdateCallback(std::function<void (float)> callback);

	/**
	 * Set a function to be called when the transition is done.
	 * @param transitionDoneCallback Function called when transition is complete.
	 */
	void setSuccessCallback(std::function<void ()> transitionDoneCallback);

	/**
	 * Checks if a transition is in progress
	 * @returns wether or not a transition is in progress.
	 */
	bool isTransitioning() const;

private:
	bool transitioning;

	float startValue;
	float endValue;
	float currentValue;
	float direction;
	float valuesPerSecond;
	std::chrono::duration<float> duration;
	std::chrono::high_resolution_clock::time_point startTime;

	std::function<void (float)> updateCallback;
	std::function<void ()> doneCallback;
};

#endif /* TRANSITION_H_ */
