#include "BlackHole.h"

#include "physics/SimplePhysics.h"
#include "physics/PhysicsBody.h"
#include "log.h"

BlackHole::BlackHole(SimplePhysics* physics)
{
	this->body = physics->createBody();
}

PhysicsBody* BlackHole::getBody() const
{
	return this->body;
}

glm::vec2 BlackHole::calculateGravitationalEffect(const PhysicsBody* otherBody, GravitationCalculation calculation) const
{
	const float distance = glm::distance(this->body->getPosition(), otherBody->getPosition());
	float force = 0.0f;

	if (distance > 0.1f)
	{
		if (calculation == GravitationCalculation::NEWTON)
		{
			force = (GRAVITATIONAL_CONSTANT * this->body->getMass() * otherBody->getMass()) / (distance * distance);
		}
		else if (calculation == GravitationCalculation::SIMPLE)
		{
			float clampedDistance = glm::clamp(distance, 400.0f, 500000.0f);
			force = (GRAVITATIONAL_CONSTANT * this->body->getMass() * otherBody->getMass()) / pow(clampedDistance, 2.0f);
//			LOG("Distance: %f. Force: %f\n", distance, force);
		}
	}

	glm::vec2 forceVector = glm::normalize(this->body->getPosition() - otherBody->getPosition()) * force;

	return forceVector;
}
