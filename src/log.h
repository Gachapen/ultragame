#ifndef LOG_H_
#define LOG_H_

#ifdef __ANDROID__
#include <android/log.h>
#define LOG(...) ((void)__android_log_print(ANDROID_LOG_INFO, "ULTRAGAME_C++", __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "ULTRAGAME_C++", __VA_ARGS__))
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "ULTRAGAME_C++", __VA_ARGS__))
#else
#include <stdio.h>
#define LOG(...) ((void)printf(__VA_ARGS__))
#define LOGE(...) ((void)fprintf(stderr, __VA_ARGS__))
#define LOGD(...) ((void)printf(__VA_ARGS__))
#endif

#endif
