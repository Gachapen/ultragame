//
//  GameCenter.h
//  ultragame
//
//  Created by Asbjoern Sporaland on 24/10/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "/Users/Sporaland/Documents/SDL/src/video/uikit/SDL_uikitappDelegate.h"
#import "/Users/Sporaland/Documents/SDL/src/video/uikit/SDL_uikitwindow.h"


@interface ViewHandler : NSObject

/**
 * Gets the ViewController used by SDL to present the window.
 * @param window the SDL window.
 * @returns The ViewController used by SDL to present to screen.
 */
+ (SDL_uikitviewcontroller *)getViewFrom:(SDL_Window*) window;

@end
