//
//  GameCenterAndGooglePlayHandler.h
//  ultragame
//
//  Created by Asbjoern Sporaland on 10/22/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#ifndef __ultragame__GameCenterAndGooglePlayHandler__
#define __ultragame__GameCenterAndGooglePlayHandler__





#ifdef __cplusplus // This is a straight C function we can call from Objective-C
extern "C" {
#endif
	void googlePlaySignedInCallback();
	
#ifdef __cplusplus
}
#endif


#ifdef __cplusplus // The Class below should only be availible in c++.

#include <SDL2/SDL.h>
#include <functional>
#include "PlayGamesServiceMenu.h"

/**
 * Handles Signing into and out of google play games and apple game center.
 * This class provides an interface for the c++ objects to call the Objective-C code needed to 
 * contact google or apple.
 */
class GameCenterAndGooglePlayHandler
{
public:
	
	/**
	 * Gives the Delegate a pointer to the window that is setup and managed by SDL.
	 * This is needed to display the windows from google play and game center.
	 */
	static void setWindow(SDL_Window* window);
	static void signInGameCenterPlayer();
	static void showGameCenterLeaderBoard();
	static void submitGameCenterScore(int score);
	
	static void initGooglePlus();
	static void signInGooglePlusPlayer();
	static void signOutGooglePlusPlayer();
	static bool checkIfSignedInToGooglePlus();
	static void submitGoogleScore(int score);
	static void unlockAchievement(std::string identifier);
	static void showGooglePlayLeaderBoard();
	static void showGooglePlayAchievements();
	static void googlePlayGamesHandlerSignedIn();
	
	/**
	 * Used to set the callback used when google Play reports that it has signed in.
	 */
	static void setGooglePlayGamesSignedInCallback(std::function<void()> callBack);
	
private:
	
	/**
	 * Called when google play reports that the player is signed in.
	 */
	static std::function<void()> googlePlayOnSignIn;
	
	/**
	 * Holds the acheievement Ids
	 */
	static std::map<std::string,std::string> achievementIds;
	
	
	
};

#endif






#endif /* defined(__ultragame__GameCenterAndGooglePlayHandler__) */
