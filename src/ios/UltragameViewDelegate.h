//
//  UltragameViewDelegate
//  ultragame
//
//  Created by Asbjoern Sporaland on 24/10/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import <GooglePlus/GooglePlus.h>
#import <PlayGameServices/PlayGameServices.h>


#import <SDL2/SDL.h>

#import "/Users/Sporaland/Documents/SDL/src/video/uikit/SDL_uikitwindow.h"
#import "/Users/Sporaland/Documents/SDL/src/video/uikit/SDL_uikitappDelegate.h"



/**
 * Used to communicate with the services provided by google and apple.
 */
@interface UltragameViewDelegate : SDLUIKitDelegate <	GKGameCenterControllerDelegate,
							UIAlertViewDelegate,
							GPPSignInDelegate,
							GPGLeaderboardControllerDelegate,
							GPGAchievementControllerDelegate> {
}

/**
 * Gets the singleton instance of this class.
 * @returns The singleton.
 */
+ (UltragameViewDelegate *)getInstance;

/**
 * Gives the Delegate a pointer to the window that is setup and managed by SDL.
 * This is needed to display the windows from google play and game center.
 * @param gameWindow The SDL_Window used by SDL.
 */
+ (void) setWindow:(SDL_Window*) gameWindow;

/**
 * Presents the Game Center leaderboard as a modal ViewController over the current view.
 */
+ (void) showGameCenterLeaderboard;

/**
 * Presents the Google Play leaderboard as a modal ViewController over the current view.
 */
+ (void) showGooglePlayLeaderboard;

/**
 * Presents the Google Play achievements as a modal ViewController over the current view.
 */
+ (void) showGooglePlayAchievements;


+ (void) signInGameCenterPlayer;
+ (void) saveGameCenterScore: (int) score withIdentifier:(NSString *) identifier;
+ (void) saveGooglePlusScore:(int)score;
+ (void) unlockAchievement:(NSString *) identifier;

+ (void) initGooglePlus;
+ (void) googlePlusSignIn;
+ (void) googlePlusSignOut;
+ (BOOL) checkIfSignedInToGooglePlus;
@end



