//
//  GameCenter.m
//  ultragame
//
//  Created by Asbjoern Sporaland on 24/10/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#import "ViewHandler.h"





@implementation ViewHandler

+ (SDL_uikitviewcontroller *)getViewFrom:(SDL_Window*) window {
    
    SDL_uikitviewcontroller *gViewController = NULL;
    SDLUIKitDelegate *gApplicationDelegate = NULL;

    SDL_WindowData *data = (SDL_WindowData *)window->driverdata;
    gViewController = data->viewcontroller;
    gApplicationDelegate = [SDLUIKitDelegate sharedAppDelegate];
    
    return gViewController;
}

@end
