//
//  GameCenterAndGooglePlayHandler.cpp
//  ultragame
//
//  Created by Asbjoern Sporaland on 10/22/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#include "GameCenterAndGooglePlayHandler.h"

#import "UltragameViewDelegate.h"

#import <map>
#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

std::function<void()> GameCenterAndGooglePlayHandler::googlePlayOnSignIn;
std::map<std::string, std::string> GameCenterAndGooglePlayHandler::achievementIds;

void GameCenterAndGooglePlayHandler::signInGameCenterPlayer()
{
	[UltragameViewDelegate signInGameCenterPlayer];
}

void GameCenterAndGooglePlayHandler::showGameCenterLeaderBoard()
{
	[UltragameViewDelegate showGameCenterLeaderboard];
}

void GameCenterAndGooglePlayHandler::submitGameCenterScore(int score)
{
	[UltragameViewDelegate saveGameCenterScore:score withIdentifier:@"high01"];
}

void GameCenterAndGooglePlayHandler::setWindow(SDL_Window* window)
{
	[UltragameViewDelegate setWindow:window];
}

void GameCenterAndGooglePlayHandler::initGooglePlus()
{
	[UltragameViewDelegate initGooglePlus];
	
	achievementIds["achievement_meh"] = "CggIkbn2g0IQAhAN";
	achievementIds["achievement_nice"] = "CggIkbn2g0IQAhAM";
	achievementIds["achievement_cool"] = "CggIkbn2g0IQAhAL";
	achievementIds["achievement_awesome"] = "CggIkbn2g0IQAhAK";
	achievementIds["achievement_ultra"] = "CggIkbn2g0IQAhAJ";
}

void GameCenterAndGooglePlayHandler::signInGooglePlusPlayer()
{
	[UltragameViewDelegate googlePlusSignIn];
	
	
}

void GameCenterAndGooglePlayHandler::signOutGooglePlusPlayer()
{
	[UltragameViewDelegate googlePlusSignOut];
}

bool GameCenterAndGooglePlayHandler::checkIfSignedInToGooglePlus()
{
	return [UltragameViewDelegate checkIfSignedInToGooglePlus];
}

void GameCenterAndGooglePlayHandler::submitGoogleScore(int score) {
	
	[UltragameViewDelegate saveGooglePlusScore:score];
}

void GameCenterAndGooglePlayHandler::unlockAchievement(std::string identifier)
{
	NSString* googleAchievementID = [NSString stringWithUTF8String:achievementIds[identifier].c_str()];
	
	[UltragameViewDelegate unlockAchievement: googleAchievementID];
}

void GameCenterAndGooglePlayHandler::showGooglePlayLeaderBoard()
{
	[UltragameViewDelegate showGooglePlayLeaderboard];
}

void GameCenterAndGooglePlayHandler::showGooglePlayAchievements()
{
	[UltragameViewDelegate showGooglePlayAchievements];
}

void GameCenterAndGooglePlayHandler::setGooglePlayGamesSignedInCallback(std::function<void ()> callBack)
{
	googlePlayOnSignIn = callBack;
}

void GameCenterAndGooglePlayHandler::googlePlayGamesHandlerSignedIn()
{
	googlePlayOnSignIn();
}

void googlePlaySignedInCallback()
{
	GameCenterAndGooglePlayHandler::googlePlayGamesHandlerSignedIn();
}




