//
//  SaveToFile.h
//  ultragame
//
//  Created by Asbjoern Sporaland on 10/3/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#ifndef __ultragame__SaveToFile__
#define __ultragame__SaveToFile__

#include "IFileSaver.h"


class IosFileSaver: public IFileSaver
{
public:
	
    bool writeString(const std::string& string, const std::string& filePath) override;
    bool appendString(const std::string& string, const std::string& filePath) override;
    std::vector<unsigned char> readFile(const std::string& filePath) override;
    void deleteFile(const std::string& filePath) override;
    std::string readFileAsString(const std::string& filePath);
    GLuint createTextureFromFile(const std::string& filePath);

};

#endif /* defined(__ultragame__SaveToFile__) */
