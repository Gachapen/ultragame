//
//  UltragameViewDelegate
//  ultragame
//
//  Created by Asbjoern Sporaland on 24/10/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#import "UltragameViewDelegate.h"
#import "ViewHandler.h"

#import "GameCenterAndGooglePlayHandler.h"



@implementation SDLUIKitDelegate (UltragameDelegate)

// pragmas to ignore warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"


// hijack the the SDL_UIKitAppDelegate to use the UIApplicationDelegate we implement here
+(NSString *)getAppDelegateClassName {
	
	return @"UltragameViewDelegate";
}

#pragma clang diagnostic pop



@end


static SDL_Window *window;
static NSString * kClientID = @"17724972177-nnei0dhik08l1atgbm2brkhha3s4ncnl.apps.googleusercontent.com";
static UIViewController *gViewController;





@implementation UltragameViewDelegate

+ (UltragameViewDelegate *)getInstance
{
	static UltragameViewDelegate *gameCenterInstance;
	
	@synchronized(self)
	{
		if (!gameCenterInstance)
		{
			// This is never freed.  Is a singleton a leak?
			gameCenterInstance = [[UltragameViewDelegate alloc] init];
		}
	}
	
	return gameCenterInstance;
}

+ (void) setWindow:(SDL_Window*) gameWindow
{
	window = gameWindow;
}

+ (void)showGameCenterLeaderboard
{
	gViewController = [ViewHandler getViewFrom:window];
	
	GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
	if (gameCenterController != nil)
	{
		gameCenterController.gameCenterDelegate = [self getInstance] ;
		gameCenterController.viewState = GKGameCenterViewControllerStateLeaderboards;
		[(id)gViewController presentModalViewController: gameCenterController animated: YES];
	}
}

+ (void)showGooglePlayLeaderboard {
	
	gViewController = [ViewHandler getViewFrom:window];
	
	GPGLeaderboardController *leadController = [[GPGLeaderboardController alloc] initWithLeaderboardId:@"CggIkbn2g0IQAhAB"];
	leadController.leaderboardDelegate = [self getInstance];
	[(id)gViewController presentModalViewController: leadController animated:YES];
}

+ (void)showGooglePlayAchievements {
	gViewController = [ViewHandler getViewFrom:window];
	
	GPGAchievementController *achController = [[GPGAchievementController alloc] init];
	achController.achievementDelegate = [self getInstance];
	[(id)gViewController presentModalViewController:achController animated:YES];}


+ (void) signInGameCenterPlayer {
	
	GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
	GKLocalPlayer *blockLocalPlayer = localPlayer;
	
	localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
		if (viewController != nil)
		{
			NSLog(@"Not Authenticated");
		}
		else if ([blockLocalPlayer isAuthenticated])
		{
			NSLog(@"Authenticated");
		}
		else
		{
			NSLog(@"Failed to sign in.");
		}
		
		NSLog(@"%@",error);
	};
	
}

+ (void) saveGameCenterScore: (int) score withIdentifier:(NSString *) identifier
{
	
	GKScore *scoreObject = [[GKScore alloc] initWithLeaderboardIdentifier:identifier];
	[scoreObject setValue:score];
	NSArray *scores = [NSArray arrayWithObject:scoreObject];
	[GKScore reportScores:scores withCompletionHandler:nil];
}

+ (void)saveGooglePlusScore:(int)score {
	
	GPGScore *scoreHandler = [[GPGScore alloc] initWithLeaderboardId:@"CggIkbn2g0IQAhAB"];
	scoreHandler.value = score;
	[scoreHandler submitScoreWithCompletionHandler:nil];
	
}

+ (void)unlockAchievement:(NSString *)identifier
{
	GPGAchievement *unlockMe = [GPGAchievement achievementWithId:identifier];
		
	[unlockMe unlockAchievementWithCompletionHandler:^(BOOL newlyUnlocked, NSError *error) {
		if (error) {
			NSLog(@"Error: %@",error);
		} else if (!newlyUnlocked) {
			NSLog(@"Achievement allready unlocked!");
		} else {
			NSLog(@"Hooray! Achievement unlocked!");
		}
	}];
	
}

+ (void)initGooglePlus {
	
	GPPSignIn *signIn = [GPPSignIn sharedInstance];
	signIn.clientID = kClientID;
	signIn.scopes = [NSArray arrayWithObjects:
			 @"https://www.googleapis.com/auth/games",
			 @"https://www.googleapis.com/auth/appstate",
			 nil];
	signIn.language = [[NSLocale preferredLanguages] objectAtIndex:0];
	signIn.delegate = [self getInstance];
	signIn.shouldFetchGoogleUserID =YES;
	[signIn trySilentAuthentication];
	
}

+ (void)googlePlusSignIn
{
	if (![[GPPSignIn sharedInstance] authentication])
	{
		[[GPPSignIn sharedInstance] authenticate];
	}
}

+ (void)googlePlusSignOut {
	
	[[GPPSignIn sharedInstance] signOut];
	[[GPPSignIn sharedInstance] disconnect];
}

+ (BOOL) checkIfSignedInToGooglePlus {
	
	if ([[GPPSignIn sharedInstance] authentication])
	{
		return true;
	}
	else
	{
		return false;
	}
}


- (void) gameCenterViewControllerDidFinish:(GKGameCenterViewController *)viewController {
	
	[gViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)leaderboardViewControllerDidFinish: (GPGLeaderboardController *)viewController {
	
	[gViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)achievementViewControllerDidFinish:(GPGAchievementController *)viewController {
	[gViewController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
	
	return [GPPURLHandler handleURL:url
		      sourceApplication:sourceApplication
			     annotation:annotation];
}

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error
{
	NSLog(@"Finished with auth.");
	if (error == nil && auth) {
		NSLog(@"Success signing in to Google! Auth object is %@", auth);
		
		// Tell your GPGManager that you're ready to go.
		[self startGoogleGamesSignIn];
		googlePlaySignedInCallback();
		
		
	} else {
		NSLog(@"Failed to log into Google\n\tError=%@\n\tAuthObj=%@",error,auth);
	}
}


-(void)startGoogleGamesSignIn
{
	[[GPGManager sharedInstance] signIn:[GPPSignIn sharedInstance]
			 reauthorizeHandler:^(BOOL requiresKeychainWipe, NSError *error)
	 {

		 if (requiresKeychainWipe)
		 {
			 [[GPPSignIn sharedInstance] signOut];
		 }
		 [[GPPSignIn sharedInstance] authenticate];
		 NSLog(@"error: %@",error);
		 
	 }];
}


@end




