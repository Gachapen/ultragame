//
//  IosGetLocale.h
//  ultragame
//
//  Created by Asbjoern Sporaland on 10/4/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#ifndef __ultragame__IosGetLocale__
#define __ultragame__IosGetLocale__

#include "IGetLocale.h"

class IosGetLocale : public IGetLocale
{
public:
    
    std::string getLocale() override;
};

#endif /* defined(__ultragame__IosGetLocale__) */
