//
//  IosGetLocale.cpp
//  ultragame
//
//  Created by Asbjoern Sporaland on 10/4/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "IosGetLocale.h"

std::string IosGetLocale::getLocale()
{
    NSString* language = [[NSLocale preferredLanguages] objectAtIndex:0];
    return std::string([language UTF8String]);
}