//
//  SaveToFile.cpp
//  ultragame
//
//  Created by Asbjoern Sporaland on 10/3/13.
//  Copyright (c) 2013 masb. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>



#include "IosFileSaver.h"


bool IosFileSaver::writeString(const std::string& string, const std::string& filePath)
{
	NSString *content = [NSString stringWithUTF8String:string.c_str()];
	
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithUTF8String:filePath.c_str()]];
	
	
	NSError *err = nil;
	[content writeToFile:fileName atomically:YES encoding:NSUTF8StringEncoding error:&err];
	if(err != nil) {
		//we have an error.
		return false;
	}
    
    return true;
}



bool IosFileSaver::appendString(const std::string& string, const std::string& filePath)
{
    NSString *content = [NSString stringWithUTF8String:string.c_str()];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithUTF8String:filePath.c_str()]];
    
    //create file if it doesn't exist
    if(![[NSFileManager defaultManager] fileExistsAtPath:fileName])
    {
        [[NSFileManager defaultManager] createFileAtPath:fileName contents:nil attributes:nil];
    }
    
    //append text to file (you'll probably want to add a newline every write)
    NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
    [file seekToEndOfFile];
    [file writeData:[content dataUsingEncoding:NSUTF8StringEncoding]];
    [file closeFile];
    
    
    return true;
}


std::vector<unsigned char> IosFileSaver::readFile(const std::string& filePath)
{
    //get file path
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithUTF8String:filePath.c_str()]];
    
    //read the whole file as a single string
    NSString *content = [NSString stringWithContentsOfFile:fileName encoding:NSUTF8StringEncoding error:nil];
    
    if (content == nil)
    {
        content = @"";
    }
    
    std::string string = std::string([content UTF8String]);
    std::vector<unsigned char> data = std::vector<unsigned char>(string.begin(),string.end());
    
    return data;
}

std::string IosFileSaver::readFileAsString(const std::string& filePath)
{
    //get file path
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithUTF8String:filePath.c_str()]];
    
    //read the whole file as a single string
    NSString *content = [NSString stringWithContentsOfFile:fileName encoding:NSUTF8StringEncoding error:nil];
    
    if (content == nil)
    {
        content = @"";
    }
    
    return std::string([content UTF8String]);
}

void IosFileSaver::deleteFile(const std::string& filePath)
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithUTF8String:filePath.c_str()]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:fileName error:NULL];
}

GLuint IosFileSaver::createTextureFromFile(const std::string &filePath)
{
    NSDictionary * options =  [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithBool:YES],
                              GLKTextureLoaderOriginBottomLeft,
                              nil];
    
    NSError * error;
    
    GLKTextureInfo* info = [GLKTextureLoader textureWithContentsOfFile:[NSString stringWithUTF8String:filePath.c_str()]
                                                               options:options
                                                                 error:&error];
    
    return info.name;
}







