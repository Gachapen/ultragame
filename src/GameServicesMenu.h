#ifndef GAMESERVICESMENU_H_
#define GAMESERVICESMENU_H_

#include "gui/View.h"
#include "gui/IOnClickListener.h"
#include "Transition.h"
#include "process/ProcessManager.h"

class GameServicesMenu: public View, public IOnClickListener
{
public:
	GameServicesMenu();
	virtual ~GameServicesMenu();
	
	/**
	 * Initiates the menu.
	 * Passes along the set used for rendering.
	 * @param quadHandler The GeometrySet used to render the menu.
	 */
	void init(GeometrySet* quadHandler);
	
	/**
	 * Checks if the menu is visible.
	 * @returns wether or not the menu is visible.
	 */
	bool isShown() const;
	
	/**
	 * Tells the menu to show itself.
	 */
	void show();
	
	/**
	 * Tells the menu to hide itself.
	 */
	void hide();
	
	/**
	 * Handles the updating of the menu.
	 * For example sliding into/out of place.
	 */
	void onUpdate(float deltaTime);
	
	/**
	 * Handles the menu ataching to the parent view.
	 * @param parent The paren view.
	 */
	void onAttachToParent(View* parent) override;
	
	/**
	 * Handles when an element in the menu has been clicked.
	 * @param clickedElement the element that was clicked.
	 */
	void onClick(const View& clickedElement) override;
	
protected:
	/**
	 * Manages the setup of the elements in the menu.
	 * @param gameServiceMenu The menus view.
	 * @param quadHandler the set used for rendering.
	 */
	virtual void setupUi(const std::shared_ptr<View>& gameServiceMenu, GeometrySet* quadHandler) = 0;

	GeometrySet* quadHandler;

private:
	/**
	 * Updates the positioning of the elements in the menu.
	 */
	void updatePositioning() override;
	
	bool shown;
	std::shared_ptr<View> gameServiceMenu;
	ProcessManager processManager;
	float mainMenuDim;
	
	std::chrono::duration<float> popupDuration;
};

#endif /* GAMESERVICESMENU_H_ */
