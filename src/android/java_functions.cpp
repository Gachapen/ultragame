#include "java_functions.h"

#include "AndroidApplication.h"

JNIEXPORT void JNICALL Java_com_masb_ultragame_UltragameActivity_onNativeSignedIn(JNIEnv* env, jclass clazz)
{
	AndroidApplication::getInstance()->onGameServiceSignIn();
}
