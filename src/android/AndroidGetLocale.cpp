#include "AndroidGetLocale.h"

#include "AndroidApplication.h"

AndroidGetLocale::~AndroidGetLocale()
{
}

std::string AndroidGetLocale::getLocale()
{
	return AndroidApplication::getInstance()->getLocale();
}
