#ifndef ANDROIDACTIVITY_H_
#define ANDROIDACTIVITY_H_

#include <jni.h>
#include <string>
#include <list>
#include <functional>

class AndroidApplication
{
public:
	static AndroidApplication* getInstance();

	void initialize();

	std::string getLocale();
	void finishActivity();
	void gameServiceSignIn();
	void gameServiceSignOut();
	bool gameServiceIsSignedIn();
	void showLeaderBoard();
	void submitLeaderBoardScore(int score);
	void showStatusAndNavBar(bool state);
	void showAchievements();
	void unlockAchievement(const std::string achievementName);
	void keepScreenOn(bool on);

	void onGameServiceSignIn();

	void addGoogleSignInCallback(std::function<void ()> callback);

private:
	AndroidApplication();
	virtual ~AndroidApplication();

	static AndroidApplication* instance;

	JNIEnv* javaEnvironment;
	jobject activityInstance;
	jclass activityClass;

	jmethodID getLocaleMethodId;
	jmethodID finishActivityMethodId;
	jmethodID gameServiceSignInMethodId;
	jmethodID gameServiceSignOutMethodId;
	jmethodID gameServiceIsSignedInMethodId;
	jmethodID showLeaderboardMethodId;
	jmethodID submitLeaderboardScoreMethodId;
	jmethodID showStatusAndNavBarMethodId;
	jmethodID showAchievementsMethodId;
	jmethodID unlockAchievementMethodId;
	jmethodID keepScreenOnMethondId;

	std::string localeString;

	std::list<std::function<void ()>> googleSignInCallbacks;
};

#endif /* ANDROIDACTIVITY_H_ */
