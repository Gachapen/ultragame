#include "AndroidApplication.h"

#include <SDL2/SDL.h>
#include "log.h"

AndroidApplication* AndroidApplication::instance = nullptr;

AndroidApplication::AndroidApplication():
	javaEnvironment(nullptr),
	activityInstance(nullptr),
	activityClass(nullptr),
	getLocaleMethodId(nullptr),
	finishActivityMethodId(nullptr),
	gameServiceSignInMethodId(nullptr),
	gameServiceSignOutMethodId(nullptr),
	gameServiceIsSignedInMethodId(nullptr),
	showLeaderboardMethodId(nullptr),
	submitLeaderboardScoreMethodId(nullptr),
	showAchievementsMethodId(nullptr),
	unlockAchievementMethodId(nullptr)
{
}

AndroidApplication::~AndroidApplication()
{
}

void AndroidApplication::addGoogleSignInCallback(std::function<void()> callback)
{
	this->googleSignInCallbacks.push_back(callback);
}

void AndroidApplication::showLeaderBoard()
{
	this->javaEnvironment->CallObjectMethod(this->activityInstance, this->showLeaderboardMethodId);
}

AndroidApplication* AndroidApplication::getInstance()
{
	if (instance == nullptr) {
		instance = new AndroidApplication();
	}

	return instance;
}

void AndroidApplication::initialize()
{
	this->javaEnvironment = static_cast<JNIEnv*>(SDL_AndroidGetJNIEnv());
	this->activityInstance = static_cast<jobject>(SDL_AndroidGetActivity());
	this->activityClass = this->javaEnvironment->GetObjectClass(activityInstance);

	this->getLocaleMethodId = this->javaEnvironment->GetMethodID(this->activityClass, "getLocale", "()Ljava/lang/String;");
	this->finishActivityMethodId = this->javaEnvironment->GetMethodID(this->activityClass, "finish", "()V");
	this->gameServiceSignInMethodId = this->javaEnvironment->GetMethodID(this->activityClass, "gameServiceSignIn", "()V");
	this->gameServiceSignOutMethodId = this->javaEnvironment->GetMethodID(this->activityClass, "gameServiceSignOut", "()V");
	this->gameServiceIsSignedInMethodId = this->javaEnvironment->GetMethodID(this->activityClass, "gameServiceIsSignedIn", "()Z");
	this->showLeaderboardMethodId = this->javaEnvironment->GetMethodID(this->activityClass, "showLeaderBoard", "()V");
	this->submitLeaderboardScoreMethodId = this->javaEnvironment->GetMethodID(this->activityClass, "submitLeaderBoardScore", "(I)V");
	this->showStatusAndNavBarMethodId = this->javaEnvironment->GetMethodID(this->activityClass, "showStatusAndNavBar", "(Z)V");
	this->showAchievementsMethodId = this->javaEnvironment->GetMethodID(this->activityClass, "showAchievements", "()V");
	this->unlockAchievementMethodId = this->javaEnvironment->GetMethodID(this->activityClass, "unlockAchievement", "(Ljava/lang/String;)V");
	this->keepScreenOnMethondId = this->javaEnvironment->GetMethodID(this->activityClass, "keepScreenOn", "(Z)V");

	googleSignInCallbacks.clear();

	LOG("Inited android activity.\n");
}

std::string AndroidApplication::getLocale()
{
	if (this->localeString == "") {
		LOG("Getting android locale.\n");
		jstring lang = (jstring) this->javaEnvironment->CallObjectMethod(this->activityInstance, this->getLocaleMethodId);
		const char* langCString = this->javaEnvironment->GetStringUTFChars(lang, nullptr);
		this->localeString = langCString;
		this->javaEnvironment->ReleaseStringUTFChars(lang, langCString);
		LOG("Got android locale %s.\n", this->localeString.c_str());
	}

	return this->localeString;
}

void AndroidApplication::finishActivity() {
	LOGD("Finishing activity.\n");
	this->javaEnvironment->CallVoidMethod(this->activityInstance, this->finishActivityMethodId);
}

void AndroidApplication::gameServiceSignIn()
{
	this->javaEnvironment->CallVoidMethod(this->activityInstance, this->gameServiceSignInMethodId);
}

void AndroidApplication::gameServiceSignOut()
{
	this->javaEnvironment->CallVoidMethod(this->activityInstance, this->gameServiceSignOutMethodId);
}

bool AndroidApplication::gameServiceIsSignedIn()
{
	return (bool) this->javaEnvironment->CallBooleanMethod(this->activityInstance, this->gameServiceIsSignedInMethodId);
}

void AndroidApplication::onGameServiceSignIn()
{
	LOG("G+ sign in.\n");
	for (auto callback : this->googleSignInCallbacks) {
		if (callback) {
			callback();
		}
	}
}

void AndroidApplication::submitLeaderBoardScore(int score)
{
	this->javaEnvironment->CallVoidMethod(this->activityInstance, this->submitLeaderboardScoreMethodId, (jint)score);
}

void AndroidApplication::showStatusAndNavBar(bool state)
{
	this->javaEnvironment->CallVoidMethod(this->activityInstance, this->showStatusAndNavBarMethodId, (jboolean)state);
}

void AndroidApplication::showAchievements()
{
	this->javaEnvironment->CallVoidMethod(this->activityInstance, this->showAchievementsMethodId);
}

void AndroidApplication::unlockAchievement(const std::string achievementName)
{
	jstring achievementString = this->javaEnvironment->NewStringUTF(achievementName.c_str());
	this->javaEnvironment->CallVoidMethod(this->activityInstance, this->unlockAchievementMethodId, achievementString);
	this->javaEnvironment->DeleteLocalRef(achievementString);
}

void AndroidApplication::keepScreenOn(bool on)
{
	this->javaEnvironment->CallVoidMethod(this->activityInstance, this->keepScreenOnMethondId, (jboolean)on);
}
