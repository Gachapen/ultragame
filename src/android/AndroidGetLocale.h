#ifndef ANDROIDGETLOCALE_H_
#define ANDROIDGETLOCALE_H_

#include "IGetLocale.h"

class AndroidGetLocale: public IGetLocale
{
public:
	virtual ~AndroidGetLocale();

	std::string getLocale() override;
};

#endif /* ANDROIDGETLOCALE_H_ */
