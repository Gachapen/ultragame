#include "GeneralFileSaver.h"

#include <SDL2/SDL.h>
#include "log.h"

#include <array>

GeneralFileSaver::~GeneralFileSaver()
{
}

std::vector<unsigned char> GeneralFileSaver::readFile(const std::string& filePath)
{
	std::vector<unsigned char> fileContent;

	SDL_RWops* file = SDL_RWFromFile(filePath.c_str(), "r");
	if (file == nullptr) {
		LOGE("Could not open file \"%s\": %s", filePath.c_str(), SDL_GetError());
		return fileContent;
	}

	const size_t BUFFER_SIZE = 4096;
	std::array<unsigned char, BUFFER_SIZE> buffer;

	size_t numObjectsRead = SDL_RWread(file, buffer.data(), sizeof(unsigned char), BUFFER_SIZE / sizeof(unsigned char));
	while (numObjectsRead != 0) {
		fileContent.insert(fileContent.end(), buffer.begin(), buffer.begin() + numObjectsRead);
		numObjectsRead = SDL_RWread(file, buffer.data(), sizeof(char), BUFFER_SIZE / sizeof(char));
	}

	SDL_RWclose(file);

	return fileContent;
}

std::string GeneralFileSaver::readFileAsString(const std::string &filePath)
{
    std::vector<unsigned char> fileContent = this->readFile(filePath);
    
    std::string data = std::string(fileContent.begin(),fileContent.end());
    return data;
}

bool GeneralFileSaver::writeString(const std::string& string, const std::string& filePath)
{
	// TODO: We should append in a "appendString" function, not here.
	SDL_RWops* file = SDL_RWFromFile(filePath.c_str(), "w");
	if (file == nullptr) {
		LOGE("Could not open file \"%s\": %s", filePath.c_str(), SDL_GetError());
		return false;
	}

	if (SDL_RWwrite(file, string.c_str(), 1, string.size()) != string.size()) {
		LOGE("Couldn't fully write string\n");
		return false;
	}

	SDL_RWclose(file);

	return true;
}

void GeneralFileSaver::deleteFile(const std::string& filePath)
{
}

bool GeneralFileSaver::appendString(const std::string& string, const std::string& filePath)
{
	// TODO: We should append in a "appendString" function, not here.
	SDL_RWops* file = SDL_RWFromFile(filePath.c_str(), "a");
	if (file == nullptr) {
		LOGE("Could not open file \"%s\": %s", filePath.c_str(), SDL_GetError());
		return false;
	}

	if (SDL_RWwrite(file, string.c_str(), 1, string.size()) != string.size()) {
		LOGE("Couldn't fully write string\n");
		return false;
	}

	SDL_RWclose(file);

	return true;
}
