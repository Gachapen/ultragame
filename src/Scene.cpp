#include "Scene.h"

Scene::Scene():
	manager(nullptr)
{
}

Scene::~Scene()
{
}

void Scene::onRender()
{
}

void Scene::onUpdate(float deltaTime)
{
}

void Scene::onEvent(const SDL_Event& event)
{
}

void Scene::onActivate()
{
}

void Scene::setHandler(SceneManager* manager)
{
	this->manager = manager;
}

void Scene::onDeactivate()
{
}

void Scene::onCreate()
{
}

void Scene::onDestroy()
{
}
