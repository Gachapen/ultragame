#ifndef TEXTUREPACKERSPRITEATLASREADER_H_
#define TEXTUREPACKERSPRITEATLASREADER_H_

#include "SpriteAtlasReader.h"

/**
 * Read atlas data from TexturePacker's json-array format.
 */
class TexturePackerSpriteAtlasReader: public SpriteAtlasReader
{
public:
	/**
	 * Reads the json file that holds the information needed about the atlas.
	 * Information like the coordinates for each sprite, size of the atlas etc.
	 * @param atlasObject the Json Object, passed by refrence.
	 * @param atlasId the ID assigned for the atlas.
	 * @param texelsPerMeter, the zoom level.
	 * @returns wether or not it was a sucess.
	 */
	bool readAtlasJson(const Json::Value& atlasObject, unsigned int atlasId, unsigned int texelsPerMeter) override;

private:
	
	/**
	 * Reads a single frame/texture/sprite from the atlas.
	 * @param frameObject the Json Object, passe by refrence.
	 * @param atlasId The id Of the atlas.
	 * @param texelsPerMeter The zoom level.
	 * @returns wether or not it was a sucess.
	 */
	bool readAtlasFrame(const Json::Value& frameObject, unsigned int atlasId, unsigned int texelsPerMeter);
};

#endif /* TEXTUREPACKERSPRITEATLASREADER_H_ */
