#include "TexturePackerSpriteAtlasReader.h"

#include "../log.h"

bool TexturePackerSpriteAtlasReader::readAtlasJson(const Json::Value& atlasObject, unsigned int atlasId, unsigned int texelsPerMeter)
{
	Json::Value framesArray = atlasObject.get("frames", Json::nullValue);
	if (framesArray.isNull() || framesArray.isArray() == false)
	{
		LOGE("SpriteManager: json atlas with no frames array.");
		return false;
	}

	Json::Value atlasMeta = atlasObject.get("meta", Json::nullValue);
	if (atlasMeta.isNull())
	{
		LOGE("SpriteManager: json atlas with no meta.");
		return false;
	}

	Json::Value atlasSize = atlasMeta.get("size", Json::nullValue);
	if (atlasSize.isNull())
	{
		LOGE("SpriteManager: json atlas with no meta size.");
		return false;
	}

	Json::Value atlasWidth = atlasSize.get("w", Json::nullValue);
	if (atlasWidth.isNull())
	{
		LOGE("SpriteManager: json atlas with no width specified.");
		return false;
	}

	Json::Value atlasHeight = atlasSize.get("h", Json::nullValue);
	if (atlasHeight.isNull())
	{
		LOGE("SpriteManager: json atlas with no height specified.");
		return false;
	}

	this->atlasWidth = atlasWidth.asInt();
	this->atlasHeight = atlasHeight.asInt();

	for (const Json::Value& frameObject : framesArray)
	{
		this->readAtlasFrame(frameObject, atlasId, texelsPerMeter);
	}

	return true;
}

bool TexturePackerSpriteAtlasReader::readAtlasFrame(const Json::Value& frameObject, unsigned int atlasId, unsigned int texelsPerMeter)
{
	Json::Value frameName = frameObject.get("filename", Json::nullValue);
	if (frameName.isNull())
	{
		LOGE("SpriteManager: json atlas frame object without name.");
		return false;
	}

	Json::Value frame = frameObject.get("frame", Json::nullValue);
	if (frame.isNull())
	{
		LOGE("SpriteManager: json atlas frame object without frame.");
		return false;
	}

	Json::Value framePosX = frame.get("x", Json::nullValue);
	if (framePosX.isNull())
	{
		LOGE("SpriteManager: json atlas frame without x position.");
		return false;
	}

	Json::Value framePosY = frame.get("y", Json::nullValue);
	if (framePosY.isNull())
	{
		LOGE("SpriteManager: json atlas frame without y position.");
		return false;
	}

	Json::Value frameWidth = frame.get("w", Json::nullValue);
	if (frameWidth.isNull())
	{
		LOGE("SpriteManager: json atlas frame without width.");
		return false;
	}

	Json::Value frameHeight = frame.get("h", Json::nullValue);
	if (frameHeight.isNull())
	{
		LOGE("SpriteManager: json atlas frame without height.");
		return false;
	}

	Sprite sprite(atlasId);
	Sprite::Quad spriteQuad;

	spriteQuad.bottomLeft.geometryVertex = glm::vec2(0.0f, 0.0f);
	spriteQuad.bottomRight.geometryVertex = glm::vec2(frameWidth.asDouble() / (double)texelsPerMeter, 0.0f);
	spriteQuad.topRight.geometryVertex = glm::vec2(frameWidth.asDouble() / (double)texelsPerMeter, frameHeight.asDouble() / (double)texelsPerMeter);
	spriteQuad.topLeft.geometryVertex = glm::vec2(0.0f, frameHeight.asDouble() / (double)texelsPerMeter);

	/*
	 * PNG is flipped upside down, so we flip the top and bottom y texture values.
	 */
	float textureCoordLeft = framePosX.asDouble() / (double)this->atlasWidth;
	float textureCoordRight = (framePosX.asDouble() + frameWidth.asDouble()) / (double)this->atlasWidth;
	float textureCoordTop = framePosY.asDouble() / (double)this->atlasHeight;
	float textureCoordBottom = (framePosY.asDouble() + frameHeight.asDouble()) / (double)this->atlasHeight;

	spriteQuad.bottomLeft.textureCoordinate = glm::vec2(textureCoordLeft, textureCoordBottom);
	spriteQuad.bottomRight.textureCoordinate = glm::vec2(textureCoordRight, textureCoordBottom);
	spriteQuad.topRight.textureCoordinate = glm::vec2(textureCoordRight, textureCoordTop);
	spriteQuad.topLeft.textureCoordinate = glm::vec2(textureCoordLeft, textureCoordTop);

	sprite.setRenderQuad(spriteQuad);
	this->spriteMap[frameName.asString()] = sprite;

	LOG("SpriteManager: Loaded sprite info \"%s\"", frameName.asCString());

	return true;
}
