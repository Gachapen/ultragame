#include "SpriteAtlasReader.h"

SpriteAtlasReader::~SpriteAtlasReader()
{
}

unsigned int SpriteAtlasReader::getAtlasWidth() const
{
	return this->atlasWidth;
}

unsigned int SpriteAtlasReader::getAtlasHeight() const
{
	return this->atlasHeight;
}

std::unordered_map<std::string, Sprite> SpriteAtlasReader::getSpriteMap() const
{
	return this->spriteMap;
}

void SpriteAtlasReader::reset()
{
	this->atlasHeight = 0;
	this->atlasWidth = 0;
	this->spriteMap.clear();
}
