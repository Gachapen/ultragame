#ifndef SPRITE_H_
#define SPRITE_H_

#include <glm/glm.hpp>

/**
 * Representation of a graphical sprite.
 */
class Sprite
{
public:
	/**
	 * A coordinate on a sprite.
	 * Contains both the geometry vertex to be rendered
	 * and the texture coordinate of the texture.
	 */
	struct Coordinate
	{
		glm::vec2 geometryVertex;
		glm::vec2 textureCoordinate;
	};

	/**
	 * Four Sprite::Coordinate representing a square.
	 */
	struct Quad
	{
		Sprite::Coordinate bottomLeft;
		Sprite::Coordinate bottomRight;
		Sprite::Coordinate topRight;
		Sprite::Coordinate topLeft;
	};

	Sprite();
	Sprite(int atlasId);

	void setRenderQuad(const Sprite::Quad& quad);

	/**
	 * @return The atlas id of this sprite, or -1 if none is set.
	 */
	int getAtlasId() const;

	/**
	 * Gets the quad used to render this sprite.
	 * @returns a Quad that holds the info needed to render this quad.
	 */
	const Sprite::Quad& getRenderQuad() const;
	float getWidth() const;
	float getHeight() const;

private:
	int atlasId;
	Sprite::Quad renderQuad;
};

#endif /* SPRITE_H_ */
