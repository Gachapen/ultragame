#ifndef SPRITEMANAGER_H_
#define SPRITEMANAGER_H_

#include <json/value.h>
#include <unordered_map>
#include <vector>
#include <glm/glm.hpp>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengles2.h>
#include "Sprite.h"

/**
 * Manages sprites.
 * The job of this class is to load sprite information, store it,
 * write sprite images to OpenGL buffers, and provide data
 * for renderers.
 */
class SpriteManager
{
public:
	SpriteManager();
	SpriteManager(const SpriteManager&) = delete;
	SpriteManager& operator = (const SpriteManager&) = delete;
	
	/**
	 * Read sprite information from the json object provided.
	 * @param spriteJson Json object to read from.
	 * @return true if it succeeded, false if not.
	 */
	bool readSpriteFile(const Json::Value& spriteJson);
	
	/**
	 * Write all sprites atlases loaded to OpenGL texture buffers.
	 */
	void writeToTextureBuffers();
	
	void removeTextureBuffers();
	
	bool hasSprite(const std::string& spriteName) const;
	
	const Sprite& getSprite(const std::string& spriteName) const;
	
	int getNumAtlases() const;
	
	/**
	 * Gets the OpenGL textureBuffer from the atlas.
	 * @param atlasNumber the number of the atlas we want the buffer to.
	 * @returns the OpenGL buffer.
	 */
	GLuint getTextureBuffer(unsigned int atlasNumber) const;
	
	/**
	 * Gets the size of the atlas in pixels.
	 * @param name The name of the atlas we are asking about.
	 * @returns A vector containing the width and height of the atlas.
	 */
	glm::ivec2 getAtlasSize(const std::string& name);
	
	
	
	
private:
	/**
	 * Information about a sprite atlas.
	 */
	struct SpriteAtlas
	{
		std::string name;			//!< Name of the atlas.
		std::string imageExtension;	//!< Extension (e.g. .png) used for the atlas image.
		std::string dataExtension;	//!< Extension (e.g. .json) used for the atlas data.
		unsigned int width;
		unsigned int height;
		unsigned int id;			//!< Unique ID for the atlas managed by the SpriteManager.
		GLuint boundTextureBuffer;	//!< Texture buffer this atlas is bound to. 0 if not bound.
	};
	
	/**
	 * Write sprite atlas to OpenGL texture buffer.
	 * @param imageName The name of the image to write.
	 * @return The buffer name for the atlas, 0 if no bound.
	 */
	GLuint writeToTextureBuffer(const std::string imageName) const;
	
	std::vector<SpriteAtlas> textureAtlasFiles;
	std::unordered_map<std::string, Sprite> spriteMap;
	
	unsigned int defaultTexelsPerMeter;
	unsigned int texelsPerMeter;
};

#endif /* SPRITEMANAGER_H_ */
