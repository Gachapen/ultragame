#include "Sprite.h"

Sprite::Sprite():
	atlasId(-1)
{
}

Sprite::Sprite(int atlasId):
	atlasId(atlasId)
{
}

void Sprite::setRenderQuad(const Sprite::Quad& quad)
{
	this->renderQuad = quad;
}

int Sprite::getAtlasId() const
{
	return this->atlasId;
}

const Sprite::Quad& Sprite::getRenderQuad() const
{
	return this->renderQuad;
}

float Sprite::getWidth() const
{
	return (this->renderQuad.bottomRight.geometryVertex.x - this->renderQuad.bottomLeft.geometryVertex.x);
}

float Sprite::getHeight() const
{
	return (this->renderQuad.topRight.geometryVertex.y - this->renderQuad.bottomRight.geometryVertex.y);
}
