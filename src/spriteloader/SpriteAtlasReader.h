#ifndef SPRITEATLASREADER_H_
#define SPRITEATLASREADER_H_

#include <unordered_map>
#include <string>
#include <json/value.h>
#include "Sprite.h"

/**
 * Abstract reader to read atlas files.
 */
class SpriteAtlasReader
{
public:
	virtual ~SpriteAtlasReader();
	
	unsigned int getAtlasWidth() const;
	unsigned int getAtlasHeight() const;
	std::unordered_map<std::string, Sprite> getSpriteMap() const;

	/**
	 * Reset the width, height, and sprite map stored.
	 */
	void reset();

	/**
	 * Read atlas data from the json object provided.
	 * @param atlasObject Json object to read from.
	 * @param atlasId Atlas ID for the sprites read.
	 * @return true if success, false if not.
	 */
	virtual bool readAtlasJson(const Json::Value& atlasObject, unsigned int atlasId, unsigned int texelsPerMeter) = 0;

protected:
	unsigned int atlasWidth;
	unsigned int atlasHeight;
	std::unordered_map<std::string, Sprite> spriteMap;
};

#endif /* SPRITEATLASREADER_H_ */
