#include "SpriteManager.h"

#include <SDL2/SDL.h>

#ifdef __APPLE__
#include <SDL2_image/SDL_image.h>
#else
#include <SDL2/SDL_image.h>
#endif

#include <fstream>
#include <sstream>
#include <json/reader.h>
#include <memory>
#include <algorithm>
#include "TexturePackerSpriteAtlasReader.h"
#include "../log.h"
#include "../GeneralFileSaver.h"

#ifdef __APPLE__
#include "../ios/IosFileSaver.h"
#endif

#include "../Assets.h"

SpriteManager::SpriteManager():
	defaultTexelsPerMeter(70),
	texelsPerMeter(defaultTexelsPerMeter)
{
}

bool SpriteManager::readSpriteFile(const Json::Value& spriteJson)
{
	this->texelsPerMeter = spriteJson.get("texelsPerMeter", this->defaultTexelsPerMeter).asUInt();

	Json::Value spriteAtlasArray = spriteJson.get("spriteAtlases", Json::nullValue);
	if (spriteAtlasArray.isNull())
	{
		LOGE("SpriteManager: json without sprite atlas info.");
		return false;
	}
	else if (spriteAtlasArray.isArray() == false)
	{
		LOGE("SpriteManager: json atlas array was not an array...");
		return false;
	}

	unsigned int numAtlases = spriteAtlasArray.size();
	this->textureAtlasFiles.reserve(numAtlases);

	for (const Json::Value& atlasObject : spriteAtlasArray)
	{
		SpriteAtlas atlas;

		Json::Value atlasName = atlasObject.get("name", Json::nullValue);
		if (atlasName.isNull())
		{
			LOGE("SpriteManager: json atlas without name.");
			return false;
		}

		atlas.name = atlasName.asString();
		atlas.imageExtension = atlasObject.get("imageExtension", "png").asString();
		atlas.dataExtension = atlasObject.get("dataExtension", "json").asString();
		atlas.boundTextureBuffer = 0;
		atlas.id = (unsigned)this->textureAtlasFiles.size();


		Json::Reader atlasJsonReader;
		Json::Value atlasJsonRoot;
		std::string atlasFileName = atlas.name + "." + atlas.dataExtension;

		auto fileHandler = std::make_shared<GeneralFileSaver>();
		std::string atlasData = fileHandler->readFileAsString(Assets::getInstance()->getPrependAssetsRootPath(atlasFileName).c_str());
		std::istringstream atlasStream(atlasData);

		atlasJsonReader.parse(atlasStream, atlasJsonRoot, false);
		std::unique_ptr<SpriteAtlasReader> atlasReader = std::unique_ptr<SpriteAtlasReader>(new TexturePackerSpriteAtlasReader());

		if (atlasReader->readAtlasJson(atlasJsonRoot, atlas.id, this->texelsPerMeter) == true)
		{
			atlas.width = atlasReader->getAtlasWidth();
			atlas.height = atlasReader->getAtlasHeight();
			this->spriteMap = atlasReader->getSpriteMap();
		}
		else
		{
			LOGE("SpriteManager: Could not read atlas file \"%s\".", atlasFileName.c_str());
		}
        
        this->textureAtlasFiles.push_back(atlas);

	}

	return true;
}

void SpriteManager::writeToTextureBuffers()
{
	for (auto& atlas : this->textureAtlasFiles)
	{
		if (atlas.id == 0)
		{
			std::string atlasImagePath = atlas.name + "." + atlas.imageExtension;
			atlas.boundTextureBuffer = this->writeToTextureBuffer(atlasImagePath);
		}
	}
}

GLuint SpriteManager::writeToTextureBuffer(const std::string imageName) const
{
    GLuint buffer = 0;

    
#ifdef __APPLE__

    IosFileSaver fileHandler;
    buffer = fileHandler.createTextureFromFile(Assets::getInstance()->getPrependAssetsRootPath(imageName));
    
    
#else
    
	SDL_Surface* textureSurface = IMG_Load(Assets::getInstance()->getPrependAssetsRootPath(imageName).c_str());
	if (!textureSurface) {
		LOGE("Failed to load texture \"%s\": %s\n", imageName.c_str(), IMG_GetError());
		return 0;
	}
    
	GLenum colorMode = GL_RGB;
	if(textureSurface->format->BytesPerPixel == 4) {
    	colorMode = GL_RGBA;
	}
    
    /*
     *  We had a problem where SDL_image loaded images in the BGR(A) format on some systems.
     *  Here we check if the blue color is the first color and sets the BGR(A) format if it is.
     */
    GLenum imageColorMode = GL_RGBA;
//    if (textureSurface->format->Bmask == 0xFF)
//    {
//        imageColorMode = GL_BGRA;
//        if (textureSurface->format->BytesPerPixel == 4) {
//            imageColorMode = GL_BGRA;
//        }
//    }


	glGenTextures(1, &buffer);
	glBindTexture(GL_TEXTURE_2D, buffer);
	glTexImage2D(GL_TEXTURE_2D, 0, colorMode, textureSurface->w, textureSurface->h, 0, imageColorMode, GL_UNSIGNED_BYTE, textureSurface->pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
    
	SDL_FreeSurface(textureSurface);
    
#endif


	return buffer;
}

const Sprite& SpriteManager::getSprite(const std::string& spriteName) const
{
	auto spriteIt = this->spriteMap.find(spriteName);
    assert(spriteIt != this->spriteMap.end());
    return this->spriteMap.find(spriteName)->second;
}

bool SpriteManager::hasSprite(const std::string& spriteName) const
{
	const auto& spriteIt = this->spriteMap.find(spriteName);
	if (spriteIt != this->spriteMap.end())
	{
		return true;
	}

	return false;
}

int SpriteManager::getNumAtlases() const
{
	return (unsigned)this->textureAtlasFiles.size();
}

GLuint SpriteManager::getTextureBuffer(unsigned int atlasNumber) const
{
	assert(atlasNumber < this->textureAtlasFiles.size());

	return this->textureAtlasFiles[atlasNumber].boundTextureBuffer;
}


glm::ivec2 SpriteManager::getAtlasSize(const std::string &name)
{
    auto it = std::find_if(textureAtlasFiles.begin(), textureAtlasFiles.end(), [name](const SpriteAtlas& atlas)
                                                                        {
                                                                            return (atlas.name == name);
                                                                        });
    assert(it != textureAtlasFiles.end());
    return glm::ivec2(it->width,it->height);
}

void SpriteManager::removeTextureBuffers()
{
	for (auto& atlas : this->textureAtlasFiles)
	{
		if (atlas.boundTextureBuffer != 0)
		{
			glDeleteTextures(1, &atlas.boundTextureBuffer);
		}
	}
}
