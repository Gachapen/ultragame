#ifndef LOCALIZATION_H_
#define LOCALIZATION_H_

#include <string>
#include <map>
#include <SDL2/SDL.h>
#include "pugixml/pugixml.hpp"

class Localization
{
public:
	class Strings {
	public:
		Strings(Localization* localization);
		std::string get(const std::string& stringName);
		void add(const std::string& locale, const std::string& stringName, const std::string& stringValue);

	private:
		typedef std::map<std::string, std::string> StringMap;
		typedef std::map<std::string, StringMap> LocaleMap;

		/**
		 * Get the string map for the default locale.
		 * @return Pointer to the defaul local string map.
		 */
		const StringMap* getDefaultLocaleStringMap() const;

		/**
		 * Get the string map for the preferred locale.
		 * If a string map for the current locale is present, that will be returned.
		 * If this is not the case, it will return the map for the default
		 * locale by calling {@link #getDefaultLocaleStringMap()}.
		 * @return Pointer to the string map for the preferred locale.
		 */
		const StringMap* getPreferredStringMap() const;

		LocaleMap localeMap;
		Localization* localization;
	};

	static Localization* getInstance();
	static void create();
	static void destroy();

	/**
	 * Set the locale to be used from this point.
	 * @param locale The locale to use.
	 */
	void setLocale(const std::string& locale);

	/**
	 * Set the locale to be used as default.
	 * This locale will be used as a fallback if something
	 * is not present in the current.
	 * @param locale The default locale.
	 */
	void setDefaultLocale(const std::string& locale);

	/**
	 * Load the localization information from an XML file.
	 * @param localeFilePath Path to the XML file to load.
	 * @return false if loading failed, otherwise true.
	 */
	bool loadLocalization(const std::string& localeFilePath);

	Strings strings;

private:
	Localization();
	~Localization();

	static Localization* instance;

	std::string currentLocale;
	std::string defaultLocale;

	bool parseLocale(const pugi::xml_node& localeNode);
};

#endif /* LOCALIZATION_H_ */
