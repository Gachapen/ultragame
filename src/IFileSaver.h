#ifndef IFILESAVER_H_
#define IFILESAVER_H_

#include <string>
#include <vector>

class IFileSaver {
public:
	virtual ~IFileSaver()
	{ }
	
	/**
	 * Writes to the specified path by overwriting what is there.
	 * @param string The string to be written.
	 * @param filePath The path to the file.
	 * @returns Wether or not it was a sucess.
	 */
	virtual bool writeString(const std::string& string, const std::string& filePath) = 0;
	
	/**
	 * Writes to the specified path by appending to what is there.
	 * @param string The data to append.
	 * @param filePath The path to the file.
	 * @returns wether or not it was a sucess.
	 */
	virtual bool appendString(const std::string& string, const std::string& filePath) = 0;
	
	/**
	 * Reads the file at the path.
	 * @param filePath The path to the file.
	 * @returns A vector containing the read data.
	 */
	virtual std::vector<unsigned char> readFile(const std::string& filePath) = 0;
	
	/**
	 * Deletes the file at the path.
	 * @param filePath The path to the file.
	 */
	virtual void deleteFile(const std::string& filePath) = 0;
	
	/**
	 * Reads the file and returns the data as a string.
	 * @param filePath The filepath.
	 * @returns a string from the data.
	 */
	virtual std::string readFileAsString(const std::string& filePath) = 0;
	
};

#endif /* IFILESAVER_H_ */
