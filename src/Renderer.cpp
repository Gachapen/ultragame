//
//  Renderer.cpp
//  ultragame
//
//  Created by Asbjoern Sporaland on 05/11/13.
//  Copyright (c) 2013 masb. All rights reserved.
//
#include "Renderer.h"

#include <unordered_map>
#include <string>
#include <algorithm>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "OpenGLState.h"

#include "log.h"
#include "GeneralFileSaver.h"
#include "utilities/opengl_utils.h"
#include "Assets.h"

Renderer::Renderer()
{
	this->geometryOrder[GeometryType::TRIANGLE] = 0;
	this->geometryOrder[GeometryType::QUAD] = 1;
	this->geometryOrder[GeometryType::POLYGON] = 2;
	this->geometryOrder[GeometryType::LINE] = 3;
}

Renderer::~Renderer()
{
	
}

void Renderer::init(int width, int height, GLuint textureName, GLuint textureNumber)
{
	this->width = width;
	this->height = height;
	this->textureName = textureName;
	this->activeTextureNumber = textureNumber;
	
	glGenBuffers(1, &this->VBO);
}

void Renderer::createShaderProgramAndBindUniforms(std::string name,
						  std::string shaderPath,
						  std::vector<Attribute> attributes,
						  std::function<std::unordered_map<std::string, GLint> (GLuint shaderProgram)> getUniformLocations)
{
	this->fileHandler = std::unique_ptr<IFileSaver>(new GeneralFileSaver());
	
	ShaderProgram shaderProgram;
	shaderProgram.program = glCreateProgram();
	
	std::string fragmentShaderPath = shaderPath;
	std::string vertexShaderPath = shaderPath;
	
#if defined(__ANDROID__) || defined(__IPHONEOS__)
	fragmentShaderPath += "_es";
#endif
	
	fragmentShaderPath += ".fs";
	vertexShaderPath += ".vs";
	
	GLuint vertexShader;
	GLuint fragmentShader;
	
	std::string vertexShaderContent = this->fileHandler->readFileAsString(Assets::getInstance()->getPrependAssetsRootPath(vertexShaderPath));
	std::string fragmentShaderContent = this->fileHandler->readFileAsString(Assets::getInstance()->getPrependAssetsRootPath(fragmentShaderPath));
	
	vertexShader = createShader(vertexShaderContent, GL_VERTEX_SHADER);
	fragmentShader = createShader(fragmentShaderContent, GL_FRAGMENT_SHADER);
	
	assert(shaderProgram.program != 0);
	assert(vertexShader != 0);
	assert(fragmentShader != 0);
	
	for (size_t i = 0; i < attributes.size(); i++)
	{
		shaderProgram.attributes[attributes[i].name.c_str()] = attributes[i];
		glBindAttribLocation(shaderProgram.program, (GLuint)i, attributes[i].name.c_str());
	}
	
	// Link shader program.
	if (linkShaderProgram(shaderProgram.program, vertexShader, fragmentShader) == false)
	{
		LOGE("Renderer: Could not link shader %s.", name.c_str());
	}
	
	shaderProgram.uniforms = getUniformLocations(shaderProgram.program);
	this->shaderPrograms[name] = shaderProgram;
}

void Renderer::addGeometrySet(const std::shared_ptr<GeometrySet>& set)
{
	this->geometrySets.push_back(set);
	std::sort(
		  this->geometrySets.begin(),
		  this->geometrySets.end(),
		  [this](const std::shared_ptr<GeometrySet>& a, const std::shared_ptr<GeometrySet>& b)
		  {
			  if (this->geometryOrder[a->getGeometryType()] < this->geometryOrder[b->getGeometryType()])
			  {
				  return true;
			  }
			  else
			  {
				  return false;
			  }
		  });
}

void Renderer::removeGeometrySet(const std::shared_ptr<GeometrySet>& set)
{
	auto newEndIt = std::remove(this->geometrySets.begin(), this->geometrySets.end(), set);
	this->geometrySets.erase(newEndIt);
}

void Renderer::renderGeometrySets(const ShaderProgram& shaderProgram)
{
	glClear(GL_COLOR_BUFFER_BIT);

	std::vector<ObjectCoordinate> coords;

	size_t numTriangleCoords = 0;
	size_t numLineCoords = 0;

	bool renderTriangles = false;
	bool renderLines = false;

	for (const std::shared_ptr<GeometrySet>& geometrySet : this->geometrySets)
	{
		if (geometrySet->isActive() == true)
		{
			const std::vector<ObjectCoordinate>& setCoordinates = geometrySet->getVertexData();
			GeometryType type = geometrySet->getGeometryType();

			if (type == GeometryType::TRIANGLE || type == GeometryType::QUAD || type == GeometryType::POLYGON)
			{
				renderTriangles = true;
				numTriangleCoords += setCoordinates.size();
			}
			else if (type == GeometryType::LINE)
			{
				renderLines = true;
				numLineCoords += setCoordinates.size();
			}

			coords.insert(coords.end(), setCoordinates.begin(), setCoordinates.end());
		}
	}

	if (this->VBO != OpenGLState::currentlyBoundVBO)
	{
		glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
		OpenGLState::currentlyBoundVBO = this->VBO;
	}
	glBufferData(GL_ARRAY_BUFFER, coords.size() * sizeof(ObjectCoordinate), &coords.front(), GL_STREAM_DRAW);

	if (shaderProgram.program != OpenGLState::currentlyUsedProgram)
	{
		shaderProgram.readyShaderProgram();
		OpenGLState::currentlyUsedProgram = shaderProgram.program;
	}

	if (this->textureName != OpenGLState::currentlyBoundTexture)
	{
		glActiveTexture(this->activeTextureNumber);
		glBindTexture(GL_TEXTURE_2D, this->textureName);
		OpenGLState::currentlyBoundTexture = this->textureName;
	}

	if (renderTriangles == true)
	{
		glDrawArrays(GL_TRIANGLES, 0, (GLsizei)numTriangleCoords) ;
	}

	if (renderLines == true)
	{
		glDrawArrays(GL_LINES, (GLint)numTriangleCoords, (GLsizei)numLineCoords);
	}
}

void ShaderProgram::readyShaderProgram() const
{
	for (auto attribute : this->attributes) {
		this->readyAttribute(attribute.second);
	}

	glUseProgram(this->program);
}

void ShaderProgram::readyAttribute(const Attribute& attribute) const
{
	glVertexAttribPointer(attribute.index, attribute.size, attribute.type, attribute.normalized, attribute.stride, attribute.pointer);
	glEnableVertexAttribArray(attribute.index);
}
