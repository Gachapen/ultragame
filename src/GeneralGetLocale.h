#ifndef GENERALGETLOCALE_H_
#define GENERALGETLOCALE_H_

#include "IGetLocale.h"

/**
 * Will Be used for playforms other than iOS or Android to get the locale.
 * set to en (english)
 */
class GeneralGetLocale: public IGetLocale
{
public:
	virtual ~GeneralGetLocale();

	/**
	 * Returns the en as the locale.
	 */
	std::string getLocale() override;
};

#endif /* GENERALGETLOCALE_H_ */
