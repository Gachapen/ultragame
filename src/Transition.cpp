#include "Transition.h"

#include <cmath>
#include <cassert>
#include "log.h"

Transition::Transition():
	transitioning(false),
	startValue(0.0f),
	endValue(0.0f),
	currentValue(0.0f),
	direction(1.0f),
	valuesPerSecond(1.0f)
{
}

Transition::~Transition()
{
}

void Transition::setValues(float start, float end)
{
	this->startValue = start;
	this->endValue = end;

	if (this->endValue - this->startValue >= 0) {
		this->direction = 1.0f;
	} else {
		this->direction = -1.0f;
	}
}

void Transition::setDuration(std::chrono::duration<float> duration)
{
	this->duration = duration;
}

void Transition::setUpdateCallback(std::function<void(float)> callback)
{
	this->updateCallback = callback;
}

void Transition::setSuccessCallback(std::function<void()> transitionDoneCallback)
{
	this->doneCallback = transitionDoneCallback;
}

bool Transition::isTransitioning() const
{
	return this->transitioning;
}

void Transition::onInit()
{
	this->currentValue = this->startValue;
	this->transitioning = true;
	this->startTime = std::chrono::high_resolution_clock::now();
}

void Transition::onUpdate(float deltaTime)
{
	if (this->transitioning == true) {
		auto currentTime = std::chrono::high_resolution_clock::now();
		auto timeUsed = currentTime - this->startTime;

		if (timeUsed > this->duration) {
			this->transitioning = false;
			this->currentValue = this->endValue;

			if (this->updateCallback) {
				this->updateCallback(this->currentValue);
			}

			this->succeedExecution();
		} else {
			auto remainingTime = this->duration - timeUsed;
			float movementRemaining = this->endValue - this->currentValue;

			this->valuesPerSecond = movementRemaining / (std::chrono::duration<float>(remainingTime)).count();

			this->currentValue += this->valuesPerSecond * deltaTime;

			if (this->direction < 0 && this->currentValue < this->endValue) {
				this->currentValue = this->endValue;
			}

			if (this->direction > 0 && this->currentValue > this->endValue) {
				this->currentValue = this->endValue;
			}

			if (this->updateCallback) {
				this->updateCallback(this->currentValue);
			}
		}
	}
}

void Transition::onSuccess()
{
	if (this->doneCallback) {
		this->doneCallback();
	}
}

void Transition::onFail()
{
	assert(!"Transition cannot fail.");
}

void Transition::onAbort()
{
	assert(!"Transition cannot abort.");
}
