//
//  OpenGLState.h
//  ultragame
//
//  Created by Asbjoern Sporaland on 15/12/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#ifndef __ultragame__OpenGLState__
#define __ultragame__OpenGLState__


#include <SDL2/SDL.h>
#include <SDL2/SDL_opengles2.h>

class OpenGLState {
public:
	static GLuint currentlyBoundVBO;
	static GLuint currentlyUsedProgram;
	static GLuint currentlyBoundTexture;
};
#endif /* defined(__ultragame__OpenGLState__) */
