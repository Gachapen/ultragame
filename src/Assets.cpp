#include "Assets.h"

#include <sstream>

#include "log.h"

#include <json/reader.h>

Assets* Assets::instance = nullptr;

Assets::Assets():
	ANDROID_ROOT(""),
	PC_ROOT("assets/"),
	IOS_ROOT("")
{
    this->fileHandler = std::make_shared<GeneralFileSaver>();
    this->spriteManager = std::make_shared<SpriteManager>();
}

void Assets::init()
{
	// Load into sprite manager.
	std::string imageData = this->fileHandler->readFileAsString(this->getPrependAssetsRootPath("sprite.json").c_str());
	std::istringstream spriteFileStream(imageData);

	Json::Reader spriteJsonReader;
	Json::Value spriteJsonRoot;
	spriteJsonReader.parse(spriteFileStream, spriteJsonRoot, false);
	this->spriteManager->readSpriteFile(spriteJsonRoot);
	this->spriteManager->writeToTextureBuffers();
}

std::string Assets::getPrependAssetsRootPath(const std::string& path)
{
	return this->getAssetRootPath() + path;
}

void Assets::create()
{
	if (instance == nullptr) {
		instance = new Assets();
	} else {
		LOGE("Error creating Assets: Already instantiated.\n");
	}
}

void Assets::destroy()
{
	if (instance != nullptr) {
		delete instance;
		instance = nullptr;
	} else {
		LOGE("Error destroying Assets: Not instantiated.\n");
	}
}

Assets::~Assets()
{
}

std::string Assets::getAssetRootPath()
{
#ifdef __ANDROID__
   return this->ANDROID_ROOT;
#elif defined (__APPLE__)
    return this->IOS_ROOT;
#else
    return this->PC_ROOT;
#endif
}

Assets* Assets::getInstance()
{
	return instance;
}

void Assets::unloadAssets()
{
	this->spriteManager->removeTextureBuffers();
}
