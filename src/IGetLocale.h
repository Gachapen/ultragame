//
//  IGetLocale.h
//  ultragame
//
//  Created by Asbjoern Sporaland on 10/4/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#ifndef ultragame_IGetLocale_h
#define ultragame_IGetLocale_h

#include <string>

/**
 * Used to get the locale of the device.
 */
class IGetLocale
{
public:
    virtual ~IGetLocale() {}
	
	/**
	 * Returns the locale of the device
	 */
    virtual std::string getLocale() = 0;
};


#endif
