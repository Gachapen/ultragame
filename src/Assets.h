#ifndef ASSETS_H_
#define ASSETS_H_

#include <string>
#include <unordered_map>
#include <memory>
#include <SDL2/SDL_image.h>
#include "GeneralFileSaver.h"
#include "spriteloader/SpriteManager.h"


class Assets
{
public:
	/**
	 * Gets the singleton instance.
	 */
	static Assets* getInstance();
	
	/**
	 * creates the singleton.
	 */
	static void create();
	
	/**
	 * destroys the singleton.
	 */
	static void destroy();

	/**
	 * Gets the root path of the Assets folder for the specific environment.
	 * @returns the asset root path.
	 */
	std::string getAssetRootPath();
	
	/**
	 * Prepends the root path to the asset path.
	 * @param path The path to the resource we want.
	 * @returns The path to the resource, prepended by the asset Root
	 */
	std::string getPrependAssetsRootPath(const std::string& path);
	
	/**
	 * Removes the assets.
	 */
	void unloadAssets();
	void init();

	std::shared_ptr<SpriteManager> spriteManager;


private:
	Assets();
	virtual ~Assets();

	static Assets* instance;

	const std::string ANDROID_ROOT;
	const std::string PC_ROOT;
	const std::string IOS_ROOT;

	std::shared_ptr<GeneralFileSaver> fileHandler;
};

#endif /* ASSETS_H_ */
