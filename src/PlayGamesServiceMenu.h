#ifndef PLAYGAMESSERVICEMENU_H_
#define PLAYGAMESSERVICEMENU_H_

#include <memory>
#include <string>
#include "GameServicesMenu.h"
#include "gui/Button.h"
#include "gui/ListView.h"
#include "gui/ImageView.h"
#include <string>

class PlayGamesServiceMenu: public GameServicesMenu
{
public:
	PlayGamesServiceMenu();
	~PlayGamesServiceMenu();

	void onClick(const View& clickedElement) override;
        
private:
	enum class SignInState {
		SIGNED_IN,
		SIGNED_OUT,
		SIGNING_IN
	};

	void setupUi(const std::shared_ptr<View>& gameServiceMenu, GeometrySet* quadHandler) override;

	bool isSignedIn();
	void signIn();
	void signOut();
	void showLeaderBoard();
	void showAchievements();

	void onSignedOut();
	void onSignedIn();

	SignInState currentSignInState;

	std::shared_ptr<ListView> googleServicesList;

	std::shared_ptr<ListView> gameServicesList;
	std::shared_ptr<Button> leaderboardButton;
	std::shared_ptr<Button> achievementsButton;

	std::shared_ptr<Button> googlePlusSignInBtn;
	std::shared_ptr<ImageView> googlePlusImage;

	std::string signedInImage = "btn_g+normal.png";
	std::string signedOutImage = "btn_g+disabled.png";
};

#endif /* PLAYGAMESSERVICEMENU_H_ */
