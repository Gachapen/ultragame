#ifndef PROCESS_H_
#define PROCESS_H_

#include <memory>
#include "ProcessImplementation.h"

/**
 * Representation of a process running in a ProcessManager.
 */
class Process
{
public:
	Process(std::unique_ptr<ProcessImplementation> implementation);
	virtual ~Process();

	void init();
	void update(float deltaTime);
	void pause();
	void resume();
	void failExecution();
	void succeedExecution();
	void abortExecution();

	/**
	 * Set a successor process.
	 * The successor process will be started if this process succeeds.
	 * @param successor Successor process.
	 */
	void setSuccessorProcess(std::unique_ptr<Process> successor);

	/**
	 * Get the successor process.
	 * @return The successor process, or empty unique_ptr if it has no successor.
	 */
	std::unique_ptr<Process> getSuccessor();

	void onSucceed();
	void onFail();
	void onAbort();

	bool isInitialized() const;
	bool isRunning() const;
	bool isAlive() const;
	bool didSucceed() const;
	bool didFail() const;
	bool wasAborted() const;
	bool hasSuccessor() const;

private:
	/**
	 * State of the process.
	 */
	enum class State
	{
		UNINITIALIZED,//!< The process is not initialized yet.
		RUNNING,      //!< The process is executing.
		PAUSED,       //!< The process's execution is paused.
		SUCCEEDED,    //!< The process's execution succeeded.
		FAILED,       //!< The process's execution failed.
		ABORTED       //!< The process was aborted by user.
	};

	State state;
	std::unique_ptr<ProcessImplementation> implementation;
	std::unique_ptr<Process> successorProcess;
};

#endif /* PROCESS_H_ */
