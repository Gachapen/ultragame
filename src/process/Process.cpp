#include "Process.h"

Process::Process(std::unique_ptr<ProcessImplementation> implementation):
	state(State::UNINITIALIZED),
	implementation(std::move(implementation))
{
	this->implementation->onAttachToProcess(this);
}

Process::~Process()
{
}

void Process::init()
{
	this->state = State::RUNNING;
	this->implementation->onInit();
}

void Process::update(float deltaTime)
{
	this->implementation->onUpdate(deltaTime);
}

void Process::onSucceed()
{
	this->implementation->onSuccess();
}

void Process::onFail()
{
	this->implementation->onFail();
}

void Process::onAbort()
{
	this->implementation->onAbort();
}

void Process::pause()
{
	this->state = State::PAUSED;
}

void Process::resume()
{
	this->state = State::RUNNING;
}

void Process::failExecution()
{
	this->state = State::FAILED;
}

void Process::succeedExecution()
{
	this->state = State::SUCCEEDED;
}

void Process::abortExecution()
{
	this->state = State::ABORTED;
}

bool Process::isInitialized() const
{
	return (this->state != State::UNINITIALIZED);
}

bool Process::isRunning() const
{
	return (this->state == State::RUNNING);
}

bool Process::isAlive() const
{
	return (this->state == State::RUNNING
			|| this->state == State::PAUSED
			|| this->state == State::UNINITIALIZED);
}

bool Process::didSucceed() const
{
	return (this->state == State::SUCCEEDED);
}

bool Process::didFail() const
{
	return (this->state == State::FAILED);
}

void Process::setSuccessorProcess(std::unique_ptr<Process> successor)
{
	this->successorProcess = std::move(successor);
}

std::unique_ptr<Process> Process::getSuccessor()
{
	return std::move(this->successorProcess);
}

bool Process::wasAborted() const
{
	return (this->state == State::ABORTED);
}

bool Process::hasSuccessor() const
{
	if (this->successorProcess)
	{
		return true;
	}
	else
	{
		return false;
	}
}
