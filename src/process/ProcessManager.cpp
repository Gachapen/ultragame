#include "ProcessManager.h"

#include <queue>
#include <algorithm>

ProcessManager::ProcessManager()
{
}

ProcessManager::~ProcessManager()
{
}

void ProcessManager::updateProcesses(float deltaTime)
{
	std::queue<std::unique_ptr<Process>> successorProcesses;

	auto processIt = this->processes.begin();

	while (processIt != this->processes.end())
	{
		std::unique_ptr<Process>& process = *processIt;

		if (process->isInitialized() == false)
		{
			process->init();
		}

		if (process->isRunning() == true)
		{
			process->update(deltaTime);
		}

		if (process->isAlive() == false)
		{
			if (process->didSucceed() == true)
			{
				process->onSucceed();

				if (process->hasSuccessor() == true)
				{
					successorProcesses.push(process->getSuccessor());
				}
			}
			else if (process->didFail() == true)
			{
				process->onFail();
			}
			else if (process->wasAborted() == true)
			{
				process->onAbort();
			}

			processIt = this->processes.erase(processIt);
		}
		else
		{
			processIt++;
		}
	}

	while (successorProcesses.empty() == false)
	{
		this->processes.push_back(std::move(successorProcesses.front()));
		successorProcesses.pop();
	}
}

ProcessManager::ProcessHandle ProcessManager::startProcess(std::unique_ptr<ProcessImplementation> implementation)
{
	auto process = std::unique_ptr<Process>(new Process(std::move(implementation)));
	ProcessHandle handle(process.get(), this);
	this->processes.push_back(std::move(process));

	return handle;
}

bool ProcessManager::abortProcess(const ProcessHandle& handle)
{
	bool foundProcess = false;
	auto processIt = this->processes.begin();

	while (foundProcess == false && processIt != this->processes.end())
	{
		Process* process = (*processIt).get();
		if (handle.isHandleToProcess(process) == true)
		{
			foundProcess = true;
			process->abortExecution();
		}
	}

	return foundProcess;
}

ProcessManager::ProcessHandle::ProcessHandle():
	process(nullptr),
	manager(nullptr)
{
}

ProcessManager::ProcessHandle::ProcessHandle(Process* process, ProcessManager* manager):
	process(process),
	manager(manager)
{
}

bool ProcessManager::ProcessHandle::isHandleToProcess(const Process* process) const
{
	return this->process == process;
}

bool ProcessManager::ProcessHandle::isValid() const
{
	if (this->process == nullptr)
	{
		return false;
	}
	else if (this->manager == nullptr)
	{
		return false;
	}
	else if (this->manager->hasProcess(this->process) == false)
	{
		return false;
	}
	else
	{
		return true;
	}
}

ProcessManager::ProcessHandle ProcessManager::chainProcess(const ProcessHandle& chainToHandle, std::unique_ptr<ProcessImplementation> implementation)
{
	if (chainToHandle.isValid())
	{
		auto process = std::unique_ptr<Process>(new Process(std::move(implementation)));
		ProcessHandle handle(process.get(), this);
		chainToHandle.process->setSuccessorProcess(std::move(process));

		return handle;
	}
	else
	{
		return ProcessHandle();
	}
}

bool ProcessManager::hasProcess(const Process* process) const
{
	auto processIt = std::find_if(
			this->processes.begin(),
			this->processes.end(),
			[process] (const std::unique_ptr<Process>& containedProcess)
			{
				return (process == containedProcess.get());
			}
	);

	return (processIt != this->processes.end());
}

