#ifndef PROCESSMANAGER_H_
#define PROCESSMANAGER_H_

#include <vector>
#include <memory>
#include "Process.h"
#include "ProcessImplementation.h"

/**
 * Manager of running processes.
 * Handles a list of processes and updates them.
 */
class ProcessManager
{
public:
	/**
	 * Handle to a process.
	 * Used to interact with a running Process in the ProcessManager.
	 */
	class ProcessHandle
	{
		friend class ProcessManager;

	public:
		ProcessHandle();
		ProcessHandle(Process* process, ProcessManager* manager);

		/**
		 * Check if the handle is handle to the passed process pointer.
		 */
		bool isHandleToProcess(const Process* process) const;

		/**
		 * Check if the handle is valid.
		 * The handle is valid if the handle's process is running in
		 * the manager.
		 */
		bool isValid() const;

	private:
		Process* process;
		ProcessManager* manager;
	};

	ProcessManager();
	virtual ~ProcessManager();

	/**
	 * Run the update tick for all processes.
	 */
	void updateProcesses(float deltaTime);

	/**
	 * Start a process from a ProcessImplementation.
	 * @param implementation Implementation to start.
	 * @return Handle to the process started.
	 */
	ProcessHandle startProcess(std::unique_ptr<ProcessImplementation> implementation);

	/**
	 * Chain a ProcessImplementation to an already started process.
	 * The process chained will be started automatically if the process
	 * chained to succeeds its execution.
	 * @param chainToHandle Handle to running process to chain to.
	 * @param implementation Implementation to chain.
	 * @return Handle to the chained process. Not valid until process started.
	 */
	ProcessHandle chainProcess(const ProcessHandle& chainToHandle, std::unique_ptr<ProcessImplementation> implementation);

	/**
	 * Abort a process.
	 * The process will reach the ProcessImplementation::onAbort() function.
	 * @param handle Handle to process to abort.
	 * @return true if process aborted, false if it isn't valid.
	 */
	bool abortProcess(const ProcessHandle& handle);

	/**
	 * Check if the manager has the process specified.
	 */
	bool hasProcess(const Process* process) const;

private:
	std::vector<std::unique_ptr<Process>> processes;
};

#endif /* PROCESSMANAGER_H_ */
