#ifndef PROCESSIMPLEMENTATION_H_
#define PROCESSIMPLEMENTATION_H_

#include <memory>

class Process;

/**
 * Abstract class used for implementing processes.
 * Inherit this and override its method to create your own process implementation.
 */
class ProcessImplementation
{
public:
	virtual ~ProcessImplementation();

	/**
	 * Called when process is initialized from the ProcessManager.
	 */
	virtual void onInit() = 0;

	/**
	 * Called each update tick of the ProcessManager.
	 */
	virtual void onUpdate(float deltaTime) = 0;

	/**
	 * Called when process succeeds its execution.
	 */
	virtual void onSuccess() = 0;

	/**
	 * Called when process fails its execution.
	 */
	virtual void onFail() = 0;

	/**
	 * Called when process execution was aborted.
	 * This happens when user explicitly aborts the process.
	 */
	virtual void onAbort() = 0;

	void onAttachToProcess(Process* process);

protected:
	/**
	 * Set the state of the process to succeeded.
	 * The process will terminate an its successors
	 * will be started.
	 */
	void succeedExecution();

	/**
	 * Set the state of the process to failed.
	 * The process will terminate.
	 */
	void failExecution();

private:
	Process* process;
};

#endif /* PROCESSIMPLEMENTATION_H_ */
