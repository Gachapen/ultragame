#include "ProcessImplementation.h"

#include <cassert>
#include "Process.h"

ProcessImplementation::~ProcessImplementation()
{
}

void ProcessImplementation::onAttachToProcess(Process* process)
{
	this->process = process;
}

void ProcessImplementation::succeedExecution()
{
	assert(this->process != nullptr);
	this->process->succeedExecution();
}

void ProcessImplementation::failExecution()
{
	assert(this->process != nullptr);
	this->process->failExecution();
}
