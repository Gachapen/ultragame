#ifndef MODEHANDLER_H_
#define MODEHANDLER_H_

#include <string>
#include <map>
#include <memory>
#include "Scene.h"

class SceneManager {
public:
	SceneManager();
	virtual ~SceneManager();
	
	/**
	 * Adds a scene to the manager.
	 * @param name the name of the scene.
	 * @param scene The scene to be added.
	 */
	void addScene(const std::string& name, const std::shared_ptr<Scene>& scene);
	
	/**
	 * Removes a scene from the manager.
	 * @param name The name of the scene to be removed.
	 */
	bool removeScene(const std::string& name);
	
	/**
	 * Changes to the scene specified.
	 * @param name the scene to change to.
	 */
	void changeScene(const std::string& name);
	
	/**
	 * clears the scene-map
	 */
	void clearScenes();

	/**
	 * Calls the active scenes onRender.
	 */
	void onRender();
	
	/**
	 * Calls the active scenes onUpdate.
	 * @param deltaTime The time passed since the last time.
	 */
	void onUpdate(float deltaTime);
	
	/**
	 * Calls the active scenes onEvent.
	 * @param event The event to be processed.
	 */
	void onEvent(const SDL_Event& event);

private:
	std::map<std::string, std::shared_ptr<Scene> > scenes;
	std::string nextScene;
	std::shared_ptr<Scene> activeScene;
};

#endif /* MODEHANDLER_H_ */
