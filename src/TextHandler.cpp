#include "TextHandler.h"
#include "FontHandler.h"
#include "GeometrySet.h"

#include "log.h"


TextHandler::TextHandler(GeometrySet* set)
{
	this->geometrySet = set;
	this->textColor = glm::vec4(1.0f,1.0f,1.0f,1.0f);
	this->text = "";
}

TextHandler::~TextHandler()
{
}

void TextHandler::reCreateQuads()
{
	for (Quad* quad : this->characters) {
		this->geometrySet->removeGeometry(quad);
	}

	this->characters.resize(text.length());

	for (unsigned int i = 0; i < this->text.length(); i++) {
		
		Quad* characterGeometry = this->geometrySet->createQuad();
		characterGeometry->setColor(this->textColor);
		this->characters[i] = characterGeometry;
	}
}

void TextHandler::setText(std::string text)
{
	this->text = text;

	if (text.length() != this->characters.size()) {
		this->reCreateQuads();
	}

	this->updateTextSize();
	this->updateTextBox();
}

void TextHandler::setPosition(const glm::vec2& position)
{
	this->position = position;
	this->updateTextBox();
}

void TextHandler::setSize(const glm::vec2& size)
{
	this->size = size;
	this->updateTextBox();
}

void TextHandler::reCreateRenderData()
{
	this->reCreateQuads();
	this->updateTextBox();
}

void TextHandler::updateTextBox()
{
	glm::ivec2 textPosition = (glm::ivec2)this->position;

	float xScaleFactor = this->size.x / this->textSize.x;
	float yScaleFactor = this->size.y / this->textSize.y;
	
	float lowestYOffset = std::numeric_limits<float>::max();

	for (char letter : this->text) {
		CharacterData data = FontHandler::getCharacterData(letter);
		if (data.yoffset < lowestYOffset) {
			lowestYOffset = data.yoffset;
		}
	}

	int nextCharacterXPosition = 0;

	for (unsigned int i = 0; i < this->text.length(); i++) {
		char character = this->text[i];
		CharacterData data = FontHandler::getCharacterData(character);
		Sprite sprite = FontHandler::getSprite(character);
		
		glm::vec2 charSize;
		charSize.x = data.width * xScaleFactor;
		charSize.y = data.height * yScaleFactor;

		glm::vec2 charPosition;
		charPosition.x = textPosition.x + (data.xoffset * xScaleFactor) + (nextCharacterXPosition * xScaleFactor);
		charPosition.y = textPosition.y + (data.yoffset * yScaleFactor);

		Quad* characterGeometry = this->characters[i];
		characterGeometry->setTexture(sprite);
		characterGeometry->setVertices(glm::vec2(charPosition.x, charPosition.y),
					       glm::vec2(charPosition.x + charSize.x, charPosition.y),
					       glm::vec2(charPosition.x + charSize.x, charPosition.y + charSize.y),
					       glm::vec2(charPosition.x, charPosition.y + charSize.y));

		characterGeometry->signalChange();
		
		nextCharacterXPosition += data.xadvance;
	}
}

void TextHandler::setColor(glm::vec4 color)
{
	this->textColor = color;

	for (Quad* quad : this->characters) {
		quad->setColor(this->textColor);
	}
}

void TextHandler::updateTextSize()
{
	int textTotalWidth = 0;

	CharacterSetData setData = FontHandler::getCharacterSetData();

	for (unsigned int i = 0; i < this->text.length(); i++) {
		char character = this->text[i];
		CharacterData data = FontHandler::getCharacterData(character);

		textTotalWidth += data.xoffset;

		if (i < this->text.length() - 1) {
			textTotalWidth += data.xadvance;
		} else {
			textTotalWidth += data.width;
		}
	}

	this->textSize = glm::vec2(textTotalWidth, setData.lineHeight);
}

const glm::ivec2& TextHandler::getTextSize()
{
	return this->textSize;
}
