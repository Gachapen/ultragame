#include <string>
#include "Application.h"
#include "log.h"

#ifdef __IPHONEOS__
#include "ios/GameCenterAndGooglePlayHandler.h"
#endif

Application app;


/**
 * If the device is running iOS we ned to set the loop up as a call Back Function.
 * We den tell SDL to use that callback to present the screen.
 */
#ifdef __IPHONEOS__
extern "C"
{
    void ShowFrame(void*)
    {
        if (app.isRunning())
        {
            app.handleEvents();
            app.update();
            app.render();            
        }
        else
        {
            SDL_Quit();
        }
    }
}
#endif

int main(int argc, char* argv[])
{
	LOGD("Entering main.\n");

	if (app.init() == false) {
		return 1;
	}

#if __IPHONEOS__
	GameCenterAndGooglePlayHandler::setWindow(app.getWindow());

	app.setRunning(true);
	SDL_iPhoneSetAnimationCallback(app.getWindow(), 1, ShowFrame,0);
	return 0;
#else
	return app.execute();
#endif

	LOGD("Exiting main.\n");
}
