#ifndef MODE_H_
#define MODE_H_

#include <SDL2/SDL.h>

class SceneManager;

/**
 * A scene represents what is currently presented to the user. One scene is active at a time.
 * The current active screen, managed by the SceneManager lets the user interact with it.
 */
class Scene {
public:
	Scene();
	virtual ~Scene();

	/**
	 * Sets the manager for the scene.
	 * @param manager The SceneManager to handle the scenes.
	 */
	void setHandler(SceneManager* manager);

	/**
	 * Handles the rendering of the scene
	 */
	virtual void onRender();
	
	/**
	 * Handles the updating of the scene
	 * @param deltaTime The time passed since the last update.
	 */
	virtual void onUpdate(float deltaTime);
	
	/**
	 * Handles any events that occur.
	 * @param event Event passed by SDL.
	 */
	virtual void onEvent(const SDL_Event& event);

	/**
	 * Handles setup of the scene
	 */
	virtual void onCreate();
	
	/**
	 * Called when the scene is set as active.
	 */
	virtual void onActivate();
	
	/**
	 * Called when the Scene is deactivated
	 */
	virtual void onDeactivate();
	
	/**
	 * Called when the scene is destroyed.
	 */
	virtual void onDestroy();

protected:
	SceneManager* manager;
};

#endif /* MODE_H_ */
