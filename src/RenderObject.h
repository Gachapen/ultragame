#ifndef RENDEROBJECT_H_
#define RENDEROBJECT_H_

#include <glm/glm.hpp>

class RenderObject {
public:
	virtual ~RenderObject()
	{}

	
	/**
	 * Updates the position.
	 * @param position the new position.
	 */
	void updatePosition(const glm::vec2& position);
	
	/**
	 * Updates color of the object
	 * @param color The new color.
	 */
	void updateColor(const glm::vec4& color);

protected:
	
	/**
	 * Updates the rendering to the new position.
	 * @param position the new position.
	 */
	virtual void onPositionUpdated(const glm::vec2& position) = 0;
	
	/**
	 * Updates the render color of the object
	 * @param color The new color.
	 */
	virtual void onColorUpdated(const glm::vec4& color) = 0;

private:
	glm::vec2 position;
	glm::vec4 color;
};

#endif /* RENDEROBJECT_H_ */
