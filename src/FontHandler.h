//
//  FontHandler.h
//  ultragame
//
//  Created by Asbjoern Sporaland on 03/12/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#ifndef __ultragame__FontHandler__
#define __ultragame__FontHandler__

#include <string>
#include <unordered_map>
#include "pugixml/pugixml.hpp"
#include "spriteloader/Sprite.h"

/**
 * Holds data about a specific character in the font-map.
 */
struct CharacterData
{
	int x;
	int y;
	float xFraction;
	float yFraction;
	int width;
	int height;
	float widthFraction;
	float heightFraction;
	int xoffset;
	int yoffset;
	float xOffsetFraction;
	float yOffsetFraction;
	int xadvance;
	int page;
	int chnl;
};

/**
 * Holds data about the fontmap.
 */
struct CharacterSetData {
	int lineHeight;
	int base;
};

/**
 * The fonthandler handles fontmaps and getting the character data from those.
 */
class FontHandler
{
public:
	/**
	 * Reads the fontmap file and saves the data about the characters
	 * @param path The path for the fontmap.
	 */
	static void readFontFile(std::string path);
	
	/**
	 * Gets Data about a specific character.
	 * @param character The character you want info about.
	 */
	static const CharacterData& getCharacterData(const char character);
	
	/**
	 * Gets the sprite of a character.
	 * @param character the character we want the sprite from.
	 */
	static Sprite getSprite(const char character);
	
	/**
	 * Gets Data on the character set.
	 */
	static const CharacterSetData& getCharacterSetData();
	
private:
	static CharacterSetData characterSetData;
	static std::unordered_map<char, CharacterData> characterMap;
	static Sprite fontSprite;
	static glm::ivec2 atlasSize;
};


#endif /* defined(__ultragame__FontHandler__) */
