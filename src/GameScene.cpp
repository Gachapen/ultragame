#include "GameScene.h"

#include <memory>
#include <sstream>
#include <algorithm>

#include "log.h"
#include "SceneManager.h"
#include "MusicManager.h"
#include "gui/ListView.h"
#include "gui/Label.h"
#include "physics/PhysicsQuadShape.h"
#include "Transition.h"

#include "GamePlayRenderer.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


#ifdef __APPLE__
#include "IosFileSaver.h"
#include "ios/GameCenterAndGooglePlayHandler.h"
#else
#include "GeneralFileSaver.h"
#endif

#ifdef __ANDROID__
#include "android/AndroidApplication.h"
#endif



GameScene::GameScene():
hits(0),
accelerometer(nullptr),
platformMaxTilt(32767.0f),
platformTopBody(nullptr),
platformBottomBody(nullptr),
playerBody(nullptr),
playerSizeScaling(1.0f),
playerBoxTravelDistance(0.0f),
playerBoxDesiredTravelTime(0.0f),
playerBoxTravelTimeLeft(0.0f),
playerTiltDampening(15.0f),
firstColor(0),
firstColorIsIncreasing(true),
middleXPosition(0.0f),
playerDeathSound(nullptr),
paused(false),
stage(0),
NUM_STAGES(5)
{
}

GameScene::~GameScene()
{
}

void GameScene::onCreate()
{
	MusicManager::getInstance()->loadSet("game");
	
	this->accelerometer = SDL_JoystickOpen(0);
	if (accelerometer == NULL) {
		LOGE("Failed to get Joystick.\n");
	} else {
		LOG("Joystick \"%s\" registered with %i axes.\n", SDL_JoystickName(accelerometer), SDL_JoystickNumAxes(accelerometer));
	}
	
	// TODO: Should be in XML file, but will duplicate the android resource file.
	this->scoreAchievementRequirement[30] = "achievement_meh";
	this->scoreAchievementRequirement[60] = "achievement_nice";
	this->scoreAchievementRequirement[100] = "achievement_cool";
	this->scoreAchievementRequirement[150] = "achievement_awesome";
	this->scoreAchievementRequirement[200] = "achievement_ultra";
	
	this->renderer = std::unique_ptr<Renderer>(new GamePlayRenderer());
	GLuint name = Assets::getInstance()->spriteManager->getTextureBuffer(0);
	this->renderer->init(this->screenSize.x, this->screenSize.y, name, GL_TEXTURE0);
	
	std::vector<Attribute> attributes;
	
	Attribute position = { "position", 0, 2, GL_FLOAT,
		GL_FALSE, sizeof(ObjectCoordinate),
		(void*)offsetof(ObjectCoordinate, geometryVertex)
	};
	
	Attribute color = { "color",1, 4, GL_FLOAT, GL_FALSE,
		sizeof(ObjectCoordinate),
		(void*)offsetof(ObjectCoordinate, color)
	};
	
	Attribute textureCoordinate = { "textureCoordinate", 2, 4, GL_FLOAT, GL_FALSE,
		sizeof(ObjectCoordinate),
		(void*)offsetof(ObjectCoordinate, textureCoordinate)
		
	};
	
	attributes.push_back(position);
	attributes.push_back(color);
	attributes.push_back(textureCoordinate);
	
	
	this->renderer->createShaderProgramAndBindUniforms("simpleShader", "SimpleColor",
							   attributes,
							   [this] (GLuint shaderProgram) {
								   
								   std::unordered_map<std::string, GLint> uniforms;
								   std::string name = "modelViewProjectionMatrix";
								   GLint modelViewProjectionUniform = glGetUniformLocation(shaderProgram, name.c_str());
								   assert(modelViewProjectionUniform >= 0);
								   
								   
								   //NOTE: THE Y AXIS IS INVERTED!!!! (LIKE SDL)
								   glm::mat4 modelViewProjectionMatrix = glm::ortho(0.0f, (float)this->screenSize.x,
														    (float)this->screenSize.y, 0.0f);
								   
								   glUseProgram(shaderProgram);
								   glUniformMatrix4fv(modelViewProjectionUniform,
										      1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
								   glUseProgram(0);
								   
								   
								   uniforms[name] = modelViewProjectionUniform;
								   
								   std::string renderTexture = "renderTexture";
								   GLint textureUniform = glGetUniformLocation(shaderProgram, renderTexture.c_str());
								   assert(textureUniform >= 0);
								   
								   uniforms[renderTexture] = textureUniform;
								   
								   return uniforms;
							   }
							   );
	
	this->gameplayQuads = std::make_shared<GeometrySet>(GeometryType::QUAD);
	this->renderer->addGeometrySet(this->gameplayQuads);
	this->pauseMenuQuads = std::make_shared<GeometrySet>(GeometryType::QUAD);
	this->renderer->addGeometrySet(this->pauseMenuQuads);
	
	this->worldSize.width = 1200.0f;
	this->worldSize.height = 1920.0f;
	
	this->worldCenter.x = this->worldSize.width / 2.0f;
	this->worldCenter.y = this->worldSize.height / 2.0f;
	
	this->worldToScreenScale.x = (float) this->screenSize.x / this->worldSize.width;
	this->worldToScreenScale.y = (float) this->screenSize.y / this->worldSize.height;
	
	this->playerSize.width = this->playerSize.height = 96.0f;
	
	this->basePlatformWidth = 600.0f;
	this->platformWidthDecrease = 1.0f/3.0f;

	this->platformSize.width = basePlatformWidth;
	this->platformSize.height = 72.0f;
	this->playerBoxTravelDistance = this->worldSize.height - (2 * this->platformSize.height) - this->playerSize.height;
	
	PhysicsQuadShape playerShape(this->playerSize.width, this->playerSize.height);
	PhysicsQuadShape platformShape(this->platformSize.width, this->platformSize.height);
	
	this->playerBody = this->physics.createBody();
	this->playerBody->setShape(&playerShape);
	this->playerRenderObject = QuadRenderObject(this->gameplayQuads->createQuad(), glm::vec2(this->playerSize.width, this->playerSize.height) * this->worldToScreenScale);
	this->playerBody->setPositionChangedCallback(
						     [this](const glm::vec2& position) {
							     this->playerRenderObject.updatePosition(position * this->worldToScreenScale);
						     }
						     );
	
	this->platformBottomBody = this->physics.createBody();
	this->platformBottomBody->setShape(&platformShape);
	this->platformBottomBody->setSimulated(false);
	this->platformBottomBody->setPosition(glm::vec2(this->worldCenter.x, this->worldSize.height - (this->platformSize.height / 2.0f)));
	this->platformBottomRenderObject = QuadRenderObject(this->gameplayQuads->createQuad(), glm::vec2(this->platformSize.width, this->platformSize.height) * this->worldToScreenScale);
	this->platformBottomBody->setPositionChangedCallback(
							     [this](const glm::vec2& position) {
								     this->platformBottomRenderObject.updatePosition(position * this->worldToScreenScale);
							     }
							     );
	
	this->platformTopBody = this->physics.createBody();
	this->platformTopBody->setShape(&platformShape);
	this->platformTopBody->setSimulated(false);
	this->platformTopBody->setPosition(glm::vec2(this->worldCenter.x, 0.0f) + (this->platformSize.height / 2.0f));
	this->platformTopRenderObject = QuadRenderObject(this->gameplayQuads->createQuad(), glm::vec2(this->platformSize.width, this->platformSize.height) * this->worldToScreenScale);
	this->platformTopBody->setPositionChangedCallback(
							  [this](const glm::vec2& position) {
								  this->platformTopRenderObject.updatePosition(position * this->worldToScreenScale);
							  }
							  );
	
	this->platformSlideDuration = std::chrono::duration<float>(0.5f);
	
	this->hitsToStage[1] = 10;
	this->hitsToStage[2] = 30;
	this->hitsToStage[3] = 60;
	this->hitsToStage[4] = 100;
	
	this->accelerometer = SDL_JoystickOpen(0);
	if (accelerometer == NULL) {
		LOGE("Failed to get Joystick.\n");
	} else {
		LOG("Joystick \"%s\" registered with %i axes.\n", SDL_JoystickName(accelerometer), SDL_JoystickNumAxes(accelerometer));
	}
	
#ifdef __APPLE__
	this->platformMaxTilt = 5500.0f;
#elif defined(__ANDROID__)
	this->platformMaxTilt = 32767.0f;
#endif
	
	srand(time(nullptr));
	//
	//	this->platformHitSound = Mix_LoadWAV(Assets::getInstance()->getPrependAssetsRootPath("hit.ogg").c_str());
	//	if (this->platformHitSound == nullptr) {
	//	    LOGE("Could not load platform hit sound.");
	//	}
	
	this->playerDeathSound = Mix_LoadWAV(Assets::getInstance()->getPrependAssetsRootPath("death.ogg").c_str());
	if (this->playerDeathSound == nullptr) {
		LOGE("Could not load player death sound.");
	}
	
	this->setupPauseMenu();
	
	this->physics.setCollisionListener(this);
}

void GameScene::onActivate()
{
	MusicManager::getInstance()->playRandomTrack("game");
	
#ifdef __ANDROID__
	AndroidApplication::getInstance()->showStatusAndNavBar(false);
	AndroidApplication::getInstance()->keepScreenOn(true);
#endif
	
	this->initGame();
}

void GameScene::onDeactivate()
{
#ifdef __ANDROID__
	AndroidApplication::getInstance()->keepScreenOn(false);
#endif
	
	this->quitGame();
}

void GameScene::onDestroy()
{
	Mix_FreeChunk(this->playerDeathSound);
}

void GameScene::onCollision(PhysicsBody* firstBox, PhysicsBody* secondBox)
{
	this->hits++;
	if (this->stage < this->NUM_STAGES - 1 && this->hits >= this->hitsToStage[this->stage + 1]) {
		this->stage += 1;
		
		float width = this->platformSize.width - this->platformSize.width * this->platformWidthDecrease;
		this->updatePlatformWidths(width);
		
		LOG("Stage: %i\n", this->stage);
	}
	
	auto achievementIt = this->scoreAchievementRequirement.find(this->hits);
	
	if (achievementIt != this->scoreAchievementRequirement.end()) {
#ifdef __ANDROID__
		AndroidApplication::getInstance()->unlockAchievement(achievementIt->second);
#endif
#ifdef __APPLE__
		GameCenterAndGooglePlayHandler::unlockAchievement(achievementIt->second);
#endif
	}
	
	if (firstBox == this->playerBody || secondBox == this->playerBody) {
		this->playerPlatformHit();
	}
}

void GameScene::initGame()
{
	this->paused = false;
	this->hits = 0;
	this->playerBoxDesiredTravelTime = 4.0f;
	this->playerBoxTravelTimeLeft = 0.0f;
	
	this->stage = 0;

	glm::vec2 playerPosition(this->worldCenter.x, this->platformSize.height);
	
	this->playerBody->init();
	this->playerBody->setPosition(playerPosition);
	
	this->platformSize.width = this->basePlatformWidth;
	this->updatePlatformWidths(this->platformSize.width);

	this->addNewBox(false);
	this->playerPlatformHit();
}

void GameScene::quitGame()
{
}

void GameScene::onUpdate(float deltaTime)
{
	this->processManager.updateProcesses(deltaTime);
	
	if (this->paused == false) {
		float boxBottom = this->playerBody->getPosition().y - this->playerBody->getShape()->getBottom();
		float boxTop = this->playerBody->getPosition().y - this->playerBody->getShape()->getTop();
		
		if (boxTop > this->worldSize.height || boxBottom < 0.0f)
		{
			this->onGameOver();
		}
		
		for (const std::unique_ptr<BlackHole>& blackHole : this->blackHoles)
		{
			glm::vec2 force = blackHole->calculateGravitationalEffect(this->playerBody, BlackHole::GravitationCalculation::SIMPLE);
			force.y = 0.0f;
			this->playerBody->addForce(force);
		}
		
		float previousPlayerVerticalCenter = this->playerBody->getPosition().y;
		
		float ax = SDL_JoystickGetAxis(this->accelerometer, 0);
		float newXposition = this->worldCenter.x + ((ax/this->platformMaxTilt) * this->worldSize.width);
		
		float playerForce = 1.5;
		float seperation = newXposition - this->playerBody->getPosition().x;
		this->playerBody->addImpulse(glm::vec2(seperation * playerForce, 0.0f));
		
		this->playerBoxTravelTimeLeft -= deltaTime;
		float playerTravelDampening = this->playerBoxTravelTimeLeft / this->playerBoxDesiredTravelTime;
		this->playerBody->setDampening(glm::vec2(this->playerTiltDampening, playerTravelDampening));
		
		this->physics.update(deltaTime);
		
		
		float currentPlayerVerticalCenter = this->playerBody->getPosition().y;
		
		if (previousPlayerVerticalCenter < worldCenter.y && currentPlayerVerticalCenter >= worldCenter.y) {
			this->addNewBox(true);
		}
		else if (previousPlayerVerticalCenter > worldCenter.y && currentPlayerVerticalCenter <= worldCenter.y) {
			this->addNewBox(false);
		}
	}
}

void GameScene::onRender()
{
	if (this->firstColorIsIncreasing) {
		this->firstColor += 0.02f;
		
		if (this->firstColor >= 1.0f) {
			this->firstColor = 1.0f;
			this->firstColorIsIncreasing = false;
		}
	} else {
		this->firstColor -= 0.02f;
		
		if (this->firstColor <= 0.3f) {
			this->firstColor = 0.3f;
			this->firstColorIsIncreasing = true;
		}
	}
	
	this->setObjectColor(this->playerBody, &this->playerRenderObject);
	this->setObjectColor(this->platformBottomBody, &this->platformBottomRenderObject);
	this->setObjectColor(this->platformTopBody, &this->platformTopRenderObject);
	
	if (this->paused == true) {
		if (this->resumeButton->isClicked() == false) {
			SDL_Color resumeColor;
			resumeColor.r = ((sin(SDL_GetTicks() / 200.0) + 1.0) / 2.0) * 100;
			resumeColor.g = this->firstColor * 0.7;
			resumeColor.b = 0;
			resumeColor.a = 210;
			this->resumeButton->setCurrentColor(resumeColor);
		}
		
		if (this->quitButton->isClicked() == false) {
			SDL_Color quitColor;
			quitColor.r = this->firstColor * 0.7;
			quitColor.g = 0;
			quitColor.b = ((cos(SDL_GetTicks() / 200.0) + 1.0) / 2.0) * 100;
			quitColor.a = 210;
			this->quitButton->setCurrentColor(quitColor);
		}
	}
	
	this->renderer->render();
}

void GameScene::onEvent(const SDL_Event& event)
{
	if (this->paused == true) {
		if (event.type == SDL_MOUSEBUTTONDOWN || event.type == SDL_MOUSEBUTTONUP) {
			this->pauseMenu->onMouseButtonEvent(event.button);
		} else if (event.type == SDL_MOUSEMOTION) {
			this->pauseMenu->onMouseMoveEvent(event.motion);
		} else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_AC_BACK) {
			this->pauseGame(false);
		}
	} else {
		if (event.type == SDL_MOUSEBUTTONUP) {
			this->pauseGame(true);
		}
		
		if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_AC_BACK) {
			this->manager->changeScene("main_menu");
		}
	}
	
	if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED) {
		this->screenSize.x = event.window.data1;
		this->screenSize.y = event.window.data2;
		LOG("Game scene resize: %ix%i\n", event.window.data1, event.window.data2);
	}
}

void GameScene::setWindowSize(SDL_Point size)
{
	this->screenSize = glm::ivec2(size.x, size.y);
}

void GameScene::addNewBox(bool isAddingToTopOfScreen)
{
	PhysicsBody* boxAdded = nullptr;
	
	if (isAddingToTopOfScreen == true) {
		boxAdded = this->platformTopBody;
	} else {
		boxAdded = this->platformBottomBody;
	}
	
	boxAdded->setDisabled(false);
}

void GameScene::updatePlatformWidths(float width)
{
	auto resizeDuration = std::chrono::duration<float>(0.4f);

	std::unique_ptr<Transition> resizeTransition(new Transition());
	resizeTransition->setValues(this->platformSize.width, width);
	resizeTransition->setDuration(resizeDuration);
	resizeTransition->setUpdateCallback(
		[this](float size) {
			PhysicsQuadShape platformShape(size, this->platformSize.height);

			this->platformTopBody->setShape(&platformShape);
			this->platformBottomBody->setShape(&platformShape);
			this->platformTopRenderObject.updateWidth(size * this->worldToScreenScale.x);
			this->platformBottomRenderObject.updateWidth(size * this->worldToScreenScale.x);

			this->platformSize.width = size;
		}
	);

	this->processManager.startProcess(std::move(resizeTransition));
}


void GameScene::onGameOver()
{
	Mix_PlayChannel(-1, this->playerDeathSound, 0);
	
	std::unique_ptr<IFileSaver> saver;
#ifdef __APPLE__
	saver = std::unique_ptr<IFileSaver>(new IosFileSaver());
#else
	saver = std::unique_ptr<IFileSaver>(new GeneralFileSaver());
#endif
	
	if (saver) {
		std::string previousTopScoreString = saver->readFileAsString("savefile.txt");
		unsigned int previousTopScore = 0;
		
		if (previousTopScoreString != "") {
			previousTopScore = atoi(previousTopScoreString.c_str());
		}
		
		if (this->hits > previousTopScore) {
			std::ostringstream stream;
			stream << this->hits;
			saver->writeString(stream.str(), "savefile.txt");
			
#ifdef __ANDROID__
			AndroidApplication::getInstance()->submitLeaderBoardScore(this->hits);
#endif
#ifdef __APPLE__
			GameCenterAndGooglePlayHandler::submitGameCenterScore(this->hits);
			GameCenterAndGooglePlayHandler::submitGoogleScore(this->hits);
#endif
			
		}
	}
	this->manager->changeScene("main_menu");
}

void GameScene::playerPlatformHit()
{
	LOG("Desired travel time: %f\n", this->playerBoxDesiredTravelTime);
	
	float desiredVelocity = this->playerBoxTravelDistance / this->playerBoxDesiredTravelTime;
	float playerBounceImpulse = this->playerBody->getMass() * desiredVelocity * 5.0f;
	
	PhysicsBody* bodyHit = nullptr;
	bool isAtBottom = (this->playerBody->getPosition().y > this->worldCenter.y);
	if (isAtBottom) {
		this->playerBody->addImpulse(glm::vec2(0.0, -playerBounceImpulse));
		bodyHit = this->platformBottomBody;
	} else {
		this->playerBody->addImpulse(glm::vec2(0.0, playerBounceImpulse));
		bodyHit = this->platformTopBody;
	}
	
	glm::vec2 newPosition(0.0f, 0.0f);
	newPosition.x = (rand() % (((int) this->worldSize.width - (int) this->platformSize.width) * 10)) / 10.0f;
	newPosition.x += this->platformSize.width / 2;
	
	bodyHit->setDisabled(true);
	
	this->platformSlideDuration = std::chrono::duration<float>(this->playerBoxDesiredTravelTime - 1.0f);
	
	std::unique_ptr<Transition> slideTransition(new Transition());
	slideTransition->setValues(bodyHit->getPosition().x, newPosition.x);
	slideTransition->setDuration(this->platformSlideDuration);
	
	slideTransition->setUpdateCallback(
					   [bodyHit](float position) {
						   bodyHit->setPosition(glm::vec2(position, bodyHit->getPosition().y));
					   }
					   );
	
	this->processManager.startProcess(std::move(slideTransition));
	
	this->playerBoxTravelTimeLeft = this->playerBoxDesiredTravelTime;
	this->playerBoxDesiredTravelTime -= 1.1f / ((this->hits * this->hits) + 1.0f);
}


void GameScene::onClick(const View& clickedElement)
{
	if (clickedElement == *this->resumeButton) {
		this->pauseGame(false);
	} else if (clickedElement == *this->quitButton) {
		this->pauseMenuQuads->setActive(false);
		this->manager->changeScene("main_menu");
	}
}

void GameScene::setupPauseMenu()
{
	float labelSizePercentage = 95.0f;
	
	this->pauseMenu = std::make_shared<View>();
	this->pauseMenu->setRenderQuad(this->pauseMenuQuads->createQuad());
	this->pauseMenu->setWidthPixels(this->screenSize.x);
	this->pauseMenu->setHeightPixels(this->screenSize.y);
	this->pauseMenu->setDefaultColor(SDL_Color { 0, 0, 0, 200 });
	
	auto menuButtonList = std::make_shared<ListView>(ListView::Orientation::VERTICAL);
	menuButtonList->setRenderQuad(this->pauseMenuQuads->createQuad());
	menuButtonList->setHeightSizeType(View::SizeType::STRETCH_PARENT);
	menuButtonList->setWidthSizeType(View::SizeType::STRETCH_PARENT);
	menuButtonList->setHeightPercentage(40.0f);
	menuButtonList->setWidthPercentage(70.0f);
	menuButtonList->setItemPlacement(ListView::ItemPlacement::SPACED);
	menuButtonList->setDefaultColor(SDL_Color { 0, 0, 0, 0 });
	this->pauseMenu->addChildView(menuButtonList);
	
	this->resumeButton = std::make_shared<Button>();
	this->resumeButton->setRenderQuad(this->pauseMenuQuads->createQuad());
	this->resumeButton->setWidthSizeType(View::SizeType::STRETCH_PARENT);
	this->resumeButton->setHeightSizeType(View::SizeType::STRETCH_PARENT);
	this->resumeButton->setHeightPercentage(30.0f);
	this->resumeButton->setDefaultColor(SDL_Color { 0, 200, 0, 255 });
	this->resumeButton->setPressedColor(SDL_Color { 50, 50, 50, 255 });
	this->resumeButton->addOnClickListener(this);
	menuButtonList->addChildView(this->resumeButton);
	
	auto resumeLabel = std::make_shared<Label>(this->pauseMenuQuads.get());
	resumeLabel->setHeightSizeType(View::SizeType::FIT_PARENT);
	resumeLabel->setWidthSizeType(View::SizeType::FIT_PARENT);
	resumeLabel->setTextColor(SDL_Color { 200, 200, 200, 255 });
	resumeLabel->setText(Localization::getInstance()->strings.get("resume"));
	resumeLabel->setWidthPercentage(labelSizePercentage);
	resumeLabel->setHeightPercentage(labelSizePercentage);
	this->resumeButton->addChildView(resumeLabel);
	
	this->quitButton = std::make_shared<Button>();
	this->quitButton->setRenderQuad(this->pauseMenuQuads->createQuad());
	this->quitButton->setWidthSizeType(View::SizeType::STRETCH_PARENT);
	this->quitButton->setHeightSizeType(View::SizeType::STRETCH_PARENT);
	this->quitButton->setHeightPercentage(30.0f);
	this->quitButton->setDefaultColor(SDL_Color { 200, 0, 0, 255 });
	this->quitButton->setPressedColor(SDL_Color { 50, 50, 50, 255 });
	this->quitButton->addOnClickListener(this);
	menuButtonList->addChildView(this->quitButton);
	
	auto quitLabel = std::make_shared<Label>(this->pauseMenuQuads.get());
	quitLabel->setHeightSizeType(View::SizeType::FIT_PARENT);
	quitLabel->setWidthSizeType(View::SizeType::FIT_PARENT);
	quitLabel->setTextColor(SDL_Color { 200, 200, 200, 255 });
	quitLabel->setText(Localization::getInstance()->strings.get("quit"));
	quitLabel->setWidthPercentage(labelSizePercentage);
	quitLabel->setHeightPercentage(labelSizePercentage);
	this->quitButton->addChildView(quitLabel);
	
	this->pauseMenuQuads->setActive(false);
}

void GameScene::setObjectColor(const PhysicsBody* physicsBody, RenderObject* renderObject)
{
	glm::vec2 position = physicsBody->getPosition();
	
	float secondColor = position.y / this->worldSize.height;
	float thirdColor = position.x / this->worldSize.width;
	
	renderObject->updateColor(glm::vec4(this->firstColor, secondColor, thirdColor, 1.0f));
}

void GameScene::pauseGame(bool pause)
{
	this->paused = pause;
	this->pauseMenuQuads->setActive(pause);
#ifdef __ANDROID__
	AndroidApplication::getInstance()->showStatusAndNavBar(pause);
	AndroidApplication::getInstance()->keepScreenOn(!pause);
#endif
}
