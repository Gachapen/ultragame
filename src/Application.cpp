#include "Application.h"

#include <memory>

#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_opengles2.h>

#ifdef __APPLE__
#include "IosGetLocale.h"
#include "ios/GameCenterAndGooglePlayHandler.h"
#elif __ANDROID__
#include "android/AndroidApplication.h"
#include "android/AndroidGetLocale.h"
#else
#include "GeneralGetLocale.h"
#endif

#include "log.h"
#include "GameScene.h"
#include "MainMenuScene.h"
#include "MusicManager.h"
#include "GeneralFileSaver.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "FontHandler.h"

int eventFilter(void* instance, SDL_Event* event) {
	Application* appInstance = static_cast<Application*>(instance);
	return appInstance->onEventFilter(event);
}

Application::Application():
	running(false),
	hasShutDown(false),
	window(nullptr),
	glContext(nullptr)
{
    srand((unsigned)time(NULL));
}

Application::~Application()
{
}

bool Application::init()
{
	this->hasShutDown = false;

#ifdef __ANDROID__
	AndroidApplication::getInstance()->initialize();
#endif

#ifdef __APPLE__
	GameCenterAndGooglePlayHandler::initGooglePlus();
	GameCenterAndGooglePlayHandler::signInGameCenterPlayer();
#endif

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_JOYSTICK | SDL_INIT_TIMER) != 0) {
		LOGE("Failed to init SDL: %s\n", SDL_GetError());
		return false;
	}

	SDL_SetEventFilter(&eventFilter, this);

	int windowWidth = 540;
	int windowHeight = 720;
	int windowFlags = SDL_WINDOW_OPENGL;

#if defined (__ANDROID__) || defined (__IPHONEOS__)
	SDL_DisplayMode displayMode;

	if (SDL_GetDesktopDisplayMode(0, &displayMode) != 0) {
		LOGE("Failed to get display mode: %s\n", SDL_GetError());
		return false;
	}

	windowWidth = displayMode.w;
	windowHeight = displayMode.h;
	windowFlags = SDL_WINDOW_FULLSCREEN | SDL_WINDOW_BORDERLESS;
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
#endif

	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 0);

	this->window = SDL_CreateWindow(this->windowTitle.c_str(), 0, 0, windowWidth, windowHeight, windowFlags);

	if (this->window == nullptr) {
		LOGE("Failed to create window: %s\n", SDL_GetError());
		return false;
	}

	this->glContext = nullptr;
	this->glContext = SDL_GL_CreateContext(window);
	if (!this->glContext) {
		LOGE("Failed to create GL context: %s\n", SDL_GetError());
	}

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glViewport(0, 0, windowWidth, windowHeight);

	int mixInitFlags = MIX_INIT_OGG;
	int inittedMixFlags = Mix_Init(mixInitFlags);
	if ((mixInitFlags & inittedMixFlags) != mixInitFlags) {
		LOGE("Failed to init SDL_mixer: %s\n", Mix_GetError());
		return false;
	}

	GLenum error = glGetError();
	LOG("GLError: %d\n", error);

	int glMajor = 0;
	int glMinor = 0;
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &glMajor);
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &glMinor);
	LOGD("OpenGL version: %d.%d\n", glMajor, glMinor);

	int profile;
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &profile);
	if (profile & SDL_GL_CONTEXT_PROFILE_ES) {
		LOGD("OpenGL profile_mask: ES\n");
	}

	Assets::create();
	Localization::create();
	MusicManager::create();
	
	Assets::getInstance()->init();

	FontHandler::readFontFile(Assets::getInstance()->getPrependAssetsRootPath("font_bitmap.xml"));

	std::unique_ptr<IGetLocale> localeGetter;
#ifdef __APPLE__
	localeGetter = std::unique_ptr<IGetLocale>(new IosGetLocale());
#elif __ANDROID__
	localeGetter = std::unique_ptr<IGetLocale>(new AndroidGetLocale());
#else
	localeGetter = std::unique_ptr<IGetLocale>(new GeneralGetLocale());
#endif

	Localization::getInstance()->loadLocalization(Assets::getInstance()->getPrependAssetsRootPath("locales.xml"));
	Localization::getInstance()->setLocale(localeGetter->getLocale().c_str());

	MusicManager::getInstance()->loadFile(Assets::getInstance()->getPrependAssetsRootPath("music.xml"));

	this->onResume();

	this->sceneHandler.clearScenes();

	std::shared_ptr<GameScene> gameScene = std::make_shared<GameScene>();
	gameScene->setWindowSize(SDL_Point { windowWidth, windowHeight });
	this->sceneHandler.addScene("game", gameScene);

	std::shared_ptr<MainMenuScene> mainMenu = std::make_shared<MainMenuScene>(this);
	mainMenu->setWindowSize(SDL_Point { windowWidth, windowHeight });
	this->sceneHandler.addScene("main_menu", mainMenu);

	this->sceneHandler.changeScene("main_menu");

	return true;
}


int Application::execute()
{
	LOGD("Initiating application loop.\n");

	this->running = true;
	while (this->running) {
		this->handleEvents();
		this->update();
		this->render();
	}

	LOGD("Exited application loop.\n");

	this->onPause();
	this->shutdown();

	return 0;
}

void Application::update()
{
	auto currentTime = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float> frameDuration = currentTime - this->previousFrameStartTime;
	this->previousFrameStartTime = currentTime;
    
	this->sceneHandler.onUpdate(frameDuration.count());
}

void Application::render()
{
	this->sceneHandler.onRender();
	SDL_GL_SwapWindow(this->window);
}


bool Application::isRunning() const
{
	return this->running;
}

void Application::setRunning(bool state)
{
	this->running = state;
}

int Application::onEventFilter(SDL_Event* event)
{
	if (event->type == SDL_APP_WILLENTERBACKGROUND) {
		LOGD("ENTER BACKGROUND\n");
		this->onPause();
		return 0;
	}

	if (event->type == SDL_APP_DIDENTERFOREGROUND) {
		LOGD("DID ENTER FOREGROUND\n");
		this->onResume();
		return 0;
	}

	if (event->type == SDL_APP_TERMINATING) {
		LOGD("TERMINATING\n");
		this->shutdown();
		return 0;
	}

	return 1;
}

void Application::handleEvents() {
	SDL_Event event;

	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED) {
			glViewport(0, 0, event.window.data1, event.window.data2);
		}

		if (event.type == SDL_QUIT) {
			LOGD("Received SDL quit event.\n");
			this->running = false;
		} else {
			this->sceneHandler.onEvent(event);
		}
	}
}

void Application::onPause()
{
	LOGD("Pausing application.\n");
	MusicManager::getInstance()->stopMusic();
	Mix_HaltChannel(-1);
	Mix_CloseAudio();
}

void Application::onResume()
{
	LOGD("Resuming application.\n");
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1) {
		LOGE("Failed to open audio: %s\n", Mix_GetError());
	}

	MusicManager::getInstance()->restorePreviouslyStopped();
}

void Application::shutdown()
{
	if (this->hasShutDown == false) {
		LOGD("Shutting application down.\n");

		Assets::getInstance()->unloadAssets();
		Assets::destroy();
		Localization::destroy();
		MusicManager::destroy();

		if (this->glContext)
		{
			LOGD("Deleted GL context.\n");
			SDL_GL_DeleteContext(this->glContext);
		}

		if (this->window)
		{
			LOGD("Destroyed window.\n");
			SDL_DestroyWindow(this->window);
		}

		Mix_Quit();
		SDL_Quit();

		this->hasShutDown = true;
	}
}

SDL_Window* Application::getWindow()
{
	return this->window;
}
