#ifndef QUADRENDEROBJECT_H_
#define QUADRENDEROBJECT_H_

#include "RenderObject.h"
#include "Geometry.h"

class QuadRenderObject: public RenderObject
{
public:
	QuadRenderObject();
	
	/**
	 * Creates a quad with a size and a quad for rendering.
	 * @param geometry the geometry used for rendering.
	 * @param size The size of the object.
	 */
	QuadRenderObject(Quad* geometry, const glm::vec2& size);
	
	void updateWidth(float width);
private:
	
	
	void onPositionUpdated(const glm::vec2& position) override;
	void onColorUpdated(const glm::vec4& color) override;

	Quad* quadGeometry;
	glm::vec2 size;
};

#endif /* QUADRENDEROBJECT_H_ */
