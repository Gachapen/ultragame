#include "MainMenuScene.h"

#include "log.h"
#include "SceneManager.h"

#include "Assets.h"
#include "Localization.h"
#include "MusicManager.h"
#include "MenuRenderer.h"

#ifdef __ANDROID__
#include "android/AndroidApplication.h"
#endif

#ifdef __APPLE__
#include "IosFileSaver.h"
#include "GameCenterAndGooglePlayHandler.h"
#else
#include "GeneralFileSaver.h"
#endif

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

MainMenuScene::MainMenuScene(Application* context):
menuViewBlueColorIncreasing(false),
menuViewGreenColorIncreasing(false),
startButtonRedIncreasing(true),
startButtonGreenIncreasing(false),
signedIntoGoogle(false),
context(context)
{
}

MainMenuScene::~MainMenuScene()
{
}

void MainMenuScene::onCreate()
{
	this->renderer = std::unique_ptr<Renderer>(new MenuRenderer());
	GLuint textureName = Assets::getInstance()->spriteManager->getTextureBuffer(0);
	this->renderer->init(this->windowSize.x, this->windowSize.y, textureName, GL_TEXTURE0);
	
	std::vector<Attribute> attributes;
	
	Attribute position = {	"position", 0, 2, GL_FLOAT,
		GL_FALSE, sizeof(ObjectCoordinate),
		(void*)offsetof(ObjectCoordinate, geometryVertex)
	};
	
	Attribute color = {	"color",1, 4, GL_FLOAT, GL_FALSE,
		sizeof(ObjectCoordinate),
		(void*)offsetof(ObjectCoordinate, color)
	};
	
	Attribute textureCoordinate = {	"textureCoordinate", 2, 4, GL_FLOAT, GL_FALSE,
		sizeof(ObjectCoordinate),
		(void*)offsetof(ObjectCoordinate, textureCoordinate)
		
	};
	
	attributes.push_back(position);
	attributes.push_back(color);
	attributes.push_back(textureCoordinate);
	
	this->renderer->createShaderProgramAndBindUniforms("simpleShader", "SimpleColor",
							   attributes,
							   [this] (GLuint shaderProgram) {
								   
								   std::unordered_map<std::string, GLint> uniforms;
								   
								   std::string name = "modelViewProjectionMatrix";
								   GLint modelViewProjectionUniform = glGetUniformLocation(shaderProgram, name.c_str());
								   assert(modelViewProjectionUniform >= 0);
								   
								   //NOTE: THE Y AXIS IS INVERTED!!!! (LIKE SDL)
								   glm::mat4 modelViewProjectionMatrix = glm::ortho(0.0f, (float)this->windowSize.x,
														 (float)this->windowSize.y, 0.0f);
								   
								   glUseProgram(shaderProgram);
								   glUniformMatrix4fv(modelViewProjectionUniform,
										      1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
								   glUseProgram(0);
								   
								   uniforms[name] = modelViewProjectionUniform;

								   
								   
								   std::string renderTexture = "renderTexture";
								   GLint textureUniform = glGetUniformLocation(shaderProgram, renderTexture.c_str());
								   assert(textureUniform >= 0);
								   
								   uniforms[renderTexture] = textureUniform;
								   
								   return uniforms;
							   }
							   );
	
	this->menuQuads = std::make_shared<GeometrySet>(GeometryType::QUAD);
	this->renderer->addGeometrySet(this->menuQuads);
	
	MusicManager::getInstance()->loadSet("menu");
	
	float labelSizePercentage = 90.0f;
	
	this->menuView.setName("menuView");
	this->menuView.setHorizontalPosition(0);
	this->menuView.setVerticalPosition(0);
	this->menuView.setWidthPixels(this->windowSize.x);
	this->menuView.setHeightPixels(this->windowSize.y);
	this->menuView.setClickable(true);
	this->menuView.addOnClickListener(this);
	
	this->buttonListView = std::make_shared<ListView>(ListView::Orientation::VERTICAL);
	this->buttonListView->setName("buttonListView");
	this->buttonListView->setWidthPercentage(80.0f);
	this->buttonListView->setHeightPercentage(80.0f);
	this->buttonListView->setPositioningTypeHorizontal(View::Position::CENTERED);
	this->buttonListView->setPositioningTypeVertical(View::Position::CENTERED);
	this->buttonListView->setPixelItemSpacing(60); // TODO: Percentage.
	this->buttonListView->setDefaultColor(SDL_Color { 0, 0, 0, 0 });
	this->menuView.addChildView(this->buttonListView);
	
	std::shared_ptr<Label> gameTitle = std::make_shared<Label>(this->menuQuads.get());
	gameTitle->setText(Localization::getInstance()->strings.get("game_title"));
	gameTitle->setTextColor(SDL_Color { 255, 240, 0, 255 });
	gameTitle->setPositioningTypeHorizontal(View::Position::CENTERED);
	gameTitle->setHeightSizeType(View::SizeType::FIT_PARENT);
	gameTitle->setWidthSizeType(View::SizeType::FIT_PARENT);
	this->buttonListView->addChildView(gameTitle);
	
	this->startButtonColor = {0, 200, 0, 255};
	this->startButton = std::make_shared<Button>();
	this->startButton->setRenderQuad(this->menuQuads->createQuad());
	this->startButton->setDefaultColor(this->startButtonColor);
	this->startButton->setPressedColor(SDL_Color { 50, 50, 50, 255 });
	this->startButton->setPositioningTypeHorizontal(View::Position::CENTERED);
	this->startButton->setHeightSizeType(View::SizeType::STRETCH_PARENT);
	this->startButton->setWidthSizeType(View::SizeType::STRETCH_PARENT);
	this->startButton->setHeightPercentage(20.0f);
	this->startButton->setWidthPercentage(80.0f);
	this->startButton->addOnClickListener(this);
	std::shared_ptr<Label> startButtonLabel = std::make_shared<Label>(this->menuQuads.get());
	startButtonLabel->setTextColor(SDL_Color { 200, 200, 200, 255 });
	startButtonLabel->setWidthSizeType(View::SizeType::FIT_PARENT);
	startButtonLabel->setHeightSizeType(View::SizeType::FIT_PARENT);
	startButtonLabel->setHeightPercentage(labelSizePercentage);
	startButtonLabel->setWidthPercentage(labelSizePercentage);
	//	startButtonLabel->setRenderQuad(this->menuQuads->createQuad());
	//	startButtonLabel->setDefaultColor(SDL_Color{0, 255, 0, 255});
	startButtonLabel->setText(Localization::getInstance()->strings.get("start_button"));
	this->startButton->addChildView(startButtonLabel);
	this->buttonListView->addChildView(this->startButton);
	
	auto topScoreView = std::make_shared<ListView>(ListView::Orientation::HORIZONTAL);
	topScoreView->setName("topScoreView");
	topScoreView->setRenderQuad(this->menuQuads->createQuad());
	topScoreView->setWidthSizeType(View::SizeType::STRETCH_PARENT);
	topScoreView->setHeightSizeType(View::SizeType::STRETCH_PARENT);
	topScoreView->setWidthPercentage(80.0f);
	topScoreView->setHeightPercentage(10.0f);
	topScoreView->setItemPlacement(ListView::ItemPlacement::SPREAD);
	this->buttonListView->addChildView(topScoreView);
	
	this->scoreTitleView = std::make_shared<View>();
	this->scoreTitleView->setRenderQuad(this->menuQuads->createQuad());
	this->scoreTitleView->setName("scoreTitleView");
	this->scoreTitleView->setWidthSizeType(View::SizeType::STRETCH_PARENT);
	this->scoreTitleView->setHeightSizeType(View::SizeType::STRETCH_PARENT);
	this->scoreTitleView->setWidthPercentage(70.0f);
	topScoreView->addChildView(this->scoreTitleView);
	
	auto scoreTitleLabel = std::make_shared<Label>(this->menuQuads.get());
	scoreTitleLabel->setName("scoreTitleLabel");
	scoreTitleLabel->setHeightSizeType(View::SizeType::FIT_PARENT);
	scoreTitleLabel->setWidthSizeType(View::SizeType::FIT_PARENT);
	scoreTitleLabel->setTextColor(SDL_Color { 200, 200, 200, 255 });
	scoreTitleLabel->setHeightPercentage(labelSizePercentage);
	scoreTitleLabel->setWidthPercentage(labelSizePercentage);
	//	scoreTitleLabel->setRenderQuad(this->menuQuads->createQuad());
	//	scoreTitleLabel->setDefaultColor(SDL_Color{0, 255, 0, 255});
	scoreTitleLabel->setText(Localization::getInstance()->strings.get("top_score"));
	this->scoreTitleView->addChildView(scoreTitleLabel);
	
	this->scoreLabelView = std::make_shared<View>();
	this->scoreLabelView->setRenderQuad(this->menuQuads->createQuad());
	this->scoreLabelView->setName("scoreLabelView");
	this->scoreLabelView->setWidthSizeType(View::SizeType::STRETCH_PARENT);
	this->scoreLabelView->setHeightSizeType(View::SizeType::STRETCH_PARENT);
	this->scoreLabelView->setWidthPercentage(25.0f);
	topScoreView->addChildView(this->scoreLabelView);
	
	this->scoreLabel = std::make_shared<Label>(this->menuQuads.get());
	this->scoreLabel->setName("scoreLabel");
	this->scoreLabel->setHeightSizeType(View::SizeType::FIT_PARENT);
	this->scoreLabel->setWidthSizeType(View::SizeType::FIT_PARENT);
	this->scoreLabel->setTextColor(SDL_Color { 200, 200, 200, 255 });
	this->scoreLabel->setText("0");
	this->scoreLabel->setHeightPercentage(labelSizePercentage);
	this->scoreLabel->setWidthPercentage(labelSizePercentage);
	this->scoreLabelView->addChildView(this->scoreLabel);
	
	auto gameServiceList = std::make_shared<ListView>(ListView::Orientation::HORIZONTAL);
	gameServiceList->setName("gameServiceList");
	gameServiceList->setWidthSizeType(View::SizeType::STRETCH_PARENT);
	gameServiceList->setHeightSizeType(View::SizeType::STRETCH_PARENT);
	gameServiceList->setWidthPercentage(80.0f);
	gameServiceList->setHeightPercentage(11.0f);
	gameServiceList->setItemPlacement(ListView::ItemPlacement::SPACED);
	this->buttonListView->addChildView(gameServiceList);
	
#ifdef __IPHONEOS__
	this->gameCenterBtn = std::make_shared<Button>();
	this->gameCenterBtn->setRenderQuad(this->menuQuads->createQuad());
	this->gameCenterBtn->showBackgroundColor(true);
	this->gameCenterBtn->setDefaultColor(SDL_Color{255, 255, 255, 0});
	this->gameCenterBtn->setPressedColor(SDL_Color{255, 255, 255, 120});
	this->gameCenterBtn->setWidthSizeType(View::SizeType::FIT_PARENT);
	this->gameCenterBtn->setHeightSizeType(View::SizeType::FIT_PARENT);
	this->gameCenterBtn->addOnClickListener(this);
	gameServiceList->addChildView(this->gameCenterBtn);
	
	
	auto gameCenterImageView = std::make_shared<ImageView>();
	gameCenterImageView->setRenderQuad(this->menuQuads->createQuad());
	gameCenterImageView->setImage(Assets::getInstance()->spriteManager->getSprite("game-center-hero.png"));
	gameCenterImageView->setDefaultColor(SDL_Color{255,255,255,255});
	gameCenterImageView->setWidthSizeType(View::SizeType::FIT_PARENT);
	gameCenterImageView->setHeightSizeType(View::SizeType::FIT_PARENT);
	gameCenterImageView->setWidthPercentage(85.0f);
	gameCenterImageView->setHeightPercentage(85.0f);
	gameCenterImageView->setName("game_center_button");
	this->gameCenterBtn->addChildView(gameCenterImageView);
	
#endif
	
#if defined(__IPHONEOS__) || defined(__ANDROID__)
	this->playGamesBtn = std::make_shared<Button>();
	this->playGamesBtn->setRenderQuad(this->menuQuads->createQuad());
	this->playGamesBtn->showBackgroundColor(true);
	this->playGamesBtn->setDefaultColor(SDL_Color{ 255, 255, 255, 70});
	this->playGamesBtn->setPressedColor(SDL_Color{ 255, 255, 255, 120});
	this->playGamesBtn->setWidthSizeType(View::SizeType::FIT_PARENT);
	this->playGamesBtn->setHeightSizeType(View::SizeType::FIT_PARENT);
	this->playGamesBtn->addOnClickListener(this);
	gameServiceList->addChildView(this->playGamesBtn);
	
	auto playImageView = std::make_shared<ImageView>();
	playImageView->setRenderQuad(this->menuQuads->createQuad());
	playImageView->setImage(Assets::getInstance()->spriteManager->getSprite("play_games_green.png"));
	playImageView->setDefaultColor(SDL_Color{ 255,255,255,255});
	playImageView->setWidthSizeType(View::SizeType::FIT_PARENT);
	playImageView->setHeightSizeType(View::SizeType::FIT_PARENT);
	playImageView->setWidthPercentage(85.0f);
	playImageView->setHeightPercentage(85.0f);
	playImageView->setName("play_button");
	this->playGamesBtn->addChildView(playImageView);
	
	this->playGamesMenu = std::make_shared<PlayGamesServiceMenu>();
	this->playGamesMenu->setRenderQuad(this->menuQuads->createQuad());
	this->playGamesMenu->init(this->menuQuads.get());
	this->menuView.addChildView(this->playGamesMenu);
#endif
	
}

void MainMenuScene::onActivate()
{
	MusicManager::getInstance()->playRandomTrack("menu");
	
#ifdef __ANDROID__
	AndroidApplication::getInstance()->showStatusAndNavBar(true);
#endif
	
	std::unique_ptr<IFileSaver> fileSaver;
#ifdef __APPLE__
	fileSaver = std::unique_ptr<IFileSaver>(new IosFileSaver());
#else
	fileSaver = std::unique_ptr<IFileSaver>(new GeneralFileSaver());
#endif
	
	std::string topScoreString = fileSaver->readFileAsString("savefile.txt");
	if (topScoreString != "") {
		std::string::size_type newlinePos = topScoreString.find('\n');
		if (newlinePos != std::string::npos) {
			topScoreString.erase(newlinePos);
		}
		this->scoreLabel->setText(topScoreString);
	}
}

void MainMenuScene::onDestroy()
{
	this->startButton->removeOnClickListener(this);
	this->menuView.removeOnClickListener(this);
	
#if defined(__IPHONEOS__) || defined(__ANDROID__)
	this->playGamesBtn->removeOnClickListener(this);
#endif
#ifdef __IPHONEOS__
	this->gameCenterBtn->removeOnClickListener(this);
#endif
}

void MainMenuScene::onRender()
{
	this->renderer->render();
}

void MainMenuScene::onUpdate(float deltaTime)
{
	// This is so ugly.
	if (!this->startButton->isClicked()) {
		if (this->startButtonRedIncreasing == true) {
			if ((this->startButtonColor.r + 4) >= 255) {
				this->startButtonRedIncreasing = false;
			} else {
				this->startButtonColor.r += 4;
			}
		} else {
			if ((this->startButtonColor.r - 4) <= 0) {
				this->startButtonRedIncreasing = true;
			} else {
				this->startButtonColor.r -= 4;
			}
		}
		
		if (this->startButtonGreenIncreasing == true) {
			if ((this->startButtonColor.g + 3) >= 255) {
				this->startButtonGreenIncreasing = false;
			} else {
				this->startButtonColor.g += 3;
			}
		} else {
			if ((this->startButtonColor.g - 3) <= 0) {
				this->startButtonGreenIncreasing = true;
			} else {
				this->startButtonColor.g -= 3;
			}
		}
		
		this->startButton->setCurrentColor(this->startButtonColor);
	}
	
	SDL_Color invertedColor;
	invertedColor.r = ((int)this->startButtonColor.r * -1) + 255;
	invertedColor.g = ((int)this->startButtonColor.g * -1) + 255;
	invertedColor.b = 0;
	invertedColor.a = 255;
	this->scoreLabelView->setCurrentColor(invertedColor);
	this->scoreTitleView->setCurrentColor(invertedColor);
	
#if defined(__IPHONEOS__) || defined(__ANDROID__)
	this->playGamesMenu->onUpdate(deltaTime);
#endif
}

void MainMenuScene::onEvent(const SDL_Event& event)
{
	if (event.type == SDL_MOUSEBUTTONDOWN || event.type == SDL_MOUSEBUTTONUP) {
		this->menuView.onMouseButtonEvent(event.button);
	} else if (event.type == SDL_MOUSEMOTION) {
		this->menuView.onMouseMoveEvent(event.motion);
	}
	
	if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED) {
		this->windowSize.x = event.window.data1;
		this->windowSize.y = event.window.data2;
		LOG("Main menu resize: %ix%i\n", event.window.data1, event.window.data2);
		
		this->menuView.setWidthPixels(this->windowSize.x);
		this->menuView.setHeightPixels(this->windowSize.y);
	}
	
	// SDL doesn't handle quitting properly yet, so we ignore this for now.
	//	if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_AC_BACK) {
	//		SDL_Event quitEvent;
	//		quitEvent.type = SDL_QUIT;
	//		SDL_PushEvent(&quitEvent);
	//	}
}

void MainMenuScene::setWindowSize(SDL_Point size)
{
	this->windowSize = size;
}

void MainMenuScene::onClick(const View& clickedElement)
{
	if (clickedElement == *this->startButton.get()) {
		this->manager->changeScene("game");
	}
#if defined(__IPHONEOS__) || defined(__ANDROID__)
	else if (clickedElement == *this->playGamesBtn.get()) {
		this->playGamesMenu->show();
	}
#endif
#ifdef __IPHONEOS__
	else if (clickedElement == *this->gameCenterBtn.get()) {
		GameCenterAndGooglePlayHandler::showGameCenterLeaderBoard();
	}
#endif
}
