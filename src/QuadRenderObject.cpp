#include "QuadRenderObject.h"

#include <cassert>

QuadRenderObject::QuadRenderObject(Quad* geometry, const glm::vec2& size):
	quadGeometry(geometry),
	size(size)
{
}

QuadRenderObject::QuadRenderObject():
	quadGeometry(nullptr)
{
}

void QuadRenderObject::onPositionUpdated(const glm::vec2& position)
{
	assert(quadGeometry != nullptr);
	this->quadGeometry->setVertices(
			glm::vec2(position.x - this->size.x / 2.0f, position.y - this->size.y / 2.0f),
			glm::vec2(position.x + this->size.x / 2.0f, position.y - this->size.y / 2.0f),
			glm::vec2(position.x + this->size.x / 2.0f, position.y + this->size.y / 2.0f),
			glm::vec2(position.x - this->size.x / 2.0f, position.y + this->size.y / 2.0f)
	);
	this->quadGeometry->signalChange();
}



void QuadRenderObject::onColorUpdated(const glm::vec4& color)
{
	this->quadGeometry->setColor(color);
	this->quadGeometry->signalChange();
}


void QuadRenderObject::updateWidth(float width)
{
	this->size.x = width;
}