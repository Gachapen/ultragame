#include "FontHandler.h"
#include <SDL2/SDL.h>
#include "log.h"
#include "Assets.h"
#include "GeneralFileSaver.h"

Sprite FontHandler::fontSprite = Sprite();
std::unordered_map<char, CharacterData> FontHandler::characterMap = std::unordered_map<char, CharacterData>();
glm::ivec2 FontHandler::atlasSize = glm::ivec2();
CharacterSetData FontHandler::characterSetData;

void FontHandler::readFontFile(std::string path)
{
	GeneralFileSaver fileLoader;
	std::vector<unsigned char> fileData = fileLoader.readFile(path);

	if (fileData.size() == 0) {
		LOGE("FontHandler: Error: Got 0 bytes from font file %s\n", path.c_str());
		return;
	}

	pugi::xml_document document;
	document.load_buffer(fileData.data(), fileData.size());

	pugi::xml_node fontNode = document.child("font");
	if (!fontNode) {
		LOGE("FontHandler: Error: Could not find XML font node.\n");
	}

	pugi::xml_node commonNode = fontNode.child("common");
	if (!commonNode) {
		LOGE("FontHandler: Error: Could not find XML common node.\n");
	}

	characterSetData.lineHeight = commonNode.attribute("lineHeight").as_int();
	characterSetData.base = commonNode.attribute("base").as_int();

	pugi::xml_node charsNode = fontNode.child("chars");
	if (!charsNode) {
		LOGE("FontHandler: Error: Could not find XML chars node.\n");
	}

	atlasSize = Assets::getInstance()->spriteManager->getAtlasSize("textureAtlas");
	fontSprite = Assets::getInstance()->spriteManager->getSprite("font_bitmap.png");

	float fontStartX = atlasSize.x * fontSprite.getRenderQuad().topLeft.textureCoordinate.x;
	float fontStartY = atlasSize.y * fontSprite.getRenderQuad().topLeft.textureCoordinate.y;

	for (pugi::xml_node character : charsNode.children()) {
		pugi::xml_attribute id = character.attribute("id");
		pugi::xml_attribute x = character.attribute("x");
		pugi::xml_attribute y = character.attribute("y");
		pugi::xml_attribute width = character.attribute("width");
		pugi::xml_attribute height = character.attribute("height");
		pugi::xml_attribute xoffset = character.attribute("xoffset");
		pugi::xml_attribute yoffset = character.attribute("yoffset");
		pugi::xml_attribute xadvance = character.attribute("xadvance");
		pugi::xml_attribute page = character.attribute("page");
		pugi::xml_attribute chnl = character.attribute("chnl");

		CharacterData data = { x.as_int(), y.as_int(), (fontStartX + x.as_int()) / atlasSize.x, (fontStartY + y.as_int()) / atlasSize.y, width.as_int(),
				height.as_int(), (float) width.as_int() / (float) atlasSize.x, (float) height.as_int() / (float) atlasSize.y, xoffset.as_int(),
				yoffset.as_int(), (float) xoffset.as_int() / (float) atlasSize.x, (float) yoffset.as_int() / (float) atlasSize.y,
				xadvance.as_int(), page.as_int(), chnl.as_int() };

		characterMap[id.as_int()] = data;
	}
    
}

const CharacterData& FontHandler::getCharacterData(const char character)
{
	return characterMap[character];
}

Sprite FontHandler::getSprite(const char character)
{
	CharacterData data = characterMap[character];
	Sprite::Quad quad;
	Sprite sprite;
	
	quad.bottomLeft.geometryVertex = glm::vec2(0.0,0.0);
	quad.bottomRight.geometryVertex = glm::vec2(data.width,0.0);
	quad.topLeft.geometryVertex = glm::vec2(0.0,data.height);
	quad.bottomLeft.textureCoordinate = glm::vec2(data.width,data.height);
	
	quad.bottomLeft.textureCoordinate = glm::vec2(data.xFraction, data.yFraction + data.heightFraction);
	quad.bottomRight.textureCoordinate = glm::vec2(data.xFraction + data.widthFraction, data.yFraction + data.heightFraction);
	quad.topRight.textureCoordinate = glm::vec2(data.xFraction + data.widthFraction, data.yFraction);
	quad.topLeft.textureCoordinate = glm::vec2(data.xFraction, data.yFraction);
	
	sprite.setRenderQuad(quad);
	
	return sprite;
}

const CharacterSetData& FontHandler::getCharacterSetData()
{
	return characterSetData;
}
