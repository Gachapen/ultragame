#include "Geometry.h"

#include <SDL2/SDL.h>

#include "GeometrySet.h"
#include "FontHandler.h" //TODO: remove when done testing

Geometry::Geometry(GeometrySet* container):
	container(container)
{
}

void Geometry::signalChange()
{
	this->container->onGeometryChanged(this);
}

void Geometry::removeFromContainer()
{
    this->container->removeGeometry(this);
}

GeometrySet* Geometry::getContainer()
{
	return this->container;
}

Triangle::Triangle(GeometrySet* container):
	Geometry(container)
{
}

void Triangle::setColor(const glm::vec4& color)
{
	this->a.color = color;
	this->b.color = color;
	this->c.color = color;
}

std::vector<ObjectCoordinate> Triangle::generateRenderableVertices() const
{
	std::vector<ObjectCoordinate> coords(this->getNumVertices());
	coords[0] = this->a;
	coords[1] = this->b;
	coords[2] = this->c;

	return coords;
}

unsigned int Triangle::getNumVertices() const
{
	return 3;
}

unsigned int Triangle::getNumRenderObjects() const
{
    return 1;
}

void Triangle::setVertexA(const glm::vec2& vertex)
{
	this->a.geometryVertex = vertex;
}

void Triangle::setVertexB(const glm::vec2& vertex)
{
	this->b.geometryVertex = vertex;
}

void Triangle::setVertexC(const glm::vec2& vertex)
{
	this->c.geometryVertex = vertex;
}

void Triangle::setVertices(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c)
{
	this->a.geometryVertex = a;
	this->b.geometryVertex = b;
	this->c.geometryVertex = c;
}

void Triangle::setColorA(const glm::vec4& color)
{
	this->a.color = color;
}

void Triangle::setColorB(const glm::vec4& color)
{
	this->b.color = color;
}

void Triangle::setColorC(const glm::vec4& color)
{
	this->c.color = color;
}

void Triangle::setColors(const glm::vec4& a, const glm::vec4& b, const glm::vec4& c)
{
	this->a.color = a;
	this->b.color = b;
	this->c.color = c;
}

Quad::Quad(GeometrySet* container, const Sprite& defaultSprite):
	Geometry(container)
{
    this->quadSprite = defaultSprite;
    this->setTextureCoordinates(defaultSprite);
}

void Quad::setColor(const glm::vec4& color)
{
	this->bottomLeft.color = color;
	this->bottomRight.color = color;
	this->topLeft.color = color;
	this->topRight.color = color;
}

std::vector<ObjectCoordinate> Quad::generateRenderableVertices() const
{
	std::vector<ObjectCoordinate> coords(this->getNumVertices());
	coords[0] = this->bottomLeft;
	coords[1] = this->bottomRight;
	coords[2] = this->topRight;
	coords[3] = this->bottomLeft;
	coords[4] = this->topRight;
	coords[5] = this->topLeft;

	return coords;
}

unsigned int Quad::getNumVertices() const
{
	// Two triangles.
	return 6;
}

unsigned int Quad::getNumRenderObjects() const
{
    return 2;
}

void Quad::setVertexBottomLeft(const glm::vec2& vertex)
{
	this->bottomLeft.geometryVertex = vertex;
}

void Quad::setVertexBottomRight(const glm::vec2& vertex)
{
	this->bottomRight.geometryVertex = vertex;
}

void Quad::setVertexTopRight(const glm::vec2& vertex)
{
	this->topRight.geometryVertex = vertex;
}

void Quad::setVertexTopLeft(const glm::vec2& vertex)
{
	this->topLeft.geometryVertex = vertex;
}

void Quad::setVertices(const glm::vec2& bottomLeft, const glm::vec2& bottomRight, const glm::vec2& topRight, const glm::vec2& topLeft)
{
    this->bottomLeft.geometryVertex = bottomLeft;
    this->bottomRight.geometryVertex = bottomRight;
    this->topRight.geometryVertex = topRight;
    this->topLeft.geometryVertex = topLeft;
}

void Quad::setColorBottomLeft(const glm::vec4& color)
{
	this->bottomLeft.color = color;
}

void Quad::setColorBottomRight(const glm::vec4& color)
{
	this->bottomRight.color = color;
}

void Quad::setColorTopRight(const glm::vec4& color)
{
	this->topRight.color = color;
}

void Quad::setColorTopLeft(const glm::vec4& color)
{
	this->topLeft.color = color;
}

void Quad::setColors(const glm::vec4& bottomLeft, const glm::vec4& bottomRight, const glm::vec4& topRight, const glm::vec4& topLeft)
{
    this->bottomLeft.color = bottomLeft;
    this->bottomRight.color = bottomRight;
    this->topRight.color = topRight;
    this->topLeft.color = topLeft;
}


void Quad::setTexture(const Sprite& texture)
{
	this->quadSprite = texture;

	this->setTextureCoordinates(texture);

	this->signalChange();
}

void Quad::setTextureCoordinates(const Sprite& texture)
{
	Sprite::Quad quad = this->quadSprite.getRenderQuad();

	// Mildly interesting... TODO: This is affecting the cross platform build...

#ifdef __IPHONEOS__
	this->bottomLeft.textureCoordinate = glm::vec2(quad.topLeft.textureCoordinate.x, 1 - quad.topLeft.textureCoordinate.y);
	this->bottomRight.textureCoordinate = glm::vec2(quad.topRight.textureCoordinate.x, 1 - quad.topRight.textureCoordinate.y);
	this->topLeft.textureCoordinate = glm::vec2(quad.bottomLeft.textureCoordinate.x, 1 - quad.bottomLeft.textureCoordinate.y);
	this->topRight.textureCoordinate = glm::vec2(quad.bottomRight.textureCoordinate.x, 1 - quad.bottomRight.textureCoordinate.y);
#else
	this->bottomLeft.textureCoordinate = glm::vec2(quad.bottomLeft.textureCoordinate.x, quad.topLeft.textureCoordinate.y);
	this->bottomRight.textureCoordinate = glm::vec2(quad.bottomRight.textureCoordinate.x, quad.topRight.textureCoordinate.y);
	this->topLeft.textureCoordinate = glm::vec2(quad.topLeft.textureCoordinate.x, quad.bottomLeft.textureCoordinate.y);
	this->topRight.textureCoordinate = glm::vec2(quad.topRight.textureCoordinate.x, quad.bottomRight.textureCoordinate.y);
#endif

	//    this->bottomLeft.textureCoordinate = glm::vec2(0.0f, 0.0f);
	//    this->bottomRight.textureCoordinate = glm::vec2(1.0f, 0.0f);
	//    this->topRight.textureCoordinate = glm::vec2(1.0f, 1.0f);
	//    this->topLeft.textureCoordinate = glm::vec2(0.0f, 1.0f);
}

Line::Line(GeometrySet* container):
	Geometry(container)
{
}

void Line::setColor(const glm::vec4& color)
{
	this->start.color = color;
	this->end.color = color;
}

std::vector<ObjectCoordinate> Line::generateRenderableVertices() const
{
	std::vector<ObjectCoordinate> coords(this->getNumVertices());
	coords[0] = this->start;
	coords[1] = this->end;

	return coords;
}

unsigned int Line::getNumVertices() const
{
	return 2;
}

unsigned int Line::getNumRenderObjects() const
{
    return 1;
}

void Line::setVertexStart(const glm::vec2& vertex)
{
	this->start.geometryVertex = vertex;
}

void Line::setVertexEnd(const glm::vec2& vertex)
{
	this->end.geometryVertex = vertex;
}

void Line::setVertices(const glm::vec2& start, const glm::vec2& end)
{
	this->start.geometryVertex = start;
	this->end.geometryVertex = end;
}

void Line::setColorStart(const glm::vec4& color)
{
	this->start.color = color;
}

void Line::setColorEnd(const glm::vec4& color)
{
	this->end.color = color;
}

void Line::setColors(const glm::vec4& start, const glm::vec4& end)
{
	this->start.color = start;
	this->end.color = end;
}

Polygon::Polygon(GeometrySet* container, unsigned int numVertices):
   Geometry(container),
   NUM_VERTICES(numVertices)
{
    this->coordinates.resize(this->NUM_VERTICES);
}

void Polygon::setColor(const glm::vec4& color)
{
    this->color = color;
    for (ObjectCoordinate& coordinate : this->coordinates)
    {
        coordinate.color = color;
    }
}

std::vector<ObjectCoordinate> Polygon::generateRenderableVertices() const
{
    std::vector<ObjectCoordinate> vertices(this->NUM_VERTICES * 3);

    glm::vec2 vertexSum;

    for (const ObjectCoordinate& coordinate : this->coordinates)
    {
        vertexSum.x += coordinate.geometryVertex.x;
        vertexSum.y += coordinate.geometryVertex.y;
    }

    glm::vec2 centroid;
    centroid.x = vertexSum.x / this->NUM_VERTICES;
    centroid.y = vertexSum.y / this->NUM_VERTICES;

    // Creates a fan of triangles.
    for (unsigned int i = 0; i < this->NUM_VERTICES; i++)
    {
        unsigned int vertexStartPos = i * 3;
        unsigned int secondPos = i;

        unsigned int thirdPos;
        if (i < this->NUM_VERTICES - 1)
        {
             thirdPos = i + 1;
        }
        else
        {
            thirdPos = 0;
        }

        vertices[vertexStartPos].color = this->coordinates[i].color;
        vertices[vertexStartPos].geometryVertex = centroid;

        vertices[vertexStartPos + 1] = this->coordinates[secondPos];
        vertices[vertexStartPos + 2] = this->coordinates[thirdPos];
    }

    return vertices;
}

unsigned int Polygon::getNumVertices() const
{
	// One triangle per vertex becomes three vertices per vertex.
    return this->NUM_VERTICES * 3;
}

void Polygon::setVertex(unsigned int vertexNum, const glm::vec2& vertex)
{
	assert(vertexNum < this->coordinates.size());
    this->coordinates[vertexNum].geometryVertex = vertex;
}

void Polygon::setColor(unsigned int vertexNum, const glm::vec4& color)
{
	assert(vertexNum < this->coordinates.size());
	this->coordinates[vertexNum].color = color;
}

unsigned int Polygon::getNumRenderObjects() const
{
    return this->NUM_VERTICES;
}
