#ifndef __ultragame__MenuRenderer__
#define __ultragame__MenuRenderer__

#include "Renderer.h"

class MenuRenderer : public Renderer{
public:
	void render() override;
};

#endif /* defined(__ultragame__MenuRenderer__) */
