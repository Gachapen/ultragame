#include "View.h"

#include "IOnClickListener.h"
#include "../log.h"

View::View():
	id(-1),
	clickable(false),
	currentMouseState(0),
	childIdIncrementor(0),
	widthSizeType(SizeType::STRETCH_PARENT),
	heightSizeType(SizeType::STRETCH_PARENT),
	widthPercentage(100.0f),
	heightPercentage(100.0f),
	defaultColor{0, 0, 0, 255},
	pressedColor{0, 0, 0, 255},
	currentColor{0, 0, 0, 255},
	size{10, 10},
	originalSize(size),
	positionPixel{0,0},
	horizontalPositionType(Position::CENTERED),
	verticalPositionType(Position::CENTERED),
	parentView(nullptr),
	renderedQuad(nullptr),
	disabled(false)
{
}

View::~View()
{
}

bool View::isHitBy(const SDL_Point& point) const
{
	SDL_Rect rect = this->getRect();
	if (point.x < rect.x) {
		return false;
	}

	if (point.x > rect.x + rect.w) {
		return false;
	}

	if (point.y < rect.y) {
		return false;
	}

	if (point.y > rect.y + rect.h) {
		return false;
	}

	return true;
}

void View::addOnClickListener(IOnClickListener* listener)
{
	this->onClickListeners.push_front(listener);
}

void View::removeOnClickListener(IOnClickListener* listener)
{
	this->onClickListeners.remove(listener);
}

void View::setId(int id)
{
	this->id = id;
}

void View::setClickable(bool state)
{
	this->clickable = state;
}

bool View::isClickable() const
{
	return this->clickable;
}

int View::getId() const
{
	return this->id;
}

void View::setHorizontalPosition(int position)
{
	printf("%s: Set horizontal pos: %i\n", this->getName().c_str(), position);
	this->positionPixel.x = position;
	this->onPositionChanged();
}

void View::setVerticalPosition(int position)
{
	this->positionPixel.y = position;
	this->onPositionChanged();
}

void View::setWidthSizeType(SizeType sizeType)
{
	this->widthSizeType = sizeType;
}

void View::setHeightSizeType(SizeType sizeType)
{
	this->heightSizeType = sizeType;
	if (this->parentView) {
		this->parentView->onChildViewChanged(this);
	}
}

void View::setWidthPercentage(float percentage)
{
	this->widthPercentage = percentage;
	if (this->parentView) {
		this->parentView->onChildViewChanged(this);
	}
}

void View::setHeightPercentage(float percentage)
{
	this->heightPercentage = percentage;
	if (this->parentView) {
		this->parentView->onChildViewChanged(this);
	}
}

void View::setWidthPixels(int pixels)
{
	this->originalSize.x = pixels;
	this->size.x = pixels;

	this->updatePositioning();

	if (this->parentView) {
		this->parentView->onChildViewChanged(this);
	}
}

void View::setHeightPixels(int pixels)
{
	this->originalSize.y = pixels;
	this->size.y = pixels;

	this->updatePositioning();

	if (this->parentView) {
		this->parentView->onChildViewChanged(this);
	}
}

void View::setOriginalSize(const SDL_Point& size)
{
	this->originalSize = size;
	if (this->parentView) {
		this->parentView->onChildViewChanged(this);
	}
}

void View::setDefaultColor(SDL_Color color)
{
    this->defaultColor = color;
    if (!(this->currentMouseState & STATE_MOUSE_CLICK) && !(this->currentMouseState & STATE_MOUSE_HOVER)) {
    	this->currentColor = color;
    }

    this->updateQuadColor();
}

SDL_Color View::getColor() const
{
    return this->currentColor;
}

void View::onChildViewChanged(View* childView)
{
	if (childView->isDisabled() == false) {
		childView->updatePositioning();
	}
}

void View::notifyClickListeners()
{
	for (auto clickListener : this->onClickListeners) {
		clickListener->onClick(*this);
	}
}


void View::onMouseFocus()
{
	if (!(this->currentMouseState & STATE_MOUSE_HOVER)) {
		this->currentMouseState |= STATE_MOUSE_HOVER;
	}

	if (this->currentMouseState & STATE_MOUSE_CLICK) {
		this->onEnterPressedState();
	}
}

void View::onMouseBlur()
{
	if (this->currentMouseState & STATE_MOUSE_HOVER) {
		this->currentMouseState &= ~STATE_MOUSE_HOVER;
	}

	if (this->currentMouseState & STATE_MOUSE_CLICK) {
		this->onLeavePressedState();
	}
}

void View::onMouseDown()
{
	if (!(this->currentMouseState & STATE_MOUSE_CLICK)) {
		this->currentMouseState |= STATE_MOUSE_CLICK;
		this->onEnterPressedState();
	}
}

void View::onMouseUp()
{
	if (this->currentMouseState & STATE_MOUSE_CLICK) {
		this->currentMouseState &= ~STATE_MOUSE_CLICK;
		this->onLeavePressedState();
		this->notifyClickListeners();
	}
}

bool View::isFocused() const
{
	if (this->currentMouseState & STATE_MOUSE_HOVER) {
		return true;
	}

	return false;
}

bool View::isClicked() const
{
	if (this->isFocused() && (this->currentMouseState & STATE_MOUSE_CLICK)) {
		return true;
	}

	return false;
}

void View::insertChildView(const std::shared_ptr<View>& view, unsigned int position)
{
	view->parentView = this;
	view->setId(this->childIdIncrementor++);
	view->onAttachToParent(this);
	view->updatePositioning();
	this->childViews.insert(this->childViews.begin() + position, view);
	this->onChildAdded(view.get());
}

void View::addChildView(const std::shared_ptr<View>& view)
{
	this->insertChildView(view, (unsigned)this->childViews.size());
	printf("%s: Adding child: %s\n", this->getName().c_str(), view->getName().c_str());
}

void View::removeChildView(const std::shared_ptr<View>& view)
{
	auto viewIt = this->childViews.begin();
    
	while (viewIt != this->childViews.end()) {
        
		std::shared_ptr<View> currentView = *viewIt;
        
		if (currentView == view) {
            
			viewIt = this->childViews.erase(viewIt);
			this->onChildRemoved(currentView.get());
		} else {
            
			viewIt++;
		}
	}
}

// TODO: Hover mode. Not really needed for touch screens.
void View::onMouseMoveEvent(const SDL_MouseMotionEvent& event)
{
	if (this->isDisabled() == false) {
		SDL_Point mouseClickPoint{event.x, event.y};

		for (auto viewIt = this->childViews.rbegin(); viewIt != this->childViews.rend(); viewIt++) {
			(*viewIt)->onMouseMoveEvent(event);
		}

		if (this->isHitBy(mouseClickPoint) && this->isFocused() == false) {
			this->onMouseFocus();
		} else if (this->isHitBy(mouseClickPoint) == false && this->isFocused()) {
			this->onMouseBlur();
		}
	}
}

bool View::onMouseButtonEvent(const SDL_MouseButtonEvent& event)
{
	if (this->isDisabled() == false) {
		SDL_Point mouseClickPoint { event.x, event.y };

		for (auto viewIt = this->childViews.rbegin(); viewIt != this->childViews.rend(); viewIt++) {
			if ((*viewIt)->onMouseButtonEvent(event) == true) {
				return true;
			}
		}

		if (this->isClickable()) {
			if (this->isHitBy(mouseClickPoint)) {
				if (event.state == SDL_PRESSED) {
					this->onMouseDown();
				} else if (event.state == SDL_RELEASED) {
					this->onMouseUp();
				}
				return true;
			} else {
				if (this->currentMouseState & STATE_MOUSE_CLICK) {
					this->currentMouseState &= ~STATE_MOUSE_CLICK;
				}
			}
		}
	}

	return false;
}

SDL_Rect View::getRect() const
{
	SDL_Rect rect = {this->positionPixel.x, this->positionPixel.y, this->size.x, this->size.y};
	return rect;
}

std::vector<std::shared_ptr<View>>& View::getChildViews()
{
    return this->childViews;
}

void View::removeChildViews()
{
    for (auto child : this->childViews) {
        child->removeChildViews();
    }
    
    this->childViews.clear();
}

SDL_Point View::getPositionPixel() const
{
    return this->positionPixel;
}

void View::updatePositioning()
{
	if (this->parentView == nullptr) {
		LOGE("Trying to update view positioning with no parent view.\n");
		return;
	}

	printf("%s: Updating position:", this->name.c_str());

	SDL_Rect parentRect = this->parentView->getRect();

	switch (this->widthSizeType) {
	case SizeType::STRETCH_PARENT: {
		this->size.x = parentRect.w * (this->widthPercentage / 100.0f);
		printf(" size.x = STRETCH_PARENT (result %i).", this->size.x);
		break;
	}

	case SizeType::FIT_PARENT: {
		float scaleValue = 1.0f;
		if (((float) parentRect.w / (float) this->originalSize.x) < ((float) parentRect.h / (float) this->originalSize.y)) {
			scaleValue = ((float) parentRect.w * (this->widthPercentage / 100.0f)) / (float) this->originalSize.x;
		} else {
			scaleValue = ((float) parentRect.h * (this->heightPercentage / 100.0f)) / (float) this->originalSize.y;
		}

		this->size.x = (float) this->originalSize.x * scaleValue;
		printf(" size.x = FIT_PARENT (result %i).", this->size.x);
		break;
	}

	default:
		break;
	}

	switch (this->heightSizeType) {
	case SizeType::STRETCH_PARENT: {
		this->size.y = parentRect.h * (this->heightPercentage / 100.0f);
		printf(" size.y = STRETCH_PARENT (result %i).", this->size.y);
		break;
	}

	case SizeType::FIT_PARENT: {
		float scaleValue = 1.0f;
		if (((float) parentRect.w / (float) this->originalSize.x) < ((float) parentRect.h / (float) this->originalSize.y)) {
			scaleValue = ((float) parentRect.w * (this->widthPercentage / 100.0f)) / (float) this->originalSize.x;
		} else {
			scaleValue = ((float) parentRect.h * (this->heightPercentage / 100.0f)) / (float) this->originalSize.y;
		}

		this->size.y = (float) this->originalSize.y * scaleValue;
		printf(" size.y = FIT_PARENT (result %i).", this->size.y);
		break;
	}

	default:
		break;
	}

	if (this->horizontalPositionType == Position::CENTERED) {
		this->positionPixel.x = parentRect.x + (parentRect.w / 2) - (this->size.x / 2);
		printf(" pos.x = CENTERED (result %i).", this->positionPixel.x);
	}

	if (this->verticalPositionType == Position::CENTERED) {
		this->positionPixel.y = parentRect.y + (parentRect.h / 2) - (this->size.y / 2);
		printf(" pos.y = CENTERED (result %i).", this->positionPixel.y);
	}

	printf("\n");

	this->onPositionChanged();
	this->onSizeChanged();

	for (auto childView : this->childViews) {
		childView->updatePositioning();
	}
}

View* View::getParent() const
{
    return this->parentView;
}

void View::setPressedColor(SDL_Color color)
{
	this->pressedColor = color;
}

void View::setCurrentColor(SDL_Color color)
{
	this->currentColor = color;
	this->updateQuadColor();
}

void View::onEnterPressedState()
{
	this->currentColor = this->pressedColor;
	this->updateQuadColor();
}

void View::onLeavePressedState()
{
	this->currentColor = this->defaultColor;
	this->updateQuadColor();
}

bool View::operator == (const View& other) const
{
	if (this->id == other.id && this->parentView == other.parentView) {
		return true;
	}

	return false;
}

void View::setPositioningTypeHorizontal(Position position)
{
	this->horizontalPositionType = position;
}

void View::setPositioningTypeVertical(Position position)
{
	this->verticalPositionType = position;
}

void View::onAttachToParent(View* parent)
{
}

void View::onChildAdded(View* childView)
{
}

void View::onChildRemoved(View* childView)
{
//    childView->removeQuad();
    
}

void View::setRenderQuad(Quad *quad)
{
    this->renderedQuad = quad;
}

Quad* View::getRenderQuad()
{
    return this->renderedQuad;
}

void View::updateQuadGeometry()
{
    if (this->renderedQuad != nullptr)
    {
        this->renderedQuad->setVertices(glm::vec2{this->positionPixel.x, this->positionPixel.y},
                                        glm::vec2{this->positionPixel.x + this->size.x, this->positionPixel.y},
                                        glm::vec2{this->positionPixel.x + this->size.x, this->positionPixel.y + this->size.y},
                                        glm::vec2{this->positionPixel.x, this->positionPixel.y + this->size.y});
    
        this->renderedQuad->signalChange();
    }
}

void View::updateQuadColor()
{
	if (this->renderedQuad != nullptr) {
		if (this->isDisabled() == true) {
			this->renderedQuad->setColor(glm::vec4(0.0f, 0.0f, 0.0f, 0.0f));
		} else {
			this->renderedQuad->setColor(glm::vec4(this->currentColor.r/255.0f, this->currentColor.g/255.0f, this->currentColor.b/255.0f,this->currentColor.a/255.0f));
		}


		this->renderedQuad->signalChange();
	}
}

void View::setName(const std::string& name)
{
	this->name = name;
}

const std::string& View::getName()
{
	return this->name;
}

void View::onPositionChanged()
{
	this->updateQuadGeometry();

	for (auto childView : this->childViews)
	{
		childView->updatePositioning();
	}
}

void View::onSizeChanged()
{
}

void View::updateViewHierarchyRendering()
{
	this->updateRendering();

	for (std::shared_ptr<View>& child : this->childViews) {
		child->updateViewHierarchyRendering();
	}
}

void View::disable(bool disable)
{
	if (this->disabled != disable) {
		this->disabled = disable;

		this->updateQuadColor();

		for (auto childView : this->childViews) {
			childView->disable(disable);
		}

		this->parentView->onChildViewChanged(this);
	}
}

bool View::isDisabled() const
{
	return this->disabled;
}

void View::updateRendering()
{
	if (this->renderedQuad != nullptr) {
		GeometrySet* set = this->renderedQuad->getContainer();
		set->removeGeometry(this->renderedQuad);
		this->renderedQuad = set->createQuad();
		this->updateQuadGeometry();
		this->updateQuadColor();
	}
}

View* View::getTopOfHierarchy()
{
	if (this->parentView != nullptr) {
		return this->parentView->getTopOfHierarchy();
	} else {
		return this;
	}
}
