#ifndef LABEL_H_
#define LABEL_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <string>

#include "View.h"
#include "../TextHandler.h"

class Label: public View
{
public:
	Label(GeometrySet* set);
	virtual ~Label();

	void setText(const std::string& text);
	void setTextColor(const SDL_Color& color);
	void updateRendering() override;

private:
	void onPositionChanged() override;
	void onSizeChanged() override;
	
	std::string text;
	SDL_Color textColor;
	
	TextHandler textHandler;
};

#endif /* LABEL_H_ */
