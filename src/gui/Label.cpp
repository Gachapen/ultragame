#include "Label.h"

Label::Label(GeometrySet* set):\
	textColor{ 255, 255, 255, 255 },
	textHandler(set)
{
	this->setDefaultColor(SDL_Color{0,0,0,0});
	this->setTextColor(textColor);
}

Label::~Label()
{
}

void Label::setText(const std::string& text)
{
	this->textHandler.setText(text);
	this->setOriginalSize(SDL_Point{this->textHandler.getTextSize().x, this->textHandler.getTextSize().y});
	
	if (text.length() != this->text.length()) {
		this->getTopOfHierarchy()->updateViewHierarchyRendering();
	}

	this->text = text;
}

void Label::setTextColor(const SDL_Color& color)
{
	this->textColor = color;
	this->textHandler.setColor(glm::vec4(color.r/255.0,color.g/255.0,color.b/255.0,color.a/255.0));
}

void Label::onPositionChanged()
{
	View::onPositionChanged();
	
	SDL_Rect renderRect = this->getRect();
	this->textHandler.setPosition(glm::vec2(renderRect.x,renderRect.y));
}

void Label::updateRendering()
{
	View::updateRendering();

	this->textHandler.reCreateRenderData();
}

void Label::onSizeChanged()
{
	View::onSizeChanged();

	SDL_Rect renderRect = this->getRect();
	this->textHandler.setSize(glm::vec2(renderRect.w,renderRect.h));
}
