#ifndef VIEW_H_
#define VIEW_H_

#include <SDL2/SDL.h>
#include <forward_list>
#include <vector>
#include <memory>
#include <string>
#include <map>
#include "../GeometrySet.h"

class IOnClickListener;

class View {
public:
	enum class SizeType {
		STRETCH_PARENT,
		FIT_PARENT,
		PIXELS
	};

	enum class Position {
		CENTERED,
		PIXELS
	};

	enum class Side {
		LEFT,
		TOP,
		BOTTOM,
		RIGHT
	};

	View();
	virtual ~View();

	bool operator == (const View& other) const;

	void addOnClickListener(IOnClickListener* listener);
	void removeOnClickListener(IOnClickListener* listener);

	void addChildView(const std::shared_ptr<View>& view);
	void insertChildView(const std::shared_ptr<View>& view, unsigned int position);
	void removeChildView(const std::shared_ptr<View>& view);

	void onMouseMoveEvent(const SDL_MouseMotionEvent& event);
	bool onMouseButtonEvent(const SDL_MouseButtonEvent& event);;

	void setId(int id);
	void setClickable(bool state);
	void setName(const std::string& name);

	void setPositioningTypeHorizontal(Position position);
	void setPositioningTypeVertical(Position position);

	void setHorizontalPosition(int position);
	void setVerticalPosition(int position);

	void setWidthSizeType(SizeType sizeType);
	void setHeightSizeType(SizeType sizeType);

	void setWidthPercentage(float percentage);
	void setHeightPercentage(float percentage);

	void setWidthPixels(int pixels);
	void setHeightPixels(int pixels);

	void setOriginalSize(const SDL_Point& size);

	void setDefaultColor(SDL_Color color);
	void setPressedColor(SDL_Color color);
	void setCurrentColor(SDL_Color color);

	bool isClickable() const;
	bool isHitBy(const SDL_Point& point) const;
	int getId() const;
	bool isFocused() const;
	bool isClicked() const;
	SDL_Rect getRect() const;
	SDL_Color getColor() const;
	SDL_Point getPositionPixel() const;
	bool isDisabled() const;

	void removeChildViews();

	virtual void onMouseFocus();
	virtual void onMouseBlur();
	virtual void onMouseDown();
	virtual void onMouseUp();

	virtual void onAttachToParent(View* parent);

	virtual void updatePositioning();

	View* getParent() const;
	View* getTopOfHierarchy();

	virtual void setRenderQuad(Quad * quad);
	virtual Quad* getRenderQuad();

	const std::string& getName();

	void disable(bool disable);

	virtual void updateViewHierarchyRendering();

protected:
	std::vector<std::shared_ptr<View>>& getChildViews();
	void notifyClickListeners();

	virtual void onChildViewChanged(View* childView);
	virtual void onEnterPressedState();
	virtual void onLeavePressedState();
	virtual void onChildAdded(View* childView);
	virtual void onChildRemoved(View* childView);
	virtual void onPositionChanged();
	virtual void onSizeChanged();
	virtual void updateRendering();
    
private:
	virtual void updateQuadGeometry();
	virtual void updateQuadColor();

	const unsigned int STATE_MOUSE_HOVER = 0x00000001;
	const unsigned int STATE_MOUSE_CLICK = 0x00000002;

	std::string name;
	int id;
	bool clickable;
	unsigned int currentMouseState;
	int childIdIncrementor;

	SizeType widthSizeType;
	SizeType heightSizeType;

	float widthPercentage;
	float heightPercentage;

	SDL_Color defaultColor;
	SDL_Color pressedColor;
	SDL_Color currentColor;

	SDL_Point size;
	SDL_Point originalSize;

	SDL_Point positionPixel;
	Position horizontalPositionType;
	Position verticalPositionType;

	std::forward_list<IOnClickListener*> onClickListeners;
	std::vector<std::shared_ptr<View> > childViews;
	View* parentView;

	Quad* renderedQuad;
	bool disabled;
};

#endif /* VIEW_H_ */
