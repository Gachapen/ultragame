#ifndef IMAGEVIEW_H_
#define IMAGEVIEW_H_

#include <SDL2/SDL_image.h>
#include "View.h"

class ImageView: public View
{
public:
	ImageView();
	virtual ~ImageView();

	void setImage(Sprite image);
	void updateRendering() override;
    
private:
    Sprite defaultImage;
    Sprite pressedImage;
};

#endif /* IMAGEVIEW_H_ */
