#include "ListView.h"

ListView::ListView(Orientation listOrientation):
	itemSpacingPixels(0),
	listOrientation(listOrientation),
	itemPlacement(ItemPlacement::TIGHT)
{
}

void ListView::setPixelItemSpacing(int pixels)
{
    this->itemSpacingPixels = pixels;
    this->placeChildren();
}

void ListView::placeChildren()
{
	printf("%s: Placing children ", this->getName().c_str());

	if (this->listOrientation == Orientation::VERTICAL) {
		printf("vertically.\n");
		this->placeChildrenVertical();
	} else {
		printf("horizontally.\n");
		this->placeChildrenHorizontal();
	}
}

void ListView::updatePositioning()
{
	View::updatePositioning();
	this->placeChildren();
}

void ListView::placeChildrenVertical()
{
	std::vector<std::shared_ptr<View>>& children = this->getChildViews();
	int verticalPositionInList = this->getPositionPixel().y;

	unsigned int numChildren = 0;
	for (auto child : children) {
		if (child->isDisabled() == false) {
			numChildren++;
		}
	}

	if (numChildren == 0) {
		return;
	}

	if (this->itemPlacement == ItemPlacement::TIGHT) {
		for (auto child : children) {
			if (child->isDisabled() == false) {
				child->setVerticalPosition(verticalPositionInList);
				verticalPositionInList += child->getRect().h + this->itemSpacingPixels;
			}
		}

	} else if (this->itemPlacement == ItemPlacement::SPREAD && numChildren > 1) {
		int emptySpace = this->getRect().h;

		for (auto child : children) {
			if (child->isDisabled() == false) {
				emptySpace -= child->getRect().h;
			}
		}

		int spaceBetweenItems = emptySpace / (numChildren - 1);

		for (auto child : children) {
			if (child->isDisabled() == false) {
				child->setVerticalPosition(verticalPositionInList);
				verticalPositionInList = verticalPositionInList + child->getRect().h + spaceBetweenItems;
			}
		}

	} else if (this->itemPlacement == ItemPlacement::SPACED) {
		int emptySpace = this->getRect().h;

		for (auto child : children) {
			if (child->isDisabled() == false) {
				emptySpace -= child->getRect().h;
			}
		}

		int spaceBetweenItems = emptySpace / (numChildren + 1);
		verticalPositionInList += spaceBetweenItems;

		for (auto child : children) {
			if (child->isDisabled() == false) {
				child->setVerticalPosition(verticalPositionInList);
				verticalPositionInList = verticalPositionInList + child->getRect().h + spaceBetweenItems;
			}
		}
	}
}

void ListView::placeChildrenHorizontal()
{
	std::vector<std::shared_ptr<View>>& children = this->getChildViews();
	int horizontalPositionInList = this->getPositionPixel().x;

	unsigned int numChildren = 0;
	for (auto child : children) {
		if (child->isDisabled() == false) {
			numChildren++;
		}
	}

	if (numChildren == 0) {
		return;
	}

	if (this->itemPlacement == ItemPlacement::TIGHT) {
		for (auto child : children) {
			if (child->isDisabled() == false) {
				child->setHorizontalPosition(horizontalPositionInList);
				horizontalPositionInList += child->getRect().w + this->itemSpacingPixels;
			}
		}

	} else if (this->itemPlacement == ItemPlacement::SPREAD && numChildren > 1) {
		int emptySpace = this->getRect().w;

		for (auto child : children) {
			if (child->isDisabled() == false) {
				emptySpace -= child->getRect().w;
			}
		}

		int spaceBetweenItems = emptySpace / (numChildren - 1);

		for (auto child : children) {
			if (child->isDisabled() == false) {
				child->setHorizontalPosition(horizontalPositionInList);
				horizontalPositionInList = horizontalPositionInList + child->getRect().w + spaceBetweenItems;
			}
		}

	} else if (this->itemPlacement == ItemPlacement::SPACED) {
		int emptySpace = this->getRect().w;

		for (auto child : children) {
			if (child->isDisabled() == false) {
				emptySpace -= child->getRect().w;
			}
		}

		int spaceBetweenItems = emptySpace / (numChildren + 1);
		horizontalPositionInList += spaceBetweenItems;

		for (auto child : children) {
			if (child->isDisabled() == false) {
				child->setHorizontalPosition(horizontalPositionInList);
				horizontalPositionInList = horizontalPositionInList + child->getRect().w + spaceBetweenItems;
			}
		}
	}
}

void ListView::setItemPlacement(ItemPlacement placement)
{
	this->itemPlacement = placement;

	this->placeChildren();
}

void ListView::onChildViewChanged(View* childView)
{
	View::onChildViewChanged(childView);
	this->placeChildren();
}

void ListView::onChildAdded(View* childView)
{
	this->placeChildren();
}

void ListView::onChildRemoved(View* childView)
{
    View::onChildRemoved(childView);
	this->placeChildren();
}

void ListView::onPositionChanged()
{
	View::onPositionChanged();
	this->placeChildren();
}
