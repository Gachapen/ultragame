#ifndef BUTTON_H_
#define BUTTON_H_

#include "View.h"

class Button: public View {
public:
	Button();
	Button(SDL_Color color);
	virtual ~Button();

	virtual void onEnterPressedState() override;
	virtual void onLeavePressedState() override;

	void showBackgroundColor(bool state);

private:
    
	bool enableBackgroundColor;
};

#endif /* BUTTON_H_ */
