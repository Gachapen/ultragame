#include "ImageView.h"

#include <cassert>

ImageView::ImageView()
{
}

ImageView::~ImageView()
{
}

void ImageView::setImage(Sprite texture)
{
    this->defaultImage = texture;

    SDL_Point originalSize;
    originalSize.x = texture.getWidth();
    originalSize.y = texture.getHeight();
    this->setOriginalSize(originalSize);
    
    this->getRenderQuad()->setTexture(this->defaultImage);
    this->getRenderQuad()->signalChange();
}

void ImageView::updateRendering()
{
	View::updateRendering();

	this->getRenderQuad()->setTexture(this->defaultImage);
	this->getRenderQuad()->signalChange();
}
