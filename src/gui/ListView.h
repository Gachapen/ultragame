#ifndef __ultragame__ListView__
#define __ultragame__ListView__

#include "View.h"

class ListView : public View
{
public:
	enum class Orientation {
		VERTICAL,
		HORIZONTAL
	};

	enum class ItemPlacement {
		TIGHT,
		SPREAD,
		SPACED
	};

    ListView(Orientation listOrientation);
    
    virtual void updatePositioning() override;

    void setPixelItemSpacing(int pixels);
    void setItemPlacement(ItemPlacement placement);
    
protected:
    virtual void onChildViewChanged(View* childView) override;
    virtual void onChildAdded(View* childView) override;
    virtual void onChildRemoved(View* childView) override;
    virtual void onPositionChanged() override;

private:
    void placeChildren();
    void placeChildrenVertical();
    void placeChildrenHorizontal();

    int itemSpacingPixels;
    Orientation listOrientation;
    ItemPlacement itemPlacement;
};

#endif /* defined(__ultragame__ListView__) */
