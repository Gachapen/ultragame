#include "Button.h"
#include "../Assets.h"

#include "View.h"

Button::Button(SDL_Color color):
	enableBackgroundColor(false)
{
    
    this->setDefaultColor(color);
	this->setClickable(true);
}

Button::Button():
	enableBackgroundColor(false)
{
	this->setClickable(true);
}

Button::~Button()
{
}

void Button::onEnterPressedState()
{
	View::onEnterPressedState();
}

void Button::onLeavePressedState()
{
	View::onLeavePressedState();
}


void Button::showBackgroundColor(bool state)
{
	this->enableBackgroundColor = state;
}
