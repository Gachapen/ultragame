#ifndef IONCLICKLISTENER_H_
#define IONCLICKLISTENER_H_

#include "View.h"

class IOnClickListener
{
public:
	virtual ~IOnClickListener()
	{}

	virtual void onClick(const View& clickedElement) = 0;
};

#endif /* IONCLICKLISTENER_H_ */
