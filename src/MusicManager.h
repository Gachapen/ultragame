#ifndef MUSICMANAGER_H_
#define MUSICMANAGER_H_

#include <string>
#include <vector>
#include <unordered_map>
#include <chrono>
#include "pugixml/pugixml.hpp"

#ifdef __APPLE__
#include <SDL2_mixer/SDL_mixer.h>
#else
#include <SDL2/SDL_mixer.h>
#endif

/**
 * Handles the music.
 */
class MusicManager
{
public:
	/**
	 * Loads a file from the path
	 * @param filePath the path of the file.
	 * @returns wether or not it was sucessful.
	 */
	bool loadFile(const std::string& filePath);
	
	/**
	 * Loads all the tracks in a set.
	 * @param setName The name of the set to be loaded.
	 * @returns wether or not it was sucessful.
	 */
	bool loadSet(const std::string& setName);
	
	/**
	 * TODO
	 */
	void unload();
	
	/**
	 * Plays the specified track
	 * @param trackName the name of the track.
	 * @returns wether or not it was sucessful.
	 */
	bool playTrack(const std::string& trackName);
	
	/**
	 * Plays a random track from a specified set
	 * @param set The set to play from.
	 * @returns wether or not it was sucessful.
	 */
	bool playRandomTrack(const std::string& set);
	
	/**
	 * Continue playing a song that was stopped.
	 */
	void restorePreviouslyStopped();
	
	/**
	 * Stops the music.
	 */
	void stopMusic();
	
	/**
	 * pauses the music.
	 */
	void pauseMusic();
	
	/**
	 * Resumes the music.
	 */
	void resumeMusic();
	
	/**
	 * Gets the names of the sets.
	 * @returns A vector containing the names of the sets.
	 */
	const std::vector<std::string>& getSetNames() const;
	
	/**
	 * Gets the names of the tracks in a set.
	 * @param set The name of the set to get the trackNames from.
	 * @returns A vector containing the names.
	 */
	std::vector<std::string> getTrackNames(const std::string& set) const;
	
	/**
	 * Gets the music manager singelton.
	 */
	static MusicManager* getInstance();
	
	/**
	 * creates the singleton.
	 */
	static void create();
	
	/**
	 * destroys the singleton.
	 */
	static void destroy();
	
private:
	
	/**
	 * A track. 
	 * Contains information aboud the track and the music data itself.
	 */
	struct Track
	{
		std::string setName;
		std::string filePath;
		Mix_Music* music;
	};
	
	
	typedef std::unordered_map<std::string, Track> TrackMap;
	
	MusicManager();
	void parseXml(const pugi::xml_node& musicNode);
	
	TrackMap tracks;
	std::vector<std::string> setNames;
	
	Mix_Music* lastTrackPlayed;
	std::chrono::high_resolution_clock::time_point startTime;
	std::chrono::duration<double> lastTrackDurationPlayed;
	
	static MusicManager* instance;
};

#endif /* MUSICMANAGER_H_ */
