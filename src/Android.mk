LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := main

SDL_PATH := ../SDL2
INCLUDE_PATH := ../include
STL_INCLUDE_PATH := ../libstdc++/include
STL_CONF_INCLUDE_PATH := ../libstdc++/libs/armeabi-v7a/include
PUGIXML_PATH := pugixml
JSONCPP_PATH := jsoncpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(INCLUDE_PATH) \
	$(LOCAL_PATH)/$(STL_INCLUDE_PATH) \
	$(LOCAL_PATH)/$(STL_CONF_INCLUDE_PATH) \
	$(LOCAL_PATH)/$(PUGIXML_PATH) \
	$(LOCAL_PATH)/$(JSONCPP_PATH) \
	$(LOCAL_PATH)/spriteloader \
	$(LOCAL_PATH)

# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
	$(subst $(LOCAL_PATH)/,, \
	$(wildcard $(LOCAL_PATH)/*.cpp) \
	$(wildcard $(LOCAL_PATH)/gui/*.cpp) \
	$(wildcard $(LOCAL_PATH)/android/*.cpp) \
	$(wildcard $(LOCAL_PATH)/utilities/*.cpp) \
	$(wildcard $(LOCAL_PATH)/spriteloader/*.cpp) \
	$(wildcard $(LOCAL_PATH)/$(PUGIXML_PATH)/*.cpp) \
	$(wildcard $(LOCAL_PATH)/$(JSONCPP_PATH)/*.cpp) \
	$(wildcard $(LOCAL_PATH)/physics/*.cpp) \
	$(wildcard $(LOCAL_PATH)/process/*.cpp))

LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image SDL2_mixer
LOCAL_STATIC_LIBRARIES := android_native_app_glue 
LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -llog -landroid
LOCAL_CPPFLAGS += -std=c++11 -w -fexceptions -Ofast -flto
LOCAL_ARM_MODE := arm

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)
