#include "PlayGamesServiceMenu.h"

#ifdef __ANDROID__
#include "android/AndroidApplication.h"
#endif

#ifdef __IPHONEOS__
#include "ios/GameCenterAndGooglePlayHandler.h"
#endif

#include "Assets.h"
#include "log.h"

PlayGamesServiceMenu::PlayGamesServiceMenu():
currentSignInState(SignInState::SIGNED_OUT)
{
}

PlayGamesServiceMenu::~PlayGamesServiceMenu()
{
	this->googlePlusSignInBtn->removeOnClickListener(this);
	this->leaderboardButton->removeOnClickListener(this);
	this->achievementsButton->removeOnClickListener(this);
}

void PlayGamesServiceMenu::setupUi(const std::shared_ptr<View>& gameServiceMenu, GeometrySet* quadHandler)
{
	float buttonSizePercentage = 30.0f;
	
	this->quadHandler = quadHandler;
	
	this->googleServicesList = std::make_shared<ListView>(ListView::Orientation::VERTICAL);
	this->googleServicesList->setWidthPercentage(100.0f);
	this->googleServicesList->setHeightPercentage(90.0f);
	this->googleServicesList->setPositioningTypeHorizontal(View::Position::CENTERED);
	this->googleServicesList->setPositioningTypeVertical(View::Position::CENTERED);
	this->googleServicesList->setItemPlacement(ListView::ItemPlacement::SPACED);
	this->googleServicesList->setDefaultColor(SDL_Color{0, 0, 0, 0});
	
	this->gameServicesList = std::make_shared<ListView>(ListView::Orientation::HORIZONTAL);
	this->gameServicesList->setWidthPercentage(60.0f);
	this->gameServicesList->setHeightPercentage(buttonSizePercentage);
	this->gameServicesList->setPositioningTypeHorizontal(View::Position::CENTERED);
	this->gameServicesList->setPositioningTypeVertical(View::Position::CENTERED);
	this->gameServicesList->setItemPlacement(ListView::ItemPlacement::SPACED);
	this->gameServicesList->setDefaultColor(SDL_Color{0, 0, 0, 0});
	
	this->googleServicesList->addChildView(this->gameServicesList);

	this->leaderboardButton = std::make_shared<Button>();
	this->leaderboardButton->setRenderQuad(quadHandler->createQuad());
	this->leaderboardButton->showBackgroundColor(true);
	this->leaderboardButton->setDefaultColor(SDL_Color{170, 170, 170, 100});
	this->leaderboardButton->setPressedColor(SDL_Color{170, 170, 170, 150});
	this->leaderboardButton->setHeightSizeType(View::SizeType::FIT_PARENT);
	this->leaderboardButton->setWidthSizeType(View::SizeType::FIT_PARENT);
	
	this->gameServicesList->addChildView(this->leaderboardButton);
	
	auto leaderboardImage = std::make_shared<ImageView>();
	leaderboardImage->setRenderQuad(quadHandler->createQuad());
	leaderboardImage->setDefaultColor(SDL_Color{255,255,255,255});
	leaderboardImage->setPressedColor(SDL_Color{255,255,255,255});
	leaderboardImage->setImage(Assets::getInstance()->spriteManager->getSprite("play_games_leaderboards_green.png"));
	leaderboardImage->setWidthSizeType(View::SizeType::FIT_PARENT);
	leaderboardImage->setHeightSizeType(View::SizeType::FIT_PARENT);
	leaderboardImage->setWidthPercentage(85.0f);
	leaderboardImage->setHeightPercentage(85.0f);
	
	this->leaderboardButton->addChildView(leaderboardImage);
	
	this->achievementsButton = std::make_shared<Button>();
	this->achievementsButton->setRenderQuad(quadHandler->createQuad());
	this->achievementsButton->showBackgroundColor(true);
	this->achievementsButton->setDefaultColor(SDL_Color { 170, 170, 170, 100 });
	this->achievementsButton->setPressedColor(SDL_Color { 170, 170, 170, 150 });
	this->achievementsButton->setHeightSizeType(View::SizeType::FIT_PARENT);
	this->achievementsButton->setWidthSizeType(View::SizeType::FIT_PARENT);
	
	this->gameServicesList->addChildView(this->achievementsButton);
	
	auto achivementsImage = std::make_shared<ImageView>();
	achivementsImage->setRenderQuad(quadHandler->createQuad());
	achivementsImage->setDefaultColor(SDL_Color { 255, 255, 255, 255 });
	achivementsImage->setPressedColor(SDL_Color { 255, 255, 255, 255 });
	achivementsImage->setImage(Assets::getInstance()->spriteManager->getSprite("play_games_achievements_green.png"));
	achivementsImage->setWidthSizeType(View::SizeType::FIT_PARENT);
	achivementsImage->setHeightSizeType(View::SizeType::FIT_PARENT);
	achivementsImage->setWidthPercentage(85.0f);
	achivementsImage->setHeightPercentage(85.0f);
	achivementsImage->setName("achievement");
	
	this->achievementsButton->addChildView(achivementsImage);
	
	this->googlePlusSignInBtn = std::make_shared<Button>();
	this->googlePlusSignInBtn->setRenderQuad(quadHandler->createQuad());
	this->googlePlusSignInBtn->setDefaultColor(SDL_Color{0, 0, 0, 0});
	this->googlePlusSignInBtn->setPressedColor(SDL_Color{170, 170, 170, 150});
	this->googlePlusSignInBtn->setHeightSizeType(View::SizeType::FIT_PARENT);
	this->googlePlusSignInBtn->setWidthSizeType(View::SizeType::FIT_PARENT);
	this->googlePlusSignInBtn->setHeightPercentage(buttonSizePercentage);

	this->googleServicesList->addChildView(this->googlePlusSignInBtn);

	this->googlePlusImage = std::make_shared<ImageView>();
	this->googlePlusImage->setRenderQuad(quadHandler->createQuad());
	this->googlePlusImage->setDefaultColor(SDL_Color{});
	this->googlePlusImage->setRenderQuad(quadHandler->createQuad());
	this->googlePlusImage->setDefaultColor(SDL_Color{255,255,255,255});
	this->googlePlusImage->setPressedColor(SDL_Color{255,255,255,255});
	this->googlePlusImage->setWidthSizeType(View::SizeType::FIT_PARENT);
	this->googlePlusImage->setHeightSizeType(View::SizeType::FIT_PARENT);
	this->googlePlusImage->setWidthPercentage(85.0f);
	this->googlePlusImage->setHeightPercentage(85.0f);
	this->googlePlusSignInBtn->addChildView(this->googlePlusImage);

	gameServiceMenu->addChildView(this->googleServicesList);
	
	this->googlePlusSignInBtn->addOnClickListener(this);
	this->leaderboardButton->addOnClickListener(this);
	this->achievementsButton->addOnClickListener(this);
	
	if (this->isSignedIn() == true) {
		this->onSignedIn();
	} else {
		this->googlePlusImage->setImage(Assets::getInstance()->spriteManager->getSprite(signedOutImage));
		this->gameServicesList->disable(true);
	}
	
#ifdef __ANDROID__
	AndroidApplication::getInstance()->addGoogleSignInCallback(std::bind(&PlayGamesServiceMenu::onSignedIn, this));
#endif
#ifdef __IPHONEOS__
	GameCenterAndGooglePlayHandler::setGooglePlayGamesSignedInCallback(std::bind(&PlayGamesServiceMenu::onSignedIn, this));
#endif
	
	
}

void PlayGamesServiceMenu::onClick(const View& clickedElement)
{
	GameServicesMenu::onClick(clickedElement);
	
	if (clickedElement == *(this->leaderboardButton.get())) {
		this->showLeaderBoard();
	}
	
	if (clickedElement == *(this->achievementsButton.get())) {
		this->showAchievements();
	}
	
	if (clickedElement == *(this->googlePlusSignInBtn.get())) {
		if (this->currentSignInState == SignInState::SIGNED_OUT) {
			this->signIn();
		} else if (this->currentSignInState == SignInState::SIGNED_IN) {
			this->signOut();
		}
	}
}

bool PlayGamesServiceMenu::isSignedIn()
{
#ifdef __ANDROID__
	return AndroidApplication::getInstance()->gameServiceIsSignedIn();
#elif defined __IPHONEOS__
	return GameCenterAndGooglePlayHandler::checkIfSignedInToGooglePlus();
#else
	return false;
#endif
}

void PlayGamesServiceMenu::signIn()
{
	if (this->currentSignInState == SignInState::SIGNED_OUT) {
		this->currentSignInState = SignInState::SIGNING_IN;
#ifdef __ANDROID__
		AndroidApplication::getInstance()->gameServiceSignIn();
#elif defined __IPHONEOS__
        GameCenterAndGooglePlayHandler::signInGooglePlusPlayer();
#endif
	}
}

void PlayGamesServiceMenu::signOut()
{
#ifdef __ANDROID__
	AndroidApplication::getInstance()->gameServiceSignOut();
#elif defined __IPHONEOS__
	GameCenterAndGooglePlayHandler::signOutGooglePlusPlayer();
#endif
	
	this->onSignedOut();
}

void PlayGamesServiceMenu::showLeaderBoard()
{
	if (this->currentSignInState == SignInState::SIGNED_IN) {
#ifdef __ANDROID__
		AndroidApplication::getInstance()->showLeaderBoard();
#elif defined __IPHONEOS__
		GameCenterAndGooglePlayHandler::showGooglePlayLeaderBoard();
#endif
	}
}

void PlayGamesServiceMenu::showAchievements()
{
	if (this->currentSignInState == SignInState::SIGNED_IN) {
#ifdef __ANDROID__
		AndroidApplication::getInstance()->showAchievements();
#elif defined __IPHONEOS__
		GameCenterAndGooglePlayHandler::showGooglePlayAchievements();
#endif
	}
}

void PlayGamesServiceMenu::onSignedOut()
{
	if (this->currentSignInState != SignInState::SIGNED_OUT) {
		this->currentSignInState = SignInState::SIGNED_OUT;
		
		this->gameServicesList->disable(true);
		
		this->googlePlusImage->setImage(Assets::getInstance()->spriteManager->getSprite(signedOutImage));
	}
}

void PlayGamesServiceMenu::onSignedIn()
{
	if (this->currentSignInState != SignInState::SIGNED_IN) {
		this->currentSignInState = SignInState::SIGNED_IN;
		
		this->gameServicesList->disable(false);
		
		this->googlePlusImage->setImage(Assets::getInstance()->spriteManager->getSprite(signedInImage));
	}
}
