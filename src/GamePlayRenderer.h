#ifndef __ultragame__GamePlayRenderer__
#define __ultragame__GamePlayRenderer__

#include "Renderer.h"

class GamePlayRenderer : public Renderer {
public:
	void render() override;
	
};
#endif /* defined(__ultragame__GamePlayRenderer__) */
