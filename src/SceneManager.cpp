#include "SceneManager.h"

#include "log.h"

SceneManager::SceneManager()
{
}

SceneManager::~SceneManager()
{
	this->clearScenes();
}

void SceneManager::addScene(const std::string& name, const std::shared_ptr<Scene>& scene)
{
	if (scenes.find(name) == scenes.end()) {
		scenes[name] = scene;
		scene->setHandler(this);
		scene->onCreate();
	} else {
		LOGE("Adding scene with already existing name %s\n", name.c_str());
	}
}

void SceneManager::changeScene(const std::string& name)
{
	if (scenes.find(name) == scenes.end()) {
		LOGE("Could not find scene %s\n", name.c_str());
		return;
	}

	if (this->activeScene == nullptr) {
		activeScene = scenes[name];
		activeScene->onActivate();
	} else {
		nextScene = name;
	}
}

void SceneManager::onRender()
{
	if (activeScene != nullptr) {
		activeScene->onRender();
	}
}

void SceneManager::onUpdate(float deltaTime)
{
	if (activeScene != nullptr) {
		activeScene->onUpdate(deltaTime);
	}

	if (nextScene != "") {
		if (activeScene != nullptr) {
			activeScene->onDeactivate();
		}

		if (scenes.find(nextScene) != scenes.end()) {
			activeScene = scenes[nextScene];
			activeScene->onActivate();
			activeScene->onUpdate(deltaTime);
		} else {
			LOGE("Could not find scene %s\n", nextScene.c_str());
		}

		nextScene = "";
	}
}

void SceneManager::onEvent(const SDL_Event& event)
{
	if (event.type == SDL_WINDOWEVENT) {
		for (auto scene : scenes) {
			scene.second->onEvent(event);
		}
	} else if (activeScene != nullptr) {
		activeScene->onEvent(event);
	}
}

bool SceneManager::removeScene(const std::string& name)
{
	auto sceneIt = scenes.find(name);
	if (sceneIt == scenes.end()) {
		LOGE("Could not find scene %s\n", name.c_str());
		return false;
	}

	sceneIt->second->onDestroy();
	return true;
}

void SceneManager::clearScenes()
{
	for (auto scene : this->scenes) {
		scene.second->onDestroy();
	}

	this->scenes.clear();
}
