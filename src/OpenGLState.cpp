//
//  OpenGLState.cpp
//  ultragame
//
//  Created by Asbjoern Sporaland on 15/12/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#include "OpenGLState.h"

GLuint OpenGLState::currentlyBoundVBO = 0;
GLuint OpenGLState::currentlyUsedProgram = 0;
GLuint OpenGLState::currentlyBoundTexture = 0;

