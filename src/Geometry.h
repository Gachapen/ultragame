#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#include <vector>
#include <glm/glm.hpp>
#include "spriteloader/Sprite.h"


class GeometrySet;

enum class GeometryType {
	TRIANGLE,
	QUAD,
	LINE,
	POLYGON
};

struct ObjectCoordinate
{
public:
    glm::vec2 geometryVertex;
    glm::vec4 color;
    glm::vec2 textureCoordinate;
};

/**
 * Representation of geometry.
 * Several classes inherit from this to represent some form of geometry.
 * Every time the color or vertex of a geometry instance is change
 * you need to call signalChange() to update the actual renderable
 * vertices.
 */
class Geometry
{
public:
	/**
	 * Construct Geometry.
	 * @param container The container which this geometry is constructed from.
	 */
	Geometry(GeometrySet* container);

	virtual ~Geometry()
	{}

	/**
	 * Set the color for the whole object (all vertices).
	 * @param color The color to set.
	 */
	virtual void setColor(const glm::vec4& color) = 0;

	/**
	 * Generates vertices ready for rendering.
	 * @return The vertices generated.
	 */
	virtual std::vector<ObjectCoordinate> generateRenderableVertices() const = 0;

	/**
	 * The number of vertices used for rendering.
	 * This number will be the same as the size of the returned vector of generateRenderableVertices().
	 * @return Number of vertices.
	 */
	virtual unsigned int getNumVertices() const = 0;

	/**
	 * Get the number of render object.
	 * A shape (geometry) might consist of several shapes. E.g. a quad
	 * has two triangles.
	 * This is information used by the renderer.
	 * @return Number of render objects.
	 */
	virtual unsigned int getNumRenderObjects() const = 0;

	/**
	 * Signal that the vertices has changed to the GeometrySet that contains
	 * this geometry.
	 * When changing colors or vertices, you must call this function for the
	 * representation in the containing GeometrySet to be correct.
	 * This will update the GeometrySet's representation of the vertices.
	 * This function is expensive since the renderable vertices has to
	 * be recalculated and copied.
	 */
	void signalChange();

	void removeFromContainer();
	GeometrySet* getContainer();

private:
	GeometrySet* container;
};


class Triangle: public Geometry
{
public:
	Triangle(GeometrySet* container);

	void setColor(const glm::vec4& color) override;
	std::vector<ObjectCoordinate> generateRenderableVertices() const override;
	unsigned int getNumVertices() const override;
	unsigned int getNumRenderObjects() const override;

	void setVertexA(const glm::vec2& vertex);
	void setVertexB(const glm::vec2& vertex);
	void setVertexC(const glm::vec2& vertex);
	void setVertices(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c);

	void setColorA(const glm::vec4& color);
	void setColorB(const glm::vec4& color);
	void setColorC(const glm::vec4& color);
	void setColors(const glm::vec4& a, const glm::vec4& b, const glm::vec4& c);

private:
	ObjectCoordinate a;
	ObjectCoordinate b;
	ObjectCoordinate c;
};


/**
 * Polygon with four corners.
 * This is the same as a Polygon with only four vertices,
 * but this might be more efficient since it stores
 * only two triangles for the rendering, rather than four.
 */
class Quad: public Geometry
{
public:
	Quad(GeometrySet* container, const Sprite& defaultSprite);

	void setColor(const glm::vec4& color) override;
	std::vector<ObjectCoordinate> generateRenderableVertices() const override;
	unsigned int getNumVertices() const override;
	unsigned int getNumRenderObjects() const override;

	void setVertexBottomLeft(const glm::vec2& vertex);
	void setVertexBottomRight(const glm::vec2& vertex);
	void setVertexTopRight(const glm::vec2& vertex);
	void setVertexTopLeft(const glm::vec2& vertex);
	void setVertices(const glm::vec2& bottomLeft, const glm::vec2& bottomRight, const glm::vec2& topRight, const glm::vec2& topLeft);

	void setColorBottomLeft(const glm::vec4& color);
	void setColorBottomRight(const glm::vec4& color);
	void setColorTopRight(const glm::vec4& color);
	void setColorTopLeft(const glm::vec4& color);
	void setColors(const glm::vec4& bottomLeft, const glm::vec4& bottomRight, const glm::vec4& topRight, const glm::vec4& topLeft);

	void setTexture(const Sprite& texture);

private:
	void setTextureCoordinates(const Sprite& texture);

	ObjectCoordinate bottomLeft;
	ObjectCoordinate bottomRight;
	ObjectCoordinate topRight;
	ObjectCoordinate topLeft;

	Sprite quadSprite;
};


class Line: public Geometry
{
public:
	Line(GeometrySet* container);

	void setColor(const glm::vec4& color) override;
	std::vector<ObjectCoordinate> generateRenderableVertices() const override;
	unsigned int getNumVertices() const override;
    unsigned int getNumRenderObjects() const override;

	void setVertexStart(const glm::vec2& vertex);
	void setVertexEnd(const glm::vec2& vertex);
	void setVertices(const glm::vec2& start, const glm::vec2& end);

	void setColorStart(const glm::vec4& color);
	void setColorEnd(const glm::vec4& color);
	void setColors(const glm::vec4& start, const glm::vec4& end);

private:
	ObjectCoordinate start;
	ObjectCoordinate end;
};


//TODO: Use indices for a triangle fan.
/**
 * Polygon.
 * This can be both convex and concave.
 */
class Polygon: public Geometry
{
public:
    Polygon(GeometrySet* container, unsigned int numVertices);

    void setColor(const glm::vec4& color) override;
    std::vector<ObjectCoordinate> generateRenderableVertices() const override;
    unsigned int getNumVertices() const override;
    unsigned int getNumRenderObjects() const override;

    void setVertex(unsigned int vertexNum, const glm::vec2& vertex);
    void setColor(unsigned int vertexNum, const glm::vec4& color);

private:
    std::vector<ObjectCoordinate> coordinates;
    glm::vec4 color;
    const unsigned int NUM_VERTICES;
};

#endif /* IGEOMETRY_H_ */
