#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <SDL2/SDL.h>
#include <string>
#include <memory>
#include <chrono>
#include "Assets.h"
#include "Localization.h"
#include "SceneManager.h"
#include "IGetLocale.h"
#include "Renderer.h"
#include "IFileSaver.h"
#include "utilities/opengl_utils.h"


class Application
{
public:
	Application();
	virtual ~Application();

	/**
	 * Initiates the application and makes it ready to execute.
	 */
	bool init();
	
	/**
	 * Starts running the application.
	 * Holds the gameloop.
	 */
	int execute();
	
	/**
	 * Handles any event.
	 * Polls SDL for events that have not been processed.
	 */
	void handleEvents();
	
	/**
	 * Updates the scene that is currently active.
	 */
	void update();
	
	/**
	 * Handles the rendering of the world
	 */
	void render();

	/**
	 * Checks wether the application is running or not.
	 */
	bool isRunning() const;
	
	/**
	 * Sets the running state of the application.
	 * @param state The state you want to set the application in.
	 */
	void setRunning(bool state);
	
	/**
	 * Gets the SDL_window created and used by SDL to present to screen.
	 * @returns The SDL_Window created and used by SDL.
	 */
	SDL_Window* getWindow();

	/**
	 * Handles the special cases of events that need to be handeled straight away
	 * @param the actual event.
	 */
	int onEventFilter(SDL_Event* event);

private:
	/**
	 * Handles what need to be done when the application is paused.
	 * Stopping music etc.
	 */
	void onPause();
	
	/**
	 * Handles what need to be done when the application resumes.
	 * Resumes music etc.
	 */
	void onResume();
	
	/** Handles application shutdown. */
	void shutdown();

	/** 
	 * Determines wether or not the application keeps runnung.
	 * If set to false, the application will stop.
	 */
	bool running;
	
	/** Determines wether or not the shutdown procedure has allready been run. */
	bool hasShutDown;

	/** The window used to present to the screen. */
	SDL_Window* window;
	
	/** The OpenGL context created by SDL. */
	SDL_GLContext glContext;

	/** The window title, Only usefull for desktop version. */
	const std::string windowTitle = "Ultragame";

	/** Manages the different scenes of the application. */
	SceneManager sceneHandler;

	/** High res timepont that keeps the previous frame startpoint. */
	std::chrono::high_resolution_clock::time_point previousFrameStartTime;

	/** Handles reading and writing to files. */
	std::unique_ptr<IFileSaver> fileHandler;
};

#endif /* APPLICATION_H_ */
