#ifndef PHYSICSBODY_H_
#define PHYSICSBODY_H_

#include <memory>
#include <functional>
#include <glm/glm.hpp>
#include "PhysicsShape.h"


/**
 * Used to simulate the position of the body. 
 * Contains information needed for the simulation
 * of the position etc. of the body.
 */
class PhysicsBody
{
public:
	enum class Type {
		STATIC,		//!< This is a body that does noy move.
		DYNAMIC		//!< This is a body that moves with physics.
	};

	PhysicsBody();

	/** 
	 * Inititates the body.
	 * Resets all the values of the body.
	 */
	void init();

	glm::vec2 getPosition() const;
	void setPosition(glm::vec2 position);

	glm::vec2 getVelocity() const;
	void setVelocity(glm::vec2 velocity);

	glm::vec2 getMomentum() const;
	void setMomentum(glm::vec2 momentum);

	void resetForce();
	glm::vec2 getForce() const;
	void setForce(glm::vec2 force);
	void addForce(glm::vec2 force);

	void addImpulse(glm::vec2 impulse);
	glm::vec2 getForceOfImpulse(float deltaTime) const;
	void resetImpulse();

	glm::vec2 getDampening() const;
	void setDampening(glm::vec2 dampening);

	float getMass() const;
	void setMass(float mass);

	void setShape(PhysicsShape* shape);
	PhysicsShape* getShape() const;

	/**
	 * Checks if the body is disabled.
	 * If the body is disabled it will not collide.
	 * @returns A bool that is true if the body is disabled.
	 */
	bool isDisabled() const;
	
	/**
	 * Sets the disabled state of the body.
	 * @param state the state to set for the disabled variable.
	 */
	void setDisabled(bool state);

	/**
	 * Checks if the body is simulated.
	 * If a body is simulated it will be updated by physics.
	 * @returns a bool that determines wether or not the body is simulated.
	 */
	bool isSimulated() const;
	
	/**
	 * Sets the body to the passed along simulation state
	 * @param state The state to be set.
	 */
	void setSimulated(bool state);

	/**
	 * Sets the callback to be called when the position changes.
	 * @param callback The function that will be called.
	 */
	void setPositionChangedCallback(std::function<void (const glm::vec2&)> callback);

private:
	float mass;
	Type bodyType;
	std::unique_ptr<PhysicsShape> shape;
	
	/** If a body is simulated it will be updated by physics. */
	bool simulated;
	
	/** If the body is disabled it will not collide. */
	bool disabled;

	glm::vec2 position;
	glm::vec2 velocity;
	glm::vec2 momentum;
	glm::vec2 force;
	glm::vec2 dampening;
	glm::vec2 impulse;

	std::function<void (const glm::vec2&)> positionChangedCallback;
};

#endif /* PHYSICSBODY_H_ */
