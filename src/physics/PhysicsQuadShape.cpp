#include "PhysicsQuadShape.h"

PhysicsQuadShape::PhysicsQuadShape(float width, float height):
	width(width),
	height(height)
{
}

PhysicsShape::Type PhysicsQuadShape::getShapeType() const
{
	return PhysicsShape::Type::QUAD;
}

float PhysicsQuadShape::getWidth() const
{
	return this->width;
}

PhysicsShape* PhysicsQuadShape::clone() const
{
	PhysicsQuadShape* shape = new PhysicsQuadShape(this->width, this->height);
	return shape;
}

float PhysicsQuadShape::getTop() const
{
	return (this->height / 2.0f);
}

float PhysicsQuadShape::getBottom() const
{
	return -(this->height / 2.0f);
}

float PhysicsQuadShape::getRight() const
{
	return (this->width / 2.0f);
}

float PhysicsQuadShape::getLeft() const
{
	return -(this->width / 2.0f);
}

float PhysicsQuadShape::getHeight() const
{
	return this->height;
}
