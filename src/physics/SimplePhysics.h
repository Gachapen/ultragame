#ifndef __ultragame__SimplePhysics__
#define __ultragame__SimplePhysics__

#include <vector>

#include "ICollisionListener.h"
#include "PhysicsBody.h"

extern const float GRAVITATIONAL_CONSTANT;

class SimplePhysics {
	
public:
	
	SimplePhysics();
	
	/**
	 * Used to set the collisin listener for the physics.
	 * The listener handles the collisions that occour.
	 * @param listener The ListenerObject.
	 */
	void setCollisionListener (ICollisionListener* listener);
	
	/**
	 * Creates a body ready to be simulated.
	 */
	PhysicsBody* createBody();
	
	/**
	 * Removes a body from the simulation.
	 * @param body The body to be removed.
	 */
	bool deleteBody(PhysicsBody* body);
	
	/**
	 * Updates the simulation with the time step.
	 * param deltaTime The timestep.
	 */
	void update(float deltaTime);
	
private:
	/**
	 * Updates a single body simulation by the timestep
	 * @param body The body to be updated.
	 * @param deltaTime The timestep.
	 */
	void updateBodyPhysics(PhysicsBody* body, float deltaTime);
	
	/**
	 * Checks for collisions in the scene.
	 */
	void checkCollisions();
	
	/**
	 * Checks if two bodies are colliding.
	 * @param firstBody The first body to check.
	 * @param secondBody The other body to check.
	 * @returns True if a collision was found.
	 */
	bool areColliding(PhysicsBody* firstBody, PhysicsBody* secondBody);
	
	/**
	 * The listener for collisions
	 * Will be called when a collision occours.
	 */
	ICollisionListener* collisionListener;
	
	/**
	 * Contains all the bodies that are simulated by the system.
	 */
	std::vector<std::unique_ptr<PhysicsBody>> bodies;
};


#endif /* defined(__ultragame__SimplePhysics__) */



