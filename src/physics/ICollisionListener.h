#ifndef ultragame_ICollisionListener_h
#define ultragame_ICollisionListener_h

class PhysicsBody;

class ICollisionListener {
public:
	virtual ~ICollisionListener() {}
	/**
	 * Each listener need to override this function, it is called when a collision occurs.
	 * The two bodies that collide are passed along.
	 * @param firstBody One of the bodies that collided.
	 * @param secondBody The other body that collided.
	 */
	virtual void onCollision(PhysicsBody* firstBody, PhysicsBody* secondBody) = 0;
	
};


#endif
