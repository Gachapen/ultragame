#ifndef PHYSICSQUADSHAPE_H_
#define PHYSICSQUADSHAPE_H_

#include "PhysicsShape.h"


/**
 * A rectangular shape.
 */
class PhysicsQuadShape: public PhysicsShape
{
public:
	PhysicsQuadShape(float width, float height);
	PhysicsShape::Type getShapeType() const override;
	PhysicsShape* clone() const override;
	float getTop() const override;
	float getBottom() const override;
	float getRight() const override;
	float getLeft() const override;

	/**
	 * Gets the with of the rectangle
	 * @returns The width.
	 */
	float getWidth() const;
	
	/**
	 * Gets the height of the rectangle.
	 * @returns The height.
	 */
	float getHeight() const;

private:
	float width;
	float height;
};

#endif /* PHYSICSQUADSHAPE_H_ */
