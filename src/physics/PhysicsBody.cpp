#include "PhysicsBody.h"

#include "SimplePhysics.h"

PhysicsBody::PhysicsBody():
	mass(1.0f),
	bodyType(Type::DYNAMIC),
	simulated(true),
	disabled(false)
{
}

void PhysicsBody::init()
{
	this->position = glm::vec2();
	this->velocity = glm::vec2();
	this->momentum = glm::vec2();
	this->force = glm::vec2();
	this->dampening = glm::vec2();
	this->impulse = glm::vec2();
}

glm::vec2 PhysicsBody::getPosition() const
{
	return this->position;
}

void PhysicsBody::setPosition(glm::vec2 position)
{
	this->position = position;
	if (this->positionChangedCallback)
	{
		this->positionChangedCallback(this->position);
	}
}

glm::vec2 PhysicsBody::getVelocity() const
{
	return this->velocity;
}

void PhysicsBody::setVelocity(glm::vec2 velocity)
{
	this->velocity = velocity;
}

glm::vec2 PhysicsBody::getMomentum() const
{
	return this->momentum;
}

void PhysicsBody::setMomentum(glm::vec2 momentum)
{
	this->momentum = momentum;
}

void PhysicsBody::resetForce()
{
	this->force = glm::vec2();
}

glm::vec2 PhysicsBody::getForce() const
{
	return this->force;
}

void PhysicsBody::setForce(glm::vec2 force)
{
	this->force = force;
}

void PhysicsBody::addForce(glm::vec2 force)
{
	this->force += force;
}

void PhysicsBody::addImpulse(glm::vec2 impulse)
{
	this->impulse += impulse;
}

glm::vec2 PhysicsBody::getForceOfImpulse(float deltaTime) const
{
	return glm::vec2(this->impulse.x / deltaTime, this->impulse.y / deltaTime);
}

void PhysicsBody::resetImpulse()
{
	this->impulse = glm::vec2();
}

glm::vec2 PhysicsBody::getDampening() const
{
	return this->dampening;
}

void PhysicsBody::setDampening(glm::vec2 dampening)
{
	this->dampening = dampening;
}

float PhysicsBody::getMass() const
{
	return this->mass;
}

void PhysicsBody::setMass(float mass)
{
	this->mass = mass;
}

void PhysicsBody::setShape(PhysicsShape* shape)
{
	this->shape = std::unique_ptr<PhysicsShape>(shape->clone());
}

PhysicsShape* PhysicsBody::getShape() const
{
	return this->shape.get();
}

bool PhysicsBody::isDisabled() const
{
	return this->disabled;
}

void PhysicsBody::setDisabled(bool state)
{
	this->disabled = state;
}

bool PhysicsBody::isSimulated() const
{
	return this->simulated;
}

void PhysicsBody::setSimulated(bool state)
{
	this->simulated = state;
}

void PhysicsBody::setPositionChangedCallback(std::function<void (const glm::vec2&)> callback)
{
	this->positionChangedCallback = callback;
}
