#include "SimplePhysics.h"

#include <algorithm>
#include <SDL2/SDL.h>
#include "../log.h"

const float GRAVITATIONAL_CONSTANT = 6.67384E-11;

SimplePhysics::SimplePhysics():
    collisionListener(nullptr)
{
}

void SimplePhysics::setCollisionListener(ICollisionListener *listener)
{
    this->collisionListener = listener;
}

PhysicsBody* SimplePhysics::createBody()
{
	PhysicsBody* body = new PhysicsBody();
	this->bodies.push_back(std::unique_ptr<PhysicsBody>(body));

	return body;
}

bool SimplePhysics::deleteBody(PhysicsBody* body)
{
	auto bodyIt = std::find_if(
			this->bodies.begin(),
			this->bodies.end(),
			[body] (const std::unique_ptr<PhysicsBody>& item) {
				return (item.get() == body);
			}
	);

	if (bodyIt != this->bodies.end())
	{
		this->bodies.erase(bodyIt);
		return true;
	}
	else
	{
		return false;
	}
}
void SimplePhysics::update(float deltaTime)
{
	for (const std::unique_ptr<PhysicsBody>& body : this->bodies)
	{
		this->updateBodyPhysics(body.get(), deltaTime);
	}

	this->checkCollisions();
}

void SimplePhysics::updateBodyPhysics(PhysicsBody* body, float deltaTime)
{
	assert(deltaTime != 0);

	glm::vec2 temporaryVector = body->getForce();
	temporaryVector.x += body->getForceOfImpulse(deltaTime).x;
	temporaryVector.y += body->getForceOfImpulse(deltaTime).y;
	body->resetImpulse();

	glm::vec2 dampening = body->getDampening();
	temporaryVector.x += -dampening.x * body->getVelocity().x;
	temporaryVector.y += -dampening.y * body->getVelocity().y;
	body->setForce(temporaryVector);

	temporaryVector = body->getPosition();
	temporaryVector.x += (body->getVelocity().x * deltaTime);
	temporaryVector.y += (body->getVelocity().y * deltaTime);
	body->setPosition(temporaryVector);

	temporaryVector = body->getMomentum();
	temporaryVector.x += (body->getForce().x * deltaTime);
	temporaryVector.y += (body->getForce().y * deltaTime);
	body->setMomentum(temporaryVector);

	temporaryVector = body->getVelocity();
	temporaryVector.x = body->getMomentum().x / body->getMass();
	temporaryVector.y = body->getMomentum().y / body->getMass();
	body->setVelocity(temporaryVector);

	body->resetForce();
}

void SimplePhysics::checkCollisions()
{
	for (const std::unique_ptr<PhysicsBody>& firstBody : this->bodies)
	{
		for (const std::unique_ptr<PhysicsBody>& secondBody : this->bodies)
		{
			if (firstBody != secondBody && firstBody->isDisabled() == false && secondBody->isDisabled() == false)
			{
				if (this->areColliding(firstBody.get(), secondBody.get()) == true)
				{
					this->collisionListener->onCollision(firstBody.get(), secondBody.get());
				}
			}
		}
	}
}

bool SimplePhysics::areColliding(PhysicsBody* firstBody, PhysicsBody* secondBody)
{
	PhysicsShape* firstShape = firstBody->getShape();
	PhysicsShape* secondShape = secondBody->getShape();

	if (firstShape == nullptr || secondShape == nullptr)
	{
		return false;
	}

	if (firstShape->getShapeType() == PhysicsShape::Type::QUAD && secondShape->getShapeType() == PhysicsShape::Type::QUAD)
	{
		if (firstBody->getPosition().x + firstShape->getRight() < secondBody->getPosition().x + secondShape->getLeft()) {
			return false;
		}

		if (firstBody->getPosition().x + firstShape->getLeft() > secondBody->getPosition().x + secondShape->getRight()) {
			return false;
		}

		if (firstBody->getPosition().y + firstShape->getTop() < secondBody->getPosition().y + secondShape->getBottom()) {
			return false;
		}

		if (firstBody->getPosition().y + firstShape->getBottom() > secondBody->getPosition().y + secondShape->getTop()) {
			return false;
		}

		return true;
	}

	return false;
}

