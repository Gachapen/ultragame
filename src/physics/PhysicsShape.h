#ifndef PHYSICSSHAPE_H_
#define PHYSICSSHAPE_H_


/**
 * Representes the physical object.
 * The shape is used for collisiondetection.
 */
class PhysicsShape
{
public:
	enum class Type
	{
		QUAD
	};

	/**
	 * Gets the type of the shape.
	 * @returns The type of the shape.
	 */
	virtual Type getShapeType() const = 0;
	
	/**
	 * Clones the shape.
	 * @returns An identical copy of the shape.
	 */
	virtual PhysicsShape* clone() const = 0;
	
	/**
	 * Returns the world axis aligned max Y.
	 * @returns the biggest Y world coordinate on the shape.
	 */
	virtual float getTop() const = 0;
	
	/**
	 * Returns the world axis aligned min Y.
	 * @returns the smallest Y world coordinate on the shape.
	 */
	virtual float getBottom() const = 0;
	
	/**
	 * Returns the world axis aligned max X.
	 * @returns the biggest X world coordinate on the shape.
	 */
	virtual float getRight() const = 0;
	
	/**
	 * Returns the world axis aligned min X.
	 * @returns the smallest
	 */
	virtual float getLeft() const = 0;
};

#endif /* PHYSICSSHAPE_H_ */
