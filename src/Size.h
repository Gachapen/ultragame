//
//  Size.h
//  ultragame
//
//  Created by Asbjoern Sporaland on 9/29/13.
//  Copyright (c) 2013 masb. All rights reserved.
//

#ifndef __ultragame__Size__
#define __ultragame__Size__

class Size {
public:
    Size();
    Size(float width, float height);
    ~Size();
    
    float width;
    float height;
};


#endif /* defined(__ultragame__Size__) */
