#ifndef __ultragame__Game__
#define __ultragame__Game__

#include <string>
#include <map>
#include <chrono>
#include <SDL2/SDL.h>


#include <SDL2/SDL_mixer.h>

#include "physics/SimplePhysics.h"
#include "physics/ICollisionListener.h"
#include "Localization.h"
#include "Assets.h"
#include "Scene.h"
#include "gui/View.h"
#include "gui/Button.h"
#include "gui/IOnClickListener.h"
#include "Application.h"
#include "IFileSaver.h"
#include "Size.h"
#include "BlackHole.h"
#include "process/ProcessManager.h"
#include "QuadRenderObject.h"

#include "Renderer.h"

class GameScene: public Scene, public ICollisionListener, public IOnClickListener {
public:
	GameScene();
	~GameScene();

	/**
	 * Called when a collision occours.
	 * @param firstBox colliding Box.
	 * @param secondBox other colliding Box.
	 */
	void onCollision(PhysicsBody* firstBox, PhysicsBody* secondBox) override;
	void onClick(const View& clickedElement) override;

	/**
	 * Handles the initializing of the game
	 */
	void initGame();
	
	/**
	 * Handles the game quitting.
	 */
	void quitGame();
	
	/**
	 * Sets the size of the window.
	 */
	void setWindowSize(SDL_Point size);

	void onRender() override;
	void onUpdate(float deltaTime) override;
	void onEvent(const SDL_Event& event) override;

	void onCreate() override;
	void onActivate() override;
	void onDeactivate() override;
	void onDestroy() override;

private:
	
	/**
	 * Adds a new box to the scene.
	 * @param isAddingToTopOfScreen specifies where on the screen the box is to be added.
	 */
	void addNewBox(bool isAddingToTopOfScreen);
	
	void updatePlatformWidths(float width);
	
	/**
	 * Handles the ending of the game.
	 * Saving and reporting scores to Google Play Games and Apple Gamecenter.
	 */
	void onGameOver();
	
	/**
	 * Called when the player hits a platform.
	 * Updates the score and increases the difficulty.
	 */
	void playerPlatformHit();
	
		
	/**
	 * Handles the setup of the pause menu.
	 */
	void setupPauseMenu();
	
	/**
	 * Sets the color of the objcet passed in.
	 * @param physicsBody the physics representation of the body, used to find the position
	 * @param renderObject the objet to be updated.
	 */
	void setObjectColor(const PhysicsBody* physicsBody, RenderObject* renderObject);
	
	/**
	 * Handles the pausing of the game.
	 * displays the pasue menu when paused.
	 * @param pause The state to set the game in.
	 */
	void pauseGame(bool pause);

	/**
	 * The number of times the player has hit a box this game.
	 * Used to give the player a score.
	 */
	unsigned int hits;

	SDL_Joystick* accelerometer;
	float platformMaxTilt;

	SimplePhysics physics;
	Size worldSize;
	glm::vec2 worldCenter;

	glm::ivec2 screenSize;
	glm::vec2 worldToScreenScale;

	PhysicsBody* platformTopBody;
	QuadRenderObject platformTopRenderObject;

	PhysicsBody* platformBottomBody;
	QuadRenderObject platformBottomRenderObject;

	glm::vec2 platformSizeScaling;
	Size platformSize;
	std::chrono::duration<float> platformSlideDuration;

	PhysicsBody* playerBody;
	QuadRenderObject playerRenderObject;

	float playerSizeScaling;
	Size playerSize;
	float playerBoxTravelDistance;
	float playerBoxDesiredTravelTime;
	float playerBoxTravelTimeLeft;
	float playerTiltDampening;

	std::shared_ptr<GeometrySet> gameplayQuads;
	std::shared_ptr<GeometrySet> pauseMenuQuads;

	float firstColor;
	bool firstColorIsIncreasing;
	
	float basePlatformWidth;
	float platformWidthDecrease;

	float middleXPosition;
	std::vector<std::unique_ptr<BlackHole>> blackHoles;

	Mix_Chunk* playerDeathSound;

	std::shared_ptr<View> pauseMenu;
	std::shared_ptr<Button> resumeButton;
	std::shared_ptr<Button> quitButton;
	bool paused;

	std::unique_ptr<Renderer> renderer;

	ProcessManager processManager;

	/** The stage of the gameplay. The higher, the more difficult. Starts at 0. */
	unsigned int stage;
	const unsigned int NUM_STAGES;

	std::map<unsigned int, unsigned int> hitsToStage;
	std::map<int, std::string> scoreAchievementRequirement;
};

#endif /* defined(__ultragame__Game__) */
