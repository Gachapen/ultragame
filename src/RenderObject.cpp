#include "RenderObject.h"

void RenderObject::updatePosition(const glm::vec2& position)
{
	this->position = position;
	this->onPositionUpdated(this->position);
}

void RenderObject::updateColor(const glm::vec4& color)
{
	this->color = color;
	this->onColorUpdated(color);
}
