#ifndef ANDROIDFILESAVER_H_
#define ANDROIDFILESAVER_H_

#include "IFileSaver.h"
#include <vector>

class GeneralFileSaver: public IFileSaver {
public:
	virtual ~GeneralFileSaver();

	bool writeString(const std::string& string, const std::string& filePath) override;
	bool appendString(const std::string& string, const std::string& filePath) override;
    std::vector<unsigned char> readFile(const std::string& filePath) override;
    std::string readFileAsString(const std::string& filePath);
    
    void deleteFile(const std::string& filePath) override;
};

#endif /* ANDROIDFILESAVER_H_ */
