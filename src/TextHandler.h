#ifndef __ultragame__TextHandler__
#define __ultragame__TextHandler__

#include <vector>
#include <string>
#include "Geometry.h"


/**
 * Handles the text.
 */
class TextHandler
{
public:
	/**
	 * Creates the TextHandler and sets the set used for rendering.
	 * @param set The GeometrySet used for rendering.
	 */
	TextHandler(GeometrySet* set);
	~TextHandler();
	
	
	/**
	 * Sets the text to be displayed.
	 * @param text The text to be displayed.
	 */
	void setText(std::string text);
	
	/**
	 * Sets the color of the text
	 * @param color the color we want.
	 */
	void setColor(glm::vec4 color);
	
	/**
	 * Sets the position of the text.
	 * @param position the positon wanted.
	 */
	void setPosition(const glm::vec2& position);
	
	/**
	 * Sets the size of the text.
	 * @param size the size we want.
	 */
	void setSize(const glm::vec2& size);
	
	/**
	 * Re-creates the renderdata used to render the text
	 */
	void reCreateRenderData();

	/**
	 * Gets the size of the text.
	 */
	const glm::ivec2& getTextSize();
	
private:
	void updateTextSize();
	void updateTextBox();
	void reCreateQuads();

	std::string text;
	GeometrySet* geometrySet;
	std::vector<Quad*> characters;
	glm::vec4 textColor;
	glm::ivec2 textSize;
	glm::vec2 size;
	glm::vec2 position;
};

#endif /* defined(__ultragame__TextHandler__) */
