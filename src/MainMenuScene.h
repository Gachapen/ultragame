#ifndef MAINMENUSCENE_H_
#define MAINMENUSCENE_H_

#include "Scene.h"
#include "gui/Button.h"
#include "gui/View.h"
#include "gui/IOnClickListener.h"
#include "gui/Label.h"
#include "gui/ListView.h"
#include "Application.h"

#if defined(__IPHONEOS__) || defined(__ANDROID__)
#include "PlayGamesServiceMenu.h"
#endif

class MainMenuScene: public Scene, public IOnClickListener
{
public:
	MainMenuScene(Application* context);
	virtual ~MainMenuScene();
	
	void setWindowSize(SDL_Point size);
	
	void onRender() override;
	void onUpdate(float deltaTime) override;
	void onEvent(const SDL_Event& event) override;
	
	void onCreate() override;
	void onActivate() override;
	void onDestroy() override;
	
	void onClick(const View& clickedElement) override;
	
private:
	
#if defined(__IPHONEOS__) || defined(__ANDROID__)
	std::shared_ptr<Button> playGamesBtn;
	std::shared_ptr<PlayGamesServiceMenu> playGamesMenu;
#endif
	
#ifdef __IPHONEOS__
	std::shared_ptr<Button> gameCenterBtn;
#endif
	
	SDL_Point windowSize;
	View menuView;
        
	std::shared_ptr<ListView> buttonListView;
	std::shared_ptr<Button> startButton;
	std::shared_ptr<View> scoreLabelView;
	std::shared_ptr<Label> scoreLabel;
	std::shared_ptr<View> scoreTitleView;
	
	bool menuViewBlueColorIncreasing;
	bool menuViewGreenColorIncreasing;
	
	SDL_Color startButtonColor;
	bool startButtonRedIncreasing;
	bool startButtonGreenIncreasing;
	
	bool signedIntoGoogle;
	
	std::unique_ptr<Renderer> renderer;
	std::shared_ptr<GeometrySet> menuQuads;
	
	
	Application* context;
};

#endif /* MAINMENUSCENE_H_ */
