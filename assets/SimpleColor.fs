uniform sampler2D renderTexture;

varying vec4 colorCoordinate;
varying vec2 fragmentTextureCoordinate;

void main()
{
    gl_FragColor = texture2D(renderTexture, fragmentTextureCoordinate) * colorCoordinate;
}
