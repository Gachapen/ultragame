uniform sampler2D renderTexture;

varying lowp vec4 colorCoordinate;
varying lowp vec2 fragmentTextureCoordinate;

void main()
{
    gl_FragColor = texture2D(renderTexture, fragmentTextureCoordinate) * colorCoordinate;
}
