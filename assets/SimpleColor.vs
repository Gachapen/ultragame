attribute vec2 position;
attribute vec4 color;
attribute vec2 textureCoordinate;

uniform mat4 modelViewProjectionMatrix;

varying vec4 colorCoordinate;
varying vec2 fragmentTextureCoordinate;

void main()
{
    fragmentTextureCoordinate = textureCoordinate;
    colorCoordinate = color;
    
    gl_Position = modelViewProjectionMatrix * vec4(position,0,1);
}