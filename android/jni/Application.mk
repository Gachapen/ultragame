NDK_TOOLCHAIN_VERSION := 4.8

# Uncomment this if you're using STL in your project
# See CPLUSPLUS-SUPPORT.html in the NDK documentation for more information
APP_STL := gnustl_shared

APP_ABI := armeabi armeabi-v7a x86

APP_PLATFORM := android-10
