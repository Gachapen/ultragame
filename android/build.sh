if [ ! -d jni/src ]; then
	mkdir jni/src
fi
rsync -va --delete ../src/ jni/src/
echo "Copied source"

if [ ! -d assets ]; then
	mkdir assets
fi
rsync -va --delete ../assets/ assets/
echo "Copied assets"

ndk-build -j4
