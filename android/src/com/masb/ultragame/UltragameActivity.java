package com.masb.ultragame;

import java.util.Locale;

import org.libsdl.app.SDLActivity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.google.example.games.basegameutils.GameHelper;

public class UltragameActivity extends SDLActivity implements GameHelper.GameHelperListener {
	private GameHelper gameHelper;
	private Handler uiThreadHandler;
	private int viewFlags;
	
	static final int REQUEST_LEADERBOARD = 0;
	static final int REQUEST_ACHIEVEMENTS = 1;
	
	// Messages from the SDL thread.
	static final int HIDE_SYSTEM_UI = 2;
	static final int KEEP_SCREEN_ON = 3;
	
	public UltragameActivity() {
		super();
		
		// Handle messages from other threads (for things like UI)
		this.uiThreadHandler = new Handler(Looper.getMainLooper()) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case HIDE_SYSTEM_UI:
					if (msg.arg1 == 1) {
						UltragameActivity.this.hideSystemUi();
					} else {
						UltragameActivity.this.showSystemUi();
					}
					break;
					
				case KEEP_SCREEN_ON:
					if (msg.arg1 == 1) {
						getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
					} else {
						getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
					}
					break;
					
				default:
					super.handleMessage(msg);
				}
			}
		};
	}
	
	@TargetApi(19)
	private void showSystemUi() {
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LAYOUT_STABLE | 
				View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | 
				View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
		);
	}
	
	@TargetApi(19)
	private void hideSystemUi() {
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LAYOUT_STABLE | 
				View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | 
				View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
				View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | 
				View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | 
				View.SYSTEM_UI_FLAG_FULLSCREEN
		);
	}
	
	@TargetApi(16)
	private void setupDefaultUiFlags_api16() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LOW_PROFILE);
	}
	
	@TargetApi(14)
	private void setupDefaultUiFlags_api14() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
	}
    
	private void setupDefaultUiFlags() {
		if (android.os.Build.VERSION.SDK_INT >= 19) {
			this.showSystemUi();
		} else if (android.os.Build.VERSION.SDK_INT >= 16) {
			this.setupDefaultUiFlags_api16();
		} else if (android.os.Build.VERSION.SDK_INT >= 14) {
			this.setupDefaultUiFlags_api14();
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.gameHelper = new GameHelper(this);
		this.gameHelper.enableDebugLog(true, "UltragameGameHelper");
		this.gameHelper.setup(this);
		
		this.setupDefaultUiFlags();
	}

	@Override
	protected void onStart() {
		super.onStart();
		this.gameHelper.onStart(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		this.gameHelper.onStop();
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		this.setupDefaultUiFlags();
	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		this.setupDefaultUiFlags();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		this.gameHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onSignInFailed() {
		if (this.gameHelper.hasSignInError()) {
			Log.e("UltragameActivity", this.gameHelper.getSignInError().toString());
		}
	}

	@Override
	public void onSignInSucceeded() {
		Log.d("UltragameActivity", "Sign in succeeded.");
		onNativeSignedIn();
	}
	
	
	// Functions called from C++

	public String getLocale() {
		Locale locale = getResources().getConfiguration().locale;
		return locale.getLanguage();
	}
	
	public void gameServiceSignIn() {
		UltragameActivity.this.gameHelper.beginUserInitiatedSignIn();
	}
	
	public void gameServiceSignOut() {
		UltragameActivity.this.gameHelper.signOut();
	}
	
	public boolean gameServiceIsSignedIn() {
		return this.gameHelper.isSignedIn();
	}
	
	public void showLeaderBoard() {
		startActivityForResult(this.gameHelper.getGamesClient().getLeaderboardIntent(getString(R.string.leaderboard_high_scores)), REQUEST_LEADERBOARD);
	}
	
	public void submitLeaderBoardScore(int score) {
		this.gameHelper.getGamesClient().submitScore(getString(R.string.leaderboard_high_scores), score);
	}
	
	public void showStatusAndNavBar(boolean show) {
		if (android.os.Build.VERSION.SDK_INT >= 19) {
			Message updateViewFlagsMessage = this.uiThreadHandler.obtainMessage(HIDE_SYSTEM_UI);
			updateViewFlagsMessage.arg1 = (show ? 0 : 1);
			updateViewFlagsMessage.sendToTarget();
		}
	}

	public void showAchievements() {
		startActivityForResult(this.gameHelper.getGamesClient().getAchievementsIntent(), REQUEST_ACHIEVEMENTS);
	}

	public void unlockAchievement(final String achievementName) {
		final String achievementIdentifier = getString(getResources().getIdentifier(achievementName, "string", this.getPackageName()));
		this.gameHelper.getGamesClient().unlockAchievement(achievementIdentifier);
	}
	
	public void keepScreenOn(boolean on) {
		Message keepOnMessage = this.uiThreadHandler.obtainMessage(KEEP_SCREEN_ON);
		keepOnMessage.arg1 = (on ? 1 : 0);
		keepOnMessage.sendToTarget();
	}
	
	
	// C++ functions called from Java
	
	public static native void onNativeSignedIn();
}
